;;; erbc9.el: An rx-interface for fsbot
;; Copyright (C) 2020 Dave Goel
;; Emacs Lisp Archive entry
;; Filename: erbc9.el
;; Package: erbc9
;; Author: Dave Goel <deego3@gmail.com>
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;;; Real Code:



(require 'rx)

;; (defun rx-eval () (error "rx-eval is disabled, for obvious reasons."))

(defun erbot-rx-install ()
  (defun rx-check (&rest ig)
    t)
  (defalias 'rx-eval 'fsi-eval))


(defun fsi-rx-to-string (&optional code nogroup &rest ignore)
  (unless code
    (fsi-rx-usage-error))
  ;; (erbn-log 'rx-to-string code nogroup ignore)
  (erbutils-enabled-check erbot-rx-p)
  (erbot-rx-install)
  (rx-to-string code nogroup))


(defun fsi-rx1 (&rest codes)
  (unless codes
    (fsi-rx-usage-error))
  (format "%S" (fsi-rx-to-string (cons 'and codes) t)))


(defun fsi-rx-usage-error ()
  (error "%s" "Usage examples: ,(rx-to-string '(and \"Em\" \"acs\")) ; ,(rx1 \"Em\" '(and \"a\" \"cs\")) ;  ,(rx1 '(repeat 3 (eval (concat \"v\" \"i!\")))) "
	 ))


(defmacro fsi-rx (&rest args)
  "Note: this is a macro, not a function. Thus, (rx (or AAAA BBB)) will lead to our preferred error below instead of Undefined variable: AAAA"
  (error "The bot does not support the macro rx directly. For supported usage, type ,(rx-to-string) or ,(rx1)"))

  


(defun erbot-rx-toggle ()
  (interactive)
  (cond
   ((null erbot-rx-p)
    (setf erbot-rx-p t erbn-rx-time (current-time) erbn-rx-ctrbad 0)
    (message "Enabled rx."))
   (t
    (setf erbot-rx-p nil)
    (message "Disabled rx."))))
   

 
(provide 'erbc9)




;;; erbc9.el ends here
