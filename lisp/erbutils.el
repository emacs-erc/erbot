;;; erbutils.el --- 
;; Time-stamp: <2006-04-24 12:30:26 deego>
;; Copyright (C) 2002,2003,2004,2005  D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbutils.el
;; Package: erbutils
;; Author: D. Goel <deego3@gmail.com>
;; Version: 0.0dev
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
 

(defvar erbutils-home-page
  "http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot")


 
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 
(defvar erbutils-version "0.0dev")

;;==========================================
;;; Code:
(require 'rot13)

(defgroup erbutils nil 
  "The group erbutils"
   :group 'applications)
(defcustom erbutils-before-load-hooks nil "" :group 'erbutils)
(defcustom erbutils-after-load-hooks nil "" :group 'erbutils)
(run-hooks 'erbutils-before-load-hooks)


(defalias 'erbutils-stringize 'erbutils-stringify)
;; should not eval anyting... since called by erbc..





(defun erbutils-stringify (msg-list)
  "ALIASED TO FSI. Used by parse."
  (if (stringp msg-list)
      msg-list
    (mapconcat 
     '(lambda (arg)
	(if (stringp arg) arg
	  (progn
	    ;; (message "DEBUG: Converted %s to a string." arg)
	    (format "%s" arg))))
     msg-list " " )))



(defun erbutils-stringifyc (msg-list &rest args)
  "FSI ALIAS.
Like erbutils-stringify, tries to return the output in the same format that the input was. Thus, symbols become strings, strings remain strings, and lists remain lists. 
Provided for user convenience fs-... etc.
NOTE that this is further used by stringifyd"
 (if args (setf msg-list (cons msg-list args)))
 (cond 
  ((stringp msg-list)
      msg-list)
  ((null msg-list) nil)
  ((listp msg-list) (mapcar 'erbutils-stringifyc msg-list))
  (t 
	(if (stringp msg-list) msg-list
	  (progn
	    ;; (message "DEBUG: Converted %s to a string." msg-list)
	    (format "%s" msg-list))))))



(defun erbutils-stringifyd (&rest arg)
  "FSI ALIAS. DWIM useful for userfunctions to parse arguments. Converts a list of symbols/strings/user-input to a string, with inputs separated by spaces."
  (let ((res (erbutils-stringifyc arg)))
    (mapconcat
     #'(lambda (a)
	 (cond
	  ((not (listp a)) 
	   (format "%s" a))
	  ((= (length a) 1)
	   (if (listp (car a))
	       (apply #'erbutils-stringifyd (car a))
	     (erbutils-stringifyd (car a))))
	  ((= (length a) 0)
	   "nil")
	  ((and (listp a) (> (length a) 1))
	   (apply #'erbutils-stringifyd a))
	  (t
	   (error " I should not be here in erbutils-stringifyd"))
	  ))
     res
     " ")))




(defun erbutils-stringifyb (msg-list)
  "FSI ALIAS. Prefer stringifyc instead for most purposes going forward 20160629 ? stringify when msg is to be thought of as an english message."
  (if (stringp msg-list)
      msg-list
    (mapconcat 
     '(lambda (arg)
	(if (stringp arg) arg
	  (format "%s" arg)))
     msg-list " " )))



(defun erbutils-string= (foo bar &optional ignore-case)
  (and foo bar 
       (if ignore-case 
     (string= (downcase foo) (downcase bar))
   (string= foo bar))))


(defun erbutils-errors-toggle ()
  (interactive)
  (setq erbutils-ignore-errors-p 
  (not erbutils-ignore-errors-p))
  (message "erbutils-ignore-errors-p set to %s"
     erbutils-ignore-errors-p))



;;;###autoload
(defmacro erbutils-ignore-errors-loudly (&rest body)
  "Copied from dg's utils.el 20160613"
    (let ((err (gensym)))
      `(condition-case ,err (list (progn ,@body) nil)
	 (error
	  (list nil ,err)))))




(defvar erbutils-ignore-errors-p t)

(defmacro erbutils-ignore-errors (&rest body)
  "DOES NOT return nil, unlike ignore-errors.."
  (let ((err (gensym)))
    `(condition-case ,err (progn ,@body)
       (error
  (progn
          ;(ding t)
          ;(ding t)
    ;;(message "ERROR: %s" (error-message-string ,err))
    ;;(sit-for 1)
    (ding t)
    (unless erbutils-ignore-errors-p
      (error (error-message-string ,err)))
    (unless fsi-found-query-p 
      (erbutils-error 
       "%s"
       (fsi-limit-lines
        (error-message-string ,err)))))))))


;;;###autoload
(defmacro utils-ignore-errors (&rest body)
    "Like ignore-errors, but returns a list of body, and the error...
Improved for me by Kalle on 7/3/01:
 * used backquote: something i was too lazy to convert my macro to..
 * removed the progn: condition-case automatically has one..
 * made sure that the return is nil.. just as it is in ignore-errors. "
    (let ((err (gensym)))
      `(condition-case ,err (list (progn ,@body) nil)
	 (error
	  (list nil ,err)))))



(defvar erbutils-error-debug-p nil
  "Turn on for debugging.."
  )
(defun erbutils-error (&rest args)
  "NOTE: This is invokable by the users as a fsi- function!  
All it does is pass args to format, so should be safe."
  (cond
   (erbutils-error-debug-p (apply 'error args))
   (t 
    (unless args (error 
		  (format "Syntax: , (fs-error msg &rest format-args)")))
    (let* ((main
	    (erbutils-random 
	     '(
	       ;;"Blue Screen: %s"
	       "BEEEP: %s"
	       "ERROR: %s"
	       "err..%s"
	       ":(   %s"
	       "D'oh!  %s"
	       "Oh sh**!  %s"
	       "Nooo!  %s"
	       "oops,  %s"
	       "Uh oh,  %s"
	       "whoops,  %s"
	       )))
	   (result
	    (format main
		    (apply 'format args))))
      (or
       (ignore-errors
	 (fsi-h4x0r-maybe
	  (fsi-studlify-maybe
	   result)))
       result)))))


(defun erbutils-matching-variables (string)
  "returns all variables that start with string"
  (apropos-internal (concat "^" (regexp-quote string))
		    'boundp))


(defun erbutils-matching-functions (string)
  "returns all functions that start with string"
  (apropos-internal (concat "^" (regexp-quote string))
        'fboundp)
)





 (defun erbutils-quote-list (ls)
   "ls is, in general, a tree...

 We will make sure here that each element of the tree that is a symbol gets
 quoted...    


 "
   (mapcar '(lambda (arg)
       (list 'quote arg))
    ls))

(defun erbutils-random (list &optional weights &rest ig)
  "NOTE: DEFALIASTED TO RANDOM-CHOOSE AND RANDOM-CHOICE.
Return a random element from LIST. 
Optional WEIGHTS are relative.  They should be integers. 
example:  (erbutils-random '(a b c) '(1 1 2)) should return c twice
as often as it returns a.
If list is nil, but weights are not, then we will return a choice numebr. That is, we will assume that the list is '(0 1 2 ...) in that case. 

"
  (assert (listp weights))
  (when (and (null list) (not (null weights)))
      (setq list (number-sequence 0 (1- (length weights)))))
  (cond
   ((null weights) 
    (nth (random (length list)) list))
   (t
    (let* (
	   (len (length list))
	   (revw (reverse weights))
	   (fir (car revw))
	   )
      ;; If weights are partially specified, fill in missing entries. 
      (while (< (length revw) len)
	(setq revw (cons fir revw)))
      (setq weights (reverse revw))
      (let* ((total (apply '+ weights))
	     (choice (random total))
	     (curw weights)
	     (ctr 0) ;; ctr does a cumsum over weights during the while loop. 
	     (num 0))
	(when (not (integerp total))
	    (error "Weights should be all integers."))
	;; return if choice < current cumsum. 
	(while (>= choice (+ ctr (car curw)))
	  (setq ctr (+ ctr (car curw)))
	  (incf num)
	  (setq curw (cdr curw)))
	(nth num list))))))

(defalias 'fsi-random-choose 'erbutils-random)



(defun erbutils-describe-variable (&optional variable buffer)
  "Like describe-variable, but doesn't print the actual value. Does not unfill. Do that in fsi-dv.."
  (unless (bufferp buffer) (setq buffer (current-buffer)))
  (if (not (symbolp variable))
      (message "Unknown variable or You did not specify a variable")
    (let (valvoid)
      (with-current-buffer buffer
  (with-output-to-temp-buffer "*Help*"
    (terpri)
    ;; 20190915 comment out erbcompat stuff. - dg.
    ;; (if (erbcompat-local-variable-p variable)
    ;;     (progn
    ;; 	  (princ (format "Local in buffer %s; " (buffer-name)))
    ;; 	  (terpri)))
    ;; (terpri)
    (let ((doc 
	   (documentation-property variable 'variable-documentation)))
      (princ (or doc "not documented as a variable.")))
    (help-setup-xref (list #'describe-variable variable (current-buffer))
		     (interactive-p))
    
    ;; Make a link to customize if this variable can be customized.
    ;; Note, it is not reliable to test only for a custom-type property
    ;; because those are only present after the var's definition
    ;; has been loaded.
    (if (or (get variable 'custom-type) ; after defcustom
	    (get variable 'custom-loads) ; from loaddefs.el
	    (get variable 'standard-value)) ; from cus-start.el
        (let ((customize-label "customize"))
	  (terpri)
	  (terpri)
	  (princ (concat "You can " customize-label " this variable."))
	  (with-current-buffer "*Help*"
	    (save-excursion
	      (re-search-backward
	       (concat "\\(" customize-label "\\)") nil t)
              (if (> 22 emacs-major-version)
                  (help-xref-button 1 (lambda (v)
                                        (if help-xref-stack
                                            (pop help-xref-stack))
                                        (customize-variable v))
                                    variable
                                    "mouse-2, RET: customize variable")
                (help-xref-button 1 'help-customize-variable variable))
              ))))
    ;; Make a hyperlink to the library if appropriate.  (Don't
    ;; change the format of the buffer's initial line in case
    ;; anything expects the current format.)
    (let ((file-name (symbol-file variable)))
      (when file-name
        (princ "\n\nDefined in `")
        (princ file-name)
        (princ "'.")
        (with-current-buffer "*Help*"
    (save-excursion
      (re-search-backward "`\\([^`']+\\)'" nil t)
      (if (> 22 emacs-major-version)
          (help-xref-button
           1 (lambda (arg)
               (let ((location
                      (find-variable-noselect arg)))
                 (pop-to-buffer (car location))
                 (goto-char (cdr location))))
           variable "mouse-2, RET: find variable's definition") 
        (help-xref-button 1 'help-variable-def variable file-name)) 
      ))))

    ;; this does not exist in emcas29??
    ;; (print-help-return-message)  
    (save-excursion
      (set-buffer standard-output)
      ;; Return the text we displayed.
      (buffer-substring-no-properties (point-min) (point-max))))))))


(defvar erbutils-itemize-style
  ;;(list "[%s] %s\n\n" "[%s] %s\n\n" "[%s] %s,\n\n" "[%s/%s] %s\n\n")
  ;; remove any commas at all. commas can interfere with urls:
  (list "[%s] %s\n\n" "[%s] %s\n\n" "[%s] %s\n\n" "[%s/%s] %s\n\n")

  "Style to use when itemizing. Uses upto 3 argumments.
The first style is used for the first term, the second used for
the final term, and the third used for any intermediate terms.
In case there are a large number of matches, we use the final
style for only the very first term.

Another good choice, for example, and used by petekaz's petebot, is  
  \(list \"[%s] %s,\n\n\" \"and also [%s] %s\n\n\" \"and [%s] %s,\n\n\")
")

(defun erbutils-itemize (result &optional N shortenedp style)
  "FSI DEFALIAS!"
  (unless style (setq style erbutils-itemize-style))
  (unless (integerp N) (setq N 0))
  (let*
      ((st1 (first style))
       (st2 (second style))
       (st3 (third style))
       (st4 (fourth style))
       (ctr N)
       (rem result)
       (lr (length result))
       (sofar "")
       (largep (> lr 4)) ;; lr is the number of terms we output. The result that was input to us is /already/ shortened per a paginator..
        ;; This logic is to tell us whether to output the total nmuber of entries for user info..
       (totalentries (+ lr N)) ;; this is the total number of entries the term must have had.
       )
    (if (and (= N 0) (equal (length result) 1))
    ;; (if (and (equal (length result) 1))
	(setq sofar (format "%s" (car result)))
      (while rem
	(setq sofar 
	      (concat 
	       sofar 
	       (cond
		((and largep (= ctr N)) (format st4 ctr (- totalentries 1) (car rem)))
		((= ctr N) (format st1 ctr (car rem)))
		((null (rest rem)) (format st2 ctr (car rem)))
		(t (format st3 ctr (car rem))))))
	(setq ctr (+ ctr 1))
	(setq rem (cdr rem))))
    (when shortenedp 
      (setq sofar (concat sofar " .. + other notes")))
    sofar))





(defun erbutils-function-minus-doc (fstr &rest ignore)
  "fstr is the string containing the function"
  (let* ((fdoc (if (stringp fstr) fstr (format "%s" fstr)))
   newdoc)
    (setq newdoc
    (with-temp-buffer 
      (insert fdoc)
      (goto-char (point-min))
      (search-forward "(" nil t)
      (forward-sexp 4)
      (if (stringp (sexp-at-point))
    ;; this sets mark.. bad programming, i know..
    (backward-kill-sexp 1))
      (erbutils-buffer-string)))
    (erbutils-single-lines newdoc)))


(defun erbutils-function-cleanup (fstr &rest ignore)
  "fstr is the string containing the function"
   (pp-to-string 
    (erbutils-function-cleanup1 (erblisp-unsandbox-defun (fsi-read fstr)))))

 
(defun erbutils-function-cleanup1 (expr)
  "Remove any occurrences of (sit-for 0) OR (erblisp-check-args)."
  (erbn-type-check expr)
  (cond
   ((atom expr) 
      expr)
   ((atom (car expr))
    (cons (car expr) (erbutils-function-cleanup1 (cdr expr))))
   (t
    (let ((e1 (caar expr)) (e2 (cadar expr)) skip)
      (setf skip (equal 'erblisp-check-args e1))
      (setf skip (or skip (and (equal e1 'sit-for) (equal e2 0))))
       (if skip 
	   (erbutils-function-cleanup1 (cdr expr))
	 (cons (car expr) (erbutils-function-cleanup1 (cdr expr))))))))



  

(defun erbutils-single-lines (str)
  "Eliminates all \n or lines comprising entirely of whitespace"
  (mapconcat 
   'identity
   (delete-if
    (lambda (str) 
      (string-match "^[ \t]*$" str))
    (split-string str
      "\n"))
   "\n"))

(defun erbutils-cleanup-whitespace (str)
  "Strip all leading whitespace and replace one or more tabs, newlines,
or spaces with a single space."
  (let ((result (erbutils-replace-regexp-in-string "[\t\n ]+" " " str)))
    (subseq result (or (position ?  result :test-not 'eq) 0))))

(defun erbutils-downcase (str)
  (if (stringp str)
      (downcase str) 
    str))






(defun erbutils-add-nick (msg)
  (if
      (and (not fsi-found-query-p)
	   (not fsi-internal-directed)
	   (not erbn-internal-midsentence-p)
	   (> (random 100) 30)
	   (stringp msg))
      (eval 
       (erbutils-random
	'(
	  ;;(concat msg ", " fs-nick)
	  (concat fs-nick ": " msg)
	  (concat fs-nick ", " msg)
	  )
	'(1 1 )))
    msg))

(defvar erbutils-add-nick-p 'maybe)   ;; unused atm but can be set to nil to disable nick adding.  
(defun erbutils-add-nick-maybe (msg)
  (if
      erbutils-add-nick-p ;; see also the conditions in erbutils-add-nick. 
      (eval 
       (erbutils-random
	'((erbutils-add-nick msg)
	  msg)
	fsi-internal-add-nick-weights
	))
    msg))


(defun erbutils-convert-sequence (arg)
  (if (sequencep arg)
    arg
    (format "%s" arg)))


(defvar erbutils-eval-until-limited-length 200)

(defun erbutils-eval-until-limited (expr)
  (let 
      ((ans nil) (donep nil) (ctr 0))
    (while (not donep)
      (incf ctr)
      (setq ans
	    (eval expr))
      (setq donep (<= (length (format "%s" ans)) 
		      erbutils-eval-until-limited-length))
      (if (and (not donep) (> ctr 50))
	  (error "Counter exceeded in erbutils-eval-until-limited.")))
    ans))



(defun erbutils-replace-strings-in-string (froms tos str &rest
             args)
  "Please note that this behavior is akin to that of let*. That is, replacements are done in succession."
  (let ((st str))
    (mapcar*
     (lambda (a b)
       (setq st (apply 'erbutils-replace-string-in-string
           a b st args)))
     froms tos)
    st))
  
;;;###autoload

(defun erbutils-replace-string-in-string (from to string &optional
					       delimited start end)
  "FSI alias! The normal replace-regex-in-string is NOT safe.
or simply look at the new emacs string-replace?
"
  (assert (stringp from))
  (assert (stringp to))
  (save-excursion
    (with-temp-buffer
      (insert string)
      (goto-char (point-min))
      (replace-string from to delimited start end)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun erbutils-sublist-p (a b &optional start)
  "tells if list a is a member of list b.  If start is true, the match
should start at the beginning of b."
 (cond
  ((null a) t)
  ((null b) nil)
  (start (and
    (equal (car a) (car b))
    (erbutils-sublist-p (cdr a) (cdr b) t)))
  (t
   (let ((foo (member (car a) b)))
     (and foo 
    (or 
     (erbutils-sublist-p (cdr a) (cdr foo) t)
     (erbutils-sublist-p a (cdr foo))))))))

;;;###autoload
(defun erbutils-flatten (tree &rest ig)
  "FSI ALIAS"
  (cond
   ((null tree) nil)
   ((listp tree) (apply 'append
      (mapcar 'erbutils-flatten tree)))
   (t (list tree))))
 
(provide 'erbutils)
(run-hooks 'erbutils-after-load-hooks)


(defun erbutils-remove-text-properties (str1)
;;;   (with-temp-buffer
;;;     (insert text)
;;;     (buffer-substring-no-properties (point-min) (point-max))))
  ;; fledermaus' code: avoid with-temp-buffer becuse of i8n problems.
  (let ((str (copy-sequence str1)))
    (set-text-properties 0 (length str) nil str) 
    str))






(defun erbutils-defalias-i (ls &optional num prefix prefix-rm
			       functionpref)
  "Similar to erbutils-defalias, except that for functions, it
defaliases a 'fsi-"

  (unless functionpref (setq functionpref "fsi-"))
  (erbutils-defalias ls num prefix prefix-rm functionpref nil)) 


(defun erbutils-defalias-f (ls &optional num prefix prefix-rm
			       functionpref)
  (unless functionpref (setq functionpref "fsi-"))
  (erbutils-defalias ls num prefix prefix-rm functionpref 'f))

(defun erbutils-defalias-v (ls &optional prefix prefix-rm
			       functionpref)
  (unless functionpref (setq functionpref "fsi-"))
  (erbutils-defalias ls nil prefix prefix-rm functionpref 'v))


(defun erbutils-defalias (ls &optional num prefix prefix-rm functionpref fvspec)
  "Define new fs- aliases from ls. 

If the entry in the ls is a function, it is defaliased.  If it is
a variable, we define a new function!  That function will return
the value of the variable.

When prefix and prefix-rm is provided, we assume that the entry is of
the form prefix-rmENTRY. And we then (defalias fs-prefixENTRY
ENTRY).

functionpref should usually be fs-.  If you want fsi- instead, you
might prefer calling erbutils-defalias-i instead.

If fvspec is 'f, we assume the target is a function
If fvspec is 'v, we assume that the target is a variable.
If nil, we try to figure out somewhat smartly.

If num is nil, we emit a warning. 
If num is 'none, we do a vanilla defalias, with no num-check.

Our new function will allow exactly NUM arguments. 
"
  (unless (listp ls) (setq ls (list ls)))
  (unless functionpref (setq functionpref "fsi-")) ;; Change default to fsi going forward. instead of fs-. That is, everything we define is immutable!
  (let* ((pref (if prefix (format "%s" prefix) ""))
	 (pref-rm (if prefix-rm (format "%s" prefix-rm) ""))
	 (lenrm (length pref-rm))
	 (reg (concat "^" (regexp-quote pref-rm)))
	 varp fp smartp thisf)
    (case fvspec
      ((nil) (setf smartp t))
      ((f) (setf fp t))
      ((v) (setf varp t))
      (otherwise (error "fvspec can only be one of 'f, 'v, or nil.")))
    (mapcar 
     (lambda (arg)
       (let* (      
	      (argst (format "%s" arg))
	      (gop (string-match reg argst))
	      (arg2 (and gop (substring argst lenrm)))
	      (foo (and gop (intern (format (concat functionpref "%s%s")
					    pref arg2)))))
	      
	 (when gop
	   (cond
	    (varp (setf thisf nil))
	    (fp (setf thisf t))
	    (t (assert smartp) (setf fp (functionp arg))))
	   (if fp  ;; functionp 
	       (progn
		 (unless num
		   (warn  "In erbutils-defalias-i, PLEASE specify NUM or 'none FOR %S %S" foo arg))
		 ;; (defalias foo arg)
		 (erbutils-defalias-n arg num)
		 )
	     (erbutils-defalias-vars (list arg prefix prefix-rm))
	     ;;`(defun ,foo () 
	     ;;   ,(concat "Pseudo function that returns the value of `"
	     ;;    argst "'. ")
	     ;;,arg)
	     ))))
     ls)))




(defun erbutils-defalias-vars (ls &optional prefix prefix-rm)
  (let* ((pref (if prefix (format "%s" prefix) ""))
   (pref-rm (if prefix-rm (format "%s" prefix-rm) ""))
   (lenrm (length pref-rm))
   (reg (concat "^" (regexp-quote pref-rm))))
    (mapcar 
     (lambda (arg)
       (let* (      
        (argst (format "%s" arg))
        (gop (string-match reg argst))
        (arg2 (and gop (substring argst lenrm)))
        (foo (and gop (intern (format "fs-%s%s" pref arg2)))))

   (when gop
     (eval 
      `(defun ,foo () 
         ,(concat "Pseudo function that returns the value of `"
      argst "'. ")
         ,arg)))))
     ls)))
      

(defun erbutils-region-to-string (fcn &rest  str)
  (with-temp-buffer
    (while str 
      (let ((aa (car str)))
  (when aa
    (insert (format "%s " aa))))
      (pop str))
    (goto-char (point-min))
    (funcall fcn (point-min) (point-max))
    (buffer-substring-no-properties (point-min) (point-max))))


(defun erbutils-rot13 (str)
  (apply
   'string
   (mapcar
    (lambda (i)
      (let ((foo (aref rot13-display-table i)))
  (if foo (aref foo 0) i)))
    str)))

(defun erbutils-file-contents (file)
  (cond
   ((not (file-exists-p file))
    "")
   (t 
    (with-temp-buffer 
      (insert-file-contents file)
      (buffer-substring-no-properties (point-min) (point-max))))))


(defun erbutils-file-sexps (file)
  (let ((str (erbutils-file-contents file))
	expr)
    (and 
     (stringp str)
     (not (string= str ""))
     (setq expr (erbn-read (concat " ( " str " )"))))))


(defun erbutils-functions-in-file (file)
  "Returns the list of functions in the file.  File should be a valid
lisp file, else error. "
  (let ((str (erbutils-file-contents file))
	expr)
    (and 
     (stringp str)
     (not (string= str ""))
     (setq expr (erbn-read (concat " ( " str " )")))
     (ignore-errors (mapcar 'second expr)))))


    
(defun erbutils-mkback-maybe (file)
  (ignore-errors (require 'mkback))
  (ignore-errors 
    (let ((mkback-interactivity -100))
      (mkback file))))


(defun erbutils-listp-proper (l) 
  "FSI ALIAS. from <Riastradh>. 20180603 dg: this xSeems to ensure that list does not have assoc-list type elements in it. That the last cdr is always a list."
  (or (null l) (and (consp l)
		    (erbutils-listp-proper (cdr l)))))


(defun erbutils-html-url-p (str)
  "Guesses if the string is a url that will yield HTML content.
Basically, look for any url that doesn't have any extension or
one that has .html, .shtml, or .htm.  Returns the str if it is
a valid url that might generate HTML."
  (when (string-match "^http://[^/]+/?\\(.*\\)?$" str)
    (let* ((path (match-string 1 str))
           (pos (position ?. path :from-end)))
      (when (or (null pos)
                (string-match "html?" (subseq path pos)))
        str))))


;;;###autoload
(defun erbutils-concat-symbols (&rest args)
  "Like `concat' but applies to symbols, and returns an interned
concatted symbol.  Also see fsbot's `erbn-command-list-from-prefix'.  

Thanks to edrx on #emacs for suggesting 'symbol-name.."
  (let* ((strings (mapcar 'symbol-name args))
	 (str (apply 'concat strings)))
    (intern str)))




(defun erbutils-remove-text--properties (str)
  (let (str2)
    (cond
     ((stringp str)
      (setq str2 (copy-sequence str))
      (set-text-properties 0 (length str2) nil str2)
      str2)
     (t (error "[ertp] Not a string.")))))




(defun erbutils-remove-text-properties-maybe (str)
  (if (stringp str) 
      (erbutils-remove-text-properties str)
    str))


(defun erbutils-buffer-string ()
  (buffer-substring-no-properties (point-min) (point-max)))


(defmacro erbutils-enabled-check (var)
  "Unsafe, unhygienic macro! Multiple var evaluations!  Does NOT insist on atoms!"
  (assert (atom var))
  `(when (or erbot-paranoid-p (not ,var))
     (error "Variable %s is disabled, or erbot-paranoid-p is t, atm. " ',var)))

(defun erbutils-replace-regexp-in-string (from to str &rest args)
  "DEFALIASED VIA FSI-REPLACE-REGEXP-IN-STRING.
A safer version of replace-regex-in-string that only works when args are strings. 
In replace-regex-in-string, TO can also be a function. We don't allow that here.
Thus, throughout erbot source code, any calls to replace-regex-in-string are to be replaced by this function."
  (assert (stringp from))
  (assert (stringp to))
  (assert (stringp str))
  (unless (<= (length args) 1) (error "Erbot allows atmost 4 args to replace-regexp-in-string"))
  (apply 'replace-regexp-in-string from to str args))


(defun erbutils-string-match (reg str &optional start &rest otherargs)
  "A safer version of string-match that only works when arguments are strings."
  (assert (stringp reg))
  (assert (stringp str))
  (when otherargs (error "Syntax: fs-string-match reg str [start]"))
  (when start (assert (numberp start)))
  (funcall 'string-match reg str start))



(defvar erbutils-irc-remove-ticks-p nil)
(defvar erbutils-irc-remove-commas-p nil)

(defun erbutils-irc-lisp-preprocess (str)
  " We can probably safely escape many things here: such as #, `, etc.  Note (check) that this function is only called when we are NOT given a ( ..). In such cases, we should feel free to, maybe, aggressively quote."
  (when erbutils-irc-escape-chars-p (setf str (erbutils-irc-escape-chars str)))
  (when erbutils-irc-remove-ticks-p (setf str (erbutils-irc-remove-ticks str)))
  (when erbutils-irc-remove-commas-p
    (setf str (erbutils-irc-remove-commas str)))
  str)

(defun erbutils-irc-remove-ticks (string)
  "IRC uses backticks a lot, unfortunately. And, when user's messages to us involve backticks, they are usually NOT thinking of backquoting, which we disallow, in any case.
UPDATE: This will be unneeded going forward. See, instead, erbutils-irc-escape-chars."
  (setf string (erbutils-replace-string-in-string "`" "1" string))
  )


(defun erbutils-irc-remove-commas (string)
  "try to avoid this error when using lisp functions. <deego> fsbot: hello onhetn, bar => <@fsbot> err..Symbol's function definition is void: fs-\,
UPDATE: This will be unneeded going forward. See, instead, erbutils-irc-escape-chars." 
  (setf string (erbutils-replace-string-in-string "," " " string))
  )


(defcustom erbutils-irc-escape-chars-p t "")



(defcustom erbutils-irc-escape-reg

  (rx
   (or  "#" "," "`" "[" "]"  "\"" "'" 
       ))
  "NB: DO NOT escape ('s. That's because we insert things (function rest-of-message) and then escape the whole thing.
A paren will espace even the surrounding parens.")

(defun erbutils-irc-escape-chars (string)
  "try to avoid this error when using lisp functions. <deego> fsbot: hello onhetn, bar => <@fsbot> err..Symbol's function definition is void: fs-\,"
  (with-temp-buffer
    (insert string)
    (goto-char (point-min))
    (while
	(re-search-forward erbutils-irc-escape-reg nil t)
      (replace-match "\\\\\\&")) ;; here \\\\ is one \. Then, \\& refers to the original match.
    (buffer-substring-no-properties (point-min) (point-max))))


			     
;; copied from utils.el 20150926 
(defvar erbutils-chars
  (append
   (loop for i from 97 to (+ 97 25) collect i)
   (loop for i from 65 to (+ 65 25) collect i)
   ))

;; copied from utils.el 20150926 
(defun erbutils-randomstring (&optional len &rest ig)
  "FSI ALIAS. NOTE that when you use the result to create terms, the complexity of this randomstring is NOT 52^len. It is ONLY 26^len The reason is that two terms cannot exist if their (downcase ) 's are identical."
  (unless len (setf len 6))
  (let* ((n (length erbutils-chars))
	 (chars (loop for i from 1 to len collect (nth (random n) erbutils-chars))))
    (apply 'string chars)))

(defun erbutils-chmod (attrib buf)
  (with-current-buffer buf
    (let* ((f (buffer-file-name))
	   (g (when f (file-truename f))))
      (when g
	(progn (shell-command-to-string
		(concat "chmod "
			(shell-quote-argument attrib)
			" "
			(shell-quote-argument g))))
	;; (sit-for 1)
	;; (message "Set attrib %s for file %s" attrib g)
	t))))

(defun erbutils-chmod-a+r (buf)
  (erbutils-chmod "a+r" buf))
     


(defun erbutils-text-char-description (str)
  (mapconcat
   #'(lambda (a)
       (text-char-description a))
       str "")
  )


(defun erbutils-fail2ban (ctr time goodp)
  ;; CTR keeps track of bad events. An event is either goodp or badp.
  ;; TIME is the last time the ctr was decremented.
  ;; Bad events lead to an increase in the ctr.
  ;; But, every 24 hours, we lower the counter a bit.
  ;; We return (ctr time) as output. 
  ;; If ctr is too high, your code may then choose to fail2ban.
  ;; will be used by erbn-octave-fail2ban in erbc8.
  (when (not goodp) (incf ctr))
  (when (> (time-to-seconds (time-subtract (current-time)
					   time))
	   86400 ;; 1 day
	   )
    (setf time (current-time)
	  ctr (- ctr 1)))
  (list ctr time))





(defun erbutils-set-assoc (list car cdr)
  "Copied from dg's utils.el. A hack, created 2002, to set-assoc.  Will use 20160506 to update erbn-lastall for fulllogging."
    (let* ((cop (copy-tree list))
	   (carcdr (cons car cdr))
	   (foundp (member carcdr cop)))
      (if foundp
	  cop
	(let ((assoced (assoc car cop)))
	  (cons carcdr
		(if assoced
		    (remove assoced cop)
		  cop))))))



(defun erbutils-backtrace ()
  "Copied from dg's utils."
  (with-temp-buffer
    (let ((standard-output (current-buffer)))
      (backtrace))
    (buffer-substring-no-properties (point-min) (point-max))))


(defun erbutils-trace-backtrace ()
  "To debug loops."
  (find-file "~/backtrace.txt")
  (goto-char (point-max))
  (insert "\n\n=======\n\n" (erbutils-backtrace) "\n\n")
  (save-buffer))



(defun erbutils-defalias-n-old-202002ee (sym &optional num )
  "Defalias fsi-f to f. But, allow ONLY n arguments.
If num is nil, the parent function would emit a warning, to guard against accidental nils. 
The only proper way to supply a nil is to supply 'none.
If you acutally supply a nil, we interpret it as = 1.

We define a function that accepts exactly NUM arguments.  If more arguments are supplied, we use ONLY the first NUM arguments.  If fewer arguments are supplied, this will result in an error. This is an old version. The behavior is relaxed in the new erbutils-defalias-n. 
"
  (assert (or (null num)
	      (numberp num)
	      (and (symbolp num) (eq num 'none))))
  (when (null num) (setf num 1))
  (assert (symbolp sym))
  (let
      ((fsifun (intern (format "fsi-%s" sym))))
    (if (numberp num) 
	(eval
	 `(defun ,fsifun (&rest args)
	    ;; (setf args (subseq args 0 ,num))
	    (apply ',sym (subseq args 0 ,num))))
      ;; num=='none
      (defalias fsifun sym))))



(defun erbutils-defalias-n (sym &optional num )
  "Defalias fsi-f to f. But, allow ONLY n arguments.
If num is nil, the parent function would emit a warning, to guard against accidental nils. 
The only proper way to supply a nil is to supply 'none.
If you acutally supply a nil, we interpret it as = 1.
We define a function that accepts exactly NUM or fewer arguments.  If more arguments are supplied, we use ONLY the first NUM arguments. 

"
  (assert (or (null num)
	      (numberp num)
	      (and (symbolp num) (eq num 'none))))
  (when (null num) (setf num 1))
  (assert (symbolp sym))
  (let
      ((fsifun (intern (format "fsi-%s" sym))))
    (if (numberp num) 
	(eval
	 ;; 20200208 note that this is NOT a macro.
	 ;; This will NOT do a multiple-eval of args or anything like that.
	 ;; Yes, we are using args multiple times, and that's just fine. 
	 `(defun ,fsifun (&rest args)
	    ;; (setf args (subseq args 0 ,num))
	    ;; (unless (= ,num (length args))
	    ;; (error "You supplied %s arguments but function %s expects exactly %s arguments." (length args) ',sym ,num))
	    (apply ',sym (subseq args 0
				 (min ,num (length args))
				 ))))
      ;; num=='none
      (defalias fsifun sym))))


;;; erbutils.el ends here
