;;; erbc7.el --- More fsbot functions. Including fstrash and dump functions.  
;; Copyright (C) 2003 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc7.el
;; Package: erbc7
;; Author: D. Goel <deego3@gmail.com>
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;;; Real Code:




(defun fsi-channel-public-p ()
  ;; if channel OR nick matches the keyword, then allow!
  (let ((foundp nil)
	(tgt (format "%s" erbn-tgt))
	(nick (format "%s" erbn-nick)) 
	;; it can be nil when you call fs-pf-load from .emacs.  so we set it to a string.
	;; you should really call erbn-pf-load, etc.
	)
    (when (not foundp)
      (loop for ii in erbn-channel-public-list do
	    (when
		(or
		 (string-match ii tgt)
		 (string-match ii nick)
		 )
	      (setf foundp "Channel")
	      (return))))
    ;; (when (not foundp)
    ;;   (loop for ii in erbn-channel-public-list do
    ;; 	    (when (string-match ii erbn-nick)
    ;; 	      (setf foundp "Nick")
    ;;    (return))))
    foundp))

 
(defun erbn-log-file (&optional howold)
  "Return a dated file name so the log doesn't grow out of bounds.  Optional argument HOWOLD: 1 means yesterday's log, 2 means day-before's logfile, etc."
  (let ((tstring
	 (if (null howold)
	     (format-time-string "%Y%m%d" ) 
	   (progn (setf howold (round howold))
		  (format-time-string "%Y%m%d" (time-subtract (current-time) (seconds-to-time (* 24 60 60 howold))))))))
    (format  "%s%s.txt" erbn-log-file-base tstring)))


		     
(defun erbn-botlog (&optional howold)
  "Print the entire botlog for the day. Optional arg HOWOLD for prior days."
  (erbn-file-contents  (erbn-log-file howold)))


(defun fsi-botlog (&optional howold &rest ig)
  " Syntax: botlog [days-old]. Or, see these logs at log/ online.
Fulllog: Every interaction is logged to an unlisted url. Type ,dump to see your previous interaction. This can also be used to see the full output, bypassing the need to type ,more repeatedly.

Log: Actions that change fsbot's db are logged publicly for all to see. Type ,botlog to see these. "
  (let* ((res (erbn-botlog howold)))
    (when (= 0 (length res)) (setf res "<No botlogs on this date>"))
    (if howold
  	res
      (progn 
  	(concat        "Syntax: botlog [days-old].  Or, see these logs at http://gnufans.net/~fsbot/botlog/ online. For session history, type ,h")))))

(defun erbn-file-contents (name)
  (with-temp-buffer
    (when
	(file-exists-p name)
      (insert-file-contents name))
    (buffer-substring-no-properties (point-min) (point-max))))
		    







(defun erbn-log-save ()
  (find-file (erbn-log-file))
  (erbot-save-kill-buffer))


(defun erbn-fulllog-new (currentall)
  (let*
      ((id (nth 0 currentall))
       ;; (elfile (expand-file-name (concat id ".el") erbn-tmplog-path))
       (txtfile (expand-file-name (concat id ".txt") erbn-tmplog-path)))
    ;; Let's not generate elfile atm. 20160517 
    ;; (find-file elfile)
    ;; (goto-char (point-max))
    ;; (insert (format "%S" currentall))
    ;; (add-to-list 'erbot-buffers-to-save (buffer-name))
    (find-file txtfile)
    ;; 20201220 try to ensure it shows up as utf-8. 
    (set-buffer-file-coding-system 'utf-8)
    ;; hm, this will have no effect on how the txt-dump file shows up in a browser. Try inserting a meta field below. doesn't work either. 
    ;; Instead, we need to find a way to coax apache to use utf-8 when rendering txt files
    (goto-char (point-max))
    ;; these have no effect because it is still a text file. 
    ;; (insert "<meta charset=\"utf-8\">\n\n")
    ;; (insert "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n\n")
    (insert (format "<%s@%s>: %s" (nth 1 currentall) (nth 2 currentall)  (nth 3 currentall)))
    (insert (format "\n\nPARSED AS:\n%s" (nth 4 currentall)))
    (insert (format "\n\nFULL RESPONSE:\n%s\n" (nth 7 currentall)))
    (insert (format "\n\nUNDO INFORMATION:\n%s\n" (erbot-trash-formatted-episode)))
    (add-to-list 'erbot-buffers-to-save (buffer-name))
    (setf erbn-log-save-last (time-to-seconds))
    nil))


(defun erbn-fulllog-make-permanent ()
  (let*
      ((f1 (expand-file-name (concat erbn-currentid ".txt") erbn-tmplog-path))
       (f2 (expand-file-name (concat erbn-currentid ".txt") erbn-fulllog-path)))
    (copy-file f1 f2 t t t t)))
       
    

(defun erbot-trash-formatted-episode ()
  "Generate a formatted output of any trash generated during the current episode. Used with erbn-fulllog-new"
  (mapconcat
   (lambda (trashterm)
     (format "%s:\n\n%s\n" trashterm
	     (fsi-describe trashterm)))
   erbn-trashcurrent
   "\n")
  )


(defun erbn-format-log (l2)
  ;; (format "%S" l2)
  (concat "("
	  (mapconcat
	   (lambda (a) (format "%S" a))
	   l2
	   "\n ")
	  ")"))


(defun erbn-log-new (ls)
  (setq erbn-logging t)
  (let ((ff (erbn-log-file))
	(tts (time-to-seconds))
	(l2 ls)
	)
    (find-file ff)
    (goto-char (point-max))
    
    (loop for ii from 1 to erbn-log-depth do (setf l2 (list 'sub l2)))
    (insert (erbn-format-log l2))
    (insert "\n===================================================================================\n")
    ;; (insert "\n<p><p>\n")
    ;; We insert a safety check here. Ideally, this should not be
    ;; needed. But, sometimes, one fsi- function calls another which
    ;; lead to more logging.  If we keep saving the file every
    ;; millisecond, that will slow us down.  That behavior - of fsi's calling other fsi's - should
    ;; ideally be weeded out.  OTOH, even users THEMSELVES may write functions that call others, so we can't really weed this out.
    ;; Now that we don't call (save-buffer)< we might not need this time-check.
    ;; That is, the save is delayed in any case. 
    ;; We retain a 1 second time delay in any case. So, if there are many back to back bot commands waiting, it won't waste time in repeated saving.
    (when (> (- tts erbn-log-save-last) 1)
      ;; delayed save.
      (add-to-list 'erbot-buffers-to-save (buffer-name))
      (setf erbn-log-save-last tts))
    t))

(defun erbn-log (&rest args)
  "Called by rw changes.  After logging, all actual code should be wrapped by a deepen, so that sublogs are properly indented.

NB that this function SETS currentid! Thus, currentid will get set even if we call something like (erbn-log-inhibit (fsi-setq )).  Now, imagine, at the top level, you do a (erbn-pf-load) before initiating the bot. The fsi-setq functions, etc. in the file will lead to currentid getting set globally!"

  (setf erbn-logging t) ;; Note that this is only temporary within this interaction.
  (setf erbn-fulllogging t) ;; If we are logging, we definitely wish to get a fulllog as well, even if no reply ('noreply or nil) is generated.
  (fsi-public-test)
  (erbn-currentid-set) ;; We need to set this here since log also stores the fulllog-currentid as a reference. However, unfortunately, erbn-log can get called globally when initiating. Thus, all initiation should be wrapped in erbot-with-globals. 
  (when erbn-log-p
    (erbn-log-new
     (list (erbn-meta)
	   args erbn-task-current
	   (concat "" (erbn-currentid-txt erbn-currentid) "")
	   "<output here>"))))
     ;; (concat
     ;;  (erbn-htmlize-escape (list (erbn-meta) args  erbn-task-current))
     ;;  " "
     ;;  (format "%s" (list (concat "\n" (erbn-htmlize-url (erbn-currentid-txt erbn-currentid)) "\n")))
     ;;  " "
     ;;  (erbn-htmlize-escape (list "<output here>"))))))

(defun erbn-htmlize-escape (txt)
  "Unused. Similar to url-insert-entities-in-string, but nih'd.
Note that since erbutils-replace-strings-in-string operates in succession, it is important to handle & first."
  (erbutils-replace-strings-in-string
   '("&" "<" ">" "\"")
   '("&amp" "&lt;" "&gt;" "&quot;" )
   (format "%s" txt)))


(defun erbn-htmlize-url (txt)
  "Unusude."
  (format "<a href=%s>%s</a>" txt txt))


(defun erbn-meta ()
  
  (list (format-time-string "%Y%m%d.%H%M%S" nil t)
	erbn-nick erbn-tgt
	;; erbn-msg should always be a string, but just in case it isn't, set it.
	(format "%s" erbn-msg)))


(defmacro erbn-unlog (&rest code)
  "A shortcut for .. see code."
  `(erbn-log-inhibit (erbn-log-deepen ,@code)))

(defmacro erbn-log-deepen (&rest code)
  `(let ((erbn-log-depth (+ 1 erbn-log-depth)))
     ,@code))

(defmacro erbn-log-enable (&rest code)
  `(let ((erbn-log-p t))
     ,@code))



(defmacro erbn-log-inhibit (&rest code)
  `(let ((erbn-log-p nil))
     ,@code))

(defun fsi-public-test ()
  (let ((match (fsi-channel-public-p)))
    (unless match 
      (error "[public-test] This action is only allowed in the presence of others, in a public channel such as #emacs or #fsbot."))
    ;;match ;; for debugging.
    t))




(defun fsi-C-h (sym &rest thing)
  "Syntactic sugar for some fsbot functions.
;;; 2003-08-16 T15:19:00-0400 (Saturday)   deego
Coded by bojohan on #emacs."
  (cond
   ((eq sym 'f)
    (apply 'fsi-df thing))
   ((eq sym 'k)
    (apply 'fsi-dk thing)) 
    ((eq sym 'c)
     (apply 'fsi-describe-key-briefly thing))
    ((eq sym 'w)
     (apply 'fsi-dw thing))
    ((eq sym 'v)
     (apply 'fsi-dv thing))))


(defun fsi-wtf-is (&optional term &rest args)
  (unless term 
    (error "Syntax: wtf TERM"))
  (require 'wtf)
  (funcall 'wtf-is (format "%s" term)))



(defalias 'fsi-wtf 'fsi-wtf-is)

;; unknown if safe. Disable for now. 
;; (defalias 'fsi-rx 'rx)
;; unsafe? 
;; (defalias 'fsi-rx-form 'fsi-rx-from)
;; unsafe?
;; <forcer> deego: (rx-to-string '(eval (with-current-buffer "*scratch*" (insert "foo")))) works, for example
;; (defalias 'fsi-rx-to-string 'fsi-rx-from)


(defun fsi-distance-fromcharset (charset &rest args)
  (let ((msg (string-to-list (fsi-downcase (fsi-stringify args))))
	(sum 0))
    (mapcar (lambda (a)
	      (when (not (member a charset)) (incf sum)))
	    msg)
    sum))




(defun fsi-distance-dvorak (&rest args)
  (apply 'fsi-distance-fromcharset 
	 '(?a ?o ?e ?u ?i ?u ?d ?h ?t ?n ?s 32)
	 args))

(defun fsi-distance-qwerty (&rest args)
  (apply 'fsi-distance-fromcharset
	 '(?a ?s ?d ?f ?g ?h ?j ?k ?l 59 ?' 32)
	 args))

(defun fsi-distance-colemak (&rest args)
  (apply 'fsi-distance-fromcharset
	 ;; supplied by defanor, thanks!
	 '(97 114 115 116 100 104 110 101 105 111 32)
	 args))




(defun fsi-distances-calculate (&rest args)
  "Calculate distances from dvorak, qwerty, and any other charsets, and update a running tally. Return a list in order."
  (let ((dv (apply 'fsi-distance-dvorak args))
	(qw (apply 'fsi-distance-qwerty args))
	(co (apply 'fsi-distance-colemak args))
	)
    (incf fsi-distance-dvorak dv)
    (incf fsi-distance-qwerty qw)
    (incf fsi-distance-colemak co)
    (list dv qw co)))



(defun fsi-argmin1 (ls)
  "hacky.. need to make efficient..."
  (let ((mm (apply #'min ls))
	(ctr -1))
    (loop for item in ls
	  do
	  (incf ctr)
	  (when (= item mm) (return)))
    ctr))

(defun fsi-battle-dvorak-qwerty-colemak (&rest args)
  (let* ((dvqwco (apply 'fsi-distances-calculate args))
	 (dv (first dvqwco))
	 (qw (second dvqwco))
	 (co (third dvqwco))
	 (winner (fsi-argmin1 dvqwco))
	 (weirdp (= winner 1 )) ;; qwerty.
	 (goodp (not weirdp))
	 (winname (nth winner '("Dvorak" "Qwerty" "Colemak")))
	 (str2 (concat winname " wins"))
	 )
    (when goodp
      (setf str2 (concat str2
			 (fsi-random-choose '(" again!" ".") '(1 1)))))
    (when weirdp
      (setf str2 (concat str2 
			 (fsi-random-choose '(". Really?" ".") '(1 1)))))
    (cond 
     ((null args) (error "Syntax ,war \"<some string>\""))
     (t (concat
	 str2 " "
	 (format "Distance.. Dvorak:Qwerty:Colemak::%d:%d:%d.   Overall.. %d:%d:%d"
		 dv qw co
		 fsi-distance-dvorak
		 fsi-distance-qwerty
		 fsi-distance-colemak
		 ))))))

(defun fsi-battle-reset ()
  (setf fsi-distance-dvorak 0)
  (setf fsi-distance-qwerty 0)
  (setf fsi-distance-colemak 0)
  )

(defalias 'fsi-battle 'fsi-battle-dvorak-qwerty-colemak)
;; (defalias 'fsi-fight 'fsi-battle-dvorak-qwerty)
(defalias 'fsi-war 'fsi-battle-dvorak-qwerty-colemak)




(defun fsi-emacs-uptime (&rest args)
  "Accurate!"
  (format-seconds "%Y, %D, %H, %M, %z%S."
		  (+ erbn-emacs-uptime-offset
		     (float-time
		      (time-subtract (current-time) before-init-time)))))

 
;; (erbutils-defalias-i '(make-list) 2)
(defun fsi-make-list (nn init)
  (when (> nn 9999)
    (error "When called with a large argument, 'make-list crashes emacs27, and therefore, fsbot. Please make a smaller list."))
  (make-list nn init))


;; Does work for things like mapconcat.
(defalias 'fsi-lambda 'lambda)




;; Try to make kbd work. From what I can tell atm (on 20150416), kbd is identical to read-
;;(defalias 'fsi-kbd 'read-kbd-macro)

(defun fsi-kbd (&optional arg)
  (unless arg (error "Syntax: kbd \"keys\""))
  (let ((ans (read-kbd-macro arg)))
    ;; (erbot-type-check ans)))
    (format "%S" ans)))
		     


;; may improve with time.
(defun fsi-pluralp (string)
  "Rudimentary plurality check.

Details:
  nonstandard: (think faqs): js, qs, xs, zs
  bs, cs, ds, fs, gs, hs, ks, js, ls, ms: ns, ps, qs (faqs), rs: ts, vs, ws, ys, plural, 
  those ending in as, is, os, ss, us tend NOT to be plural.
  non -s plurals: 
    -oci (loci, foci)
    -ae, 
    -enda (addenda). However, in the modern age, agenda is singular, so except that later.
    -ia (cilia, bacteria, criteria)
    -oa (protozoa)
    -ese (these, geese)
    -ata (data, errata)
  Exceptions: 
     - sia (example: amnesia)
  - deego, 2014 and onwards. 

"
  (let ((string (format "%s" string))
	(case-fold-search nil)) ;; that is, case sensitive!
    (and (string-match "\\(ata\\|oci\\|es\\|ls\\|ae\\|enda\\|ia\\|oa\\|ese\\|[bcdfghjkmntpqrvwyxz]s\\)$" string)
	 ;; used downcase to make the next check case-insensitive.
	 ;; example: PyMacs.
	 (not (string-match "\\(ymacs\\|emacs\\|olmes\\|genda\\|ulia\\|sia\\)$" (downcase string))))))




(defmacro erbn-trash-inhibit (&rest code)
  `(let ((erbn-trash-p nil))
     ,@code))


(defmacro erbn-untrash (&rest code)
  `(erbn-trash-inhibit (erbn-unlog ,@code)))

(defun erbot-owner-assert ()
  (when (not (member erbn-nick erbot-owner-nicks))
    (error "You don't appear to be authorized to perform this action.")))

(defun erbot-whitelist-assert ()
    (error "to be extracted here. currently, part of fsi-lispify."))


	       


(defun fsi-EmptyRecycleBin()
  ;; 20160627 no need to owner-assert any longer. All relevant trash is also logged to web, after all.
  ;; (erbot-owner-assert)
  (erbn-log 'EmptyRecycleBin)
  (erbn-untrash
   ;; we are in erbn-untrash here for several reasons:
   ;; (1) User has asked to clean the trashcan. It would be weird to collate all trash in one large trash and add it right back.
   ;; (2) That way, the size of trashcan would never decrease.
   ;; (3) All trashes are stored permanently on web in any case, plus undo information alongside the entries they were generated from.
   ;; (4) It would have been nice to temporarily generate trash, so the weblog of this one entry can store its own undo-list, but because of reasons 1..3, we don't want to do it *here*.
   ;; (5) 20170709 Update: This is WRONG, I think. User could, maybe, combine this with another fs-command to permanently wipe the undo-list of that function. TODO test. 
   (let*
       ((search (fsi-trashlist))
	(n (first search))
	(res (second search)))
     (loop for term in res do
	   (fsi-forget term "all"))
     (format "Deleted %d FsTrash items.  [%s]" n (fsi-abbrev res 80)))))

(defalias 'fsi-trashtrash 'fsi-EmptyRecycleBin)

(defun fsi-trashlist ()
  "Used by fsi-emptyrecyclebin above."
  ;; note that trashnew makes a random string with 10 random letters, not 6. 
  (fsi-search-basic  "^fstrash[0-9]\\{6\\}\\.[0-9]\\{4\\}.[a-zA-Z]\\{6\\}")) 


(defun erbn-trash (&optional meta1 meta2 notes)
  (ignore-errors
    (erbn-trash-new meta1 meta2 notes)))

(defun erbn-trash-new (&optional meta1 meta2 notes)
  "Meta1 should contain info on what produced the notes.  It should usually be a tuble. The first part should detail the action that caused this. The second part should contain info on the item being forgotten. Thus, the second part should be something like:  '(term termname) or '(defun defunname)."
  (unless (listp notes) (setf notes (list notes))) ;; You should have supplied us a list, really.
  (when erbn-trash-p
    (erbn-untrash
     (let*
	 ((meta0 (erbn-meta))
	  (notes
	   (cons (format "%S" (list meta0 meta1 meta2))
		 notes))
	  (trashterm (concat "FsTrash" (format-time-string "%y%m%d.%H%M.")
			     (erbutils-randomstring 10)))

	  ;; NOTE that the complexity of this randomstring is NOT 52^10. It is ONLY 26^10. The reason is that two terms cannot exist if their (downcase ) 's are identical.

	  
	  ;;(format "%04d" (random 1000))
	  
	  ;; (trashterm "FsTrash123") ;; to test overwriting.  Works.
	  ;; This should work ALMOST all the time. If it does not, overwrite it below. 
	  (try1 (ignore-errors
		  (fsi-set-notes trashterm notes)
		  t)))
       (unless try1
	 (ignore-errors (fsi-forget trashterm "all"))
	 (fsi-set-notes trashterm notes))
       (add-to-list 'erbn-trashcurrent trashterm)
       t))))


(defalias 'fsi-randomstring 'erbutils-randomstring)




(defun erbn-OctaveHelp (str)
  "Work in progress. 20150927." 
  (error "Not done. insecure. use inferior octave, etc. etc.")
  (let* ((str (erbutils-replace-string-in-string str " " "" str))
	 (str (shell-quote-argument str)) 
	 (octcmd (concat "\"help " str "\""))
	 (cmd (format "octave -q --eval %s" octcmd)))
    (shell-command-to-string cmd)))


(defun fsi-op ()
  "Try, silently, to become the op on the current channel.  This likely does not work on libera yet."
  (when (stringp erbn-tgt)
    (erc-cmd-MSG (concat "chanserv" " " "op " erbn-tgt))))


(defun fsi-deop ()
  "Try, silently, to deop on the current channel."
  (when (stringp erbn-tgt)
    (erc-cmd-MSG (concat "chanserv" " " "deop " erbn-tgt))))



(defalias 'fsi-lmg 'fsi-lmgtfy)

(defun fsi-lmgtfy     (&rest args)
  "NB that this could easily be a userfunction. Everything used here is available to users."
  (concat "http://lmgtfy.com/?q="
	  (mapconcat 'fsi-hex-string (fsi-stringifyc args) "+")))


(defun fsi-nbutlast (ls &optional n)
  "An fsbot-version of nbutlast. NOTE that this returns a copy, and does NOT modify the original list. 
Since we do NOT modify the original list, note that fsi-butlast is identical to fsi-nbutlast."
  (assert (listp ls))
  (when n (assert (numberp n)))
  (let ((ls1 (copy-tree ls)))
    (butlast ls1 n)))

(defalias 'fsi-butlast 'fsi-nbutlast)


;; (defalias 'fsi-rx-to-string 'rx-to-string) 
;; (defun fsi-rx (&rest args)
;;   (let ((result (apply 'fsi-rx-to-string args)))
;;     (format "%S" result)))

(defun fsi-translate-via-rudybot (&rest args)
  (format "rudybot: t8 %s" (fsi-stringifyd args)))
(defalias 'fsi-tr 'fsi-translate-via-rudybot)
(defalias 'fsi-t8 'fsi-translate-via-rudybot)
(defalias 'fsi-translate 'fsi-translate-via-rudybot)




(defun fsi-describe-package-literally (&optional pkgname &rest args)
  ;; (error "Programmed by deego 20241012 in response to a feature request from mekeor, but disabled pending a code review.")
  (unless pkgname
    (error "Syntax - , describe-package pkgname "))
  (if (stringp pkgname)
      (setq pkgname (erbn-read pkgname)))
  (unless (symbolp pkgname)
    (error "Pkgname should be a symbol."))
  (let ((description
	 (ignore-errors
	   (with-temp-buffer 
             (describe-package-1 pkgname)
             (buffer-substring-no-properties (point-min) (point-max))))))
    (if description
	description
      (error "Unable to retrieve description for package %s" pkgname))))



(defun fsi-describe-package-fully (&rest args)
  "describe pkg, but move some relevant lines to the top of the resulting page."
  (erbn-__describe-package-rearrange-string
   (apply 'fsi-describe-package-literally  args)))

(defun fsi-describe-package-succinctly (&rest args)
  "describe pkg, but try to ONLY focus on certain pertinent fields."
  (erbn-__describe-package-rearrange-string
   (apply 'fsi-describe-package-literally  args)
   'elide))


(defalias 'fsi-describe-package 'fsi-describe-package-succinctly)

(defalias 'fsi-dpf 'fsi-describe-package-fully)
(defalias 'fsi-dps 'fsi-describe-package-succinctly)
(defalias 'fsi-dpl 'fsi-describe-package-literally)
(defalias 'fsi-dp 'fsi-describe-package)


(defun erbn-__describe-package-rearrange-move-line-to-beginning (string keyword )
  "Move the line starting with KEYWORD to the beginning of STRING."
  (let ((lines (split-string string "\n"))
        (found-line nil)
	(reg (concat "^\\s-*" keyword))
        (result '()))
    (dolist (line lines)
      (if (and (not found-line) (string-match reg line))
          (progn
	    ;; (setq found-anything true))
	    (setq found-line line))
        (push line result)))
      ;; Prepend the found line to the result list without reversing
    (if found-line
	(setq result (cons found-line (reverse result)))
      (setq result (reverse result)))
    ;; Concatenate the result back into a single string
    (list (mapconcat 'identity result "\n")
	  found-line)))




(defun erbn-__describe-package-rearrange-string (string &optional elide)
  "Rearrange STRING - output of describe-package - by moving certain relevant fields to the beginning.
If ELIDE , then keep ONLY the results from keywords, provided at least one of them matches. That is used for describe-package-succinctly."
  (let ((keywords '("Website:" "Summary"))
	(lines nil)
	(matchedtext ""))
    (dolist (keyword keywords)
      (let* ((newstr_and_match
	      (erbn-__describe-package-rearrange-move-line-to-beginning string keyword)))
	(setq string (first newstr_and_match))
	(push (second newstr_and_match) lines)
	))
    (if (not elide)
	string
      (progn
	(setq lines (reverse (remove nil lines)))
	(if lines
	    (mapconcat 'identity lines "\n")
	  string)))))

	
	





(provide 'erbc7)
(run-hooks 'erbc7-after-load-hook)



;;; erbc7.el ends here
