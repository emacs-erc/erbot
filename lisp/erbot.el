;;; erbot.el --- Another robot for ERC.
;; Time-stamp: <2006-08-24 11:11:11 deego>
;; Emacs Lisp Archive entry
;; Filename: erbot.el
;; Package: erbot
;; Authors:  David Edmunston (dme@dme.org) , Dave Goel (deego3@gmail.com)
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; Maintainer: Dave Goel


;; Version:
;; Keywords: ERC, IRC, chat, robot, bot

;; Copyright (C) 2002-2018 Deepak Goel,  David Edmunston, FSF

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.




;; David Edmunston's original commentary for erc-robot.el
;; Installation:

;; The robot uses hooks to gain access to ERC.  The following need to
;; be executed after ERC has loaded:

;;     (load-library "erc-robotbot")


;; It is particularly important that the remote robot function is added
;; to the tail of the PRIVMSG hook.


;;; History:
;; erc-robot was irc robot skeleton from DME here: https://www.emacswiki.org/emacs/erc-robot.el This was used to initially populate the current file
;; Later: most erbot-related functionality has been moved out to other files, to functions such as erbot-remote1, etc.  This file is now much closer to DME's original erc-robot.el, and therefore, dme is being restored as the first author.
;;; Bugs:

;;==========================================
;;; Code:
(require 'cl)



;; Main function. 
(defun erbot-remote (proc parsed)
  "
original docs: Implements a simple robot for erc.  Messages to the robot are of the form:
\"nick: !command args\", where:
nick	- the nickname of the user who is the target of the command,
command	- the specific command,
args	- arguments to the command (optional).

For newer erc, see `erbot-on-new-erc-p' and read the specs of
the new erc-backend functions."
  (save-excursion 
    (set-buffer (process-buffer proc))
    (erbot-with-globals 
     (erbot-remote1 proc parsed))))

(defun erbot-remote1 (proc parsed)
  (let* (
	 (erbn-buffer (erc-server-buffer))
	 (sspec (cond (erbot-on-new-erc-p
		       (erc-response.sender parsed))
		      (t (aref parsed 1))))
	 (userinfo (erc-parse-user sspec))
	 (nick (erbutils-remove-text-properties-maybe (nth 0 userinfo)))
	 ;; bind fs-nick in a let.. so that changes to fs-nick are
	 ;; independent and do not affect each other.. when it is
	 ;; parsing too many messages once..
	 (fs-nick nick)
	 (erbn-nick nick)
	 (cmdargs (and erbot-on-new-erc-p
		       (erc-response.command-args parsed)))
	 (tgta 
	  (erbutils-remove-text-properties-maybe 
	   (cond (cmdargs
		  (nth 0 cmdargs))
		 (t (aref parsed 2)))))
	 (tgt (if (equalp tgta (or (erc-current-nick) erbot-nick))
		  nick
		tgta))
	 (erbn-tgt tgt)
	 (fsi-tgt tgt)
	 (fs-tgt tgt)
	 (msg 
	  (erbutils-remove-text-properties-maybe 
	   (cond (cmdargs
		  (nth 1 cmdargs))
		 (t (aref parsed 3)))))
	 (erbot-end-user-nick nick)
	 (csys     (if (fboundp 'erc-coding-system-for-target)
		       (erc-coding-system-for-target tgt)
		     'utf-8))
	 (code-in  (if (consp csys) (cdr csys)  csys))
	 (code-out (if (consp csys) (car csys)  csys))
	 ) ;; completes the let* part of let*
    ;; changing the structure here..
    ;; also changing erbot-command to erbot-reply..
    ;; from now on, erend-main will take care of what to reply..
    ;; erbot-reply will simply take the reply and reply that...
    ;; should not be setq.. else other invocations may change it..
    ;;(setq erbot-end-user-nick nick)
    
    (setq erbot-end-user-nick-latest erbot-end-user-nick)
    ;;(setq fs-tgt tgt)
    ;;(setq erbn-tgt tgt)
    
    ;;(setq fs-nick nick)
    ;;(setq erbn-nick nick)
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; make sure we decode the raw text we received...
    (unless (multibyte-string-p msg)
      (setq msg (decode-coding-string msg code-in)))
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (setf erbn-msg msg) 
    (let ((msgg
	   (erbeng-main msg proc nick tgt nil userinfo)))
      ;; erbot-reply needs a correct buffer...
      (set-buffer (process-buffer proc))
      (erbot-buffers-save)
      (cond
       (erbot-quiet-p nil)
       ((and erbot-quiet-target-p-function
	     (funcall erbot-quiet-target-p-function tgt nick msg))
	nil)
       (t (erbot-reply  ;; This will reply, create fulllog, paginate, set ids, and finally, rotate lastids.
	   msgg
	   proc erbn-nick erbn-tgt msg nil
	   ))) ;; cond ends here.
      (erbot-buffers-save) ;; erbeng-reply now creats new buffers to save!
      (when erbn-logging
	(erbn-fulllog-make-permanent))
      ))
  nil)






(provide 'erbot)
(run-hooks 'erbot-after-load-hooks)



;;; erbot.el ends here
