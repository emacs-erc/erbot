;;; erbc8.el: Octave interface for octave
;; Copyright (C) 2012 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc8.el
;; Package: erbc8
;; Author: D. Goel <deego3@gmail.com>
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 


;; You should define an octave function evalsafemy(). Make sure to make it very safe within octave. The fsbot code simply calls evalsafemy() on the string.


;;; Real Code:


(defun fsi-octave-eval (code &rest ignore)
  (erbn-log 'octave-eval code ignore)
  (erbutils-enabled-check erbot-octave-p)
  (setf code (fsi-stringifyc code))
  (assert (stringp code))
  ;; (message "%s" code) ;; for debugging.
  (with-temp-file erbn-octave-eval-file
    (insert code))
  ;; Need something like:
  ;; octave --no-gui --silent --no-window-system --eval "(evalsafemy(\"1+2\",\"\"));" 
  ;; THIS WORKS:
  ;;  (shell-command-to-string "octave --no-gui --silent --eval \"evalsafemy(\\\"1+2\\\",\\\"\\\")\" ")
  (let* ((c1 "timeout 20s octave --no-gui --no-line-editing --no-window-system --silent --eval ")
	 (c2 (concat " \"evalsafemy(\\\"\\\",\\\"" (file-truename erbn-octave-eval-file) "\\\" );\" " ))
	 (c3 (concat c1 c2))
	 (res1 (shell-command-to-string c3))
	 (res2 (if (= 0 (length res1)) "ERROR.4.EMPTYRESULT: because of (1) a terminating semi-colon or (2) a timeout." res1))
	 (goodp (not (string-match "WONTRUN" res2))) ;; was unsafe code tried?
	 (failp (erbn-octave-fail2ban goodp))
	 (resfull (if failp (concat failp res2) res2))
	 )
    resfull))



(defun erbn-octave-fail2ban (goodp)
  (let ((ls (erbutils-fail2ban erbn-octave-ctrbad erbn-octave-time goodp)))
    (setf erbn-octave-ctrbad (first ls)
	  erbn-octave-time (second ls))
    (if (>= erbn-octave-ctrbad erbn-octave-fail2ban-tolerance)
      ;; silently fail2ban.
	(progn
	  (setf erbot-octave-p nil)
	  "[I have now disabled any further octave evals.] "
	  )
      nil)))
    

(defun erbot-octave-toggle ()
  (interactive)
  (cond
   ((null erbot-octave-p)
    (setf erbot-octave-p t erbn-octave-time (current-time) erbn-octave-ctrbad 0)
    (message "Enabled octave."))
   (t
    (setf erbot-octave-p nil)
    (message "Disabled octave."))))
   

(defalias 'fsi-oct 'fsi-octave-eval)
(defalias 'fsi-oe 'fsi-octave-eval)


 
(provide 'erbc8)




;;; erbc8.el ends here
