;;; erbc5.el --- continuation of erbc.el
;; Time-stamp: <2007-11-23 11:30:12 deego>
;; Copyright (C) 2003 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc5.el
;; Package: erbc5
;; Author: D. Goel <deego3@gmail.com>
;; Keywords:  
;; Version:  
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version: 

(defconst erbc5-home-page
  "http://gnufans.net/~deego")


 
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;; See also:


(defconst erbc5-version "0.0dev")
;;==========================================
;;; Requires:
(eval-when-compile (require 'cl))

;;; Code:

(defgroup erbc5 nil 
  "The group erbc5."
  :group 'applications)
(defcustom erbc5-before-load-hook nil 
  "Hook to run before loading erbc5."
  :group 'erbc5)
(defcustom erbc5-after-load-hook nil 
  "Hook to run after loading erbc5."
  :group 'erbc5)
(run-hooks 'erbc5-before-load-hook)

(defcustom erbc5-verbosity 0
  "How verbose to be.  
Once you are experienced with this lib, 0 is the recommended
value.  Values between -90 to +90 are \"sane\".  The
rest are for debugging."
  :type 'integer
  :group 'erbc5)
(defcustom erbc5-interactivity 0
  "How interactive to be.  
Once you are experienced with this lib, 0 is the recommended
value.  Values between -90 and +90 are \"sane\".  The rest are for
debugging."
  :type 'integer
  :group 'erbc5)
(defcustom erbc5-y-or-n-p-function 'erbc5-y-or-n-p
  "Function to use for interactivity-dependent  `y-or-n-p'.
Format same as that of `erbc5-y-or-n-p'."
  :type 'function
  :group 'erbc5)
(defcustom erbc5-n-or-y-p-function 'erbc5-y-or-n-p
  "Function to use for interactivity-dependent `n-or-y-p'.
Format same as that of `erbc5-n-or-y-p'."
  :type 'function
  :group 'erbc5)
(defun erbc5-message (points &rest args)
  "Signal message, depending on POINTS anderbc5-verbosity.
ARGS are passed to `message'."
  (unless (minusp (+ points erbc5-verbosity))
    (apply #'message args)))
(defun erbc5-y-or-n-p (add prompt)
  "Query or assume t, based on `erbc5-interactivity'.
ADD is added to `erbc5-interactivity' to decide whether
to query using PROMPT, or just return t."
  (if (minusp (+ add erbc5-interactivity))
        t
      (funcall 'y-or-n-p prompt)))
(defun erbc5-n-or-y-p (add prompt)
  "Query or assume t, based on `erbc5-interactivity'.
ADD is added to `erbc5-interactivity' to decide whether
to query using PROMPT, or just return t."
  (if (minusp (+ add erbc5-interactivity))
        nil
      (funcall 'y-or-n-p prompt)))

;;; Real Code:

(defalias 'fsi-listp-proper 'erbutils-listp-proper)
(erbutils-defalias-i '(upcase downcase upcase-initials) 1)

(defun fsi-capitalize (&rest args)
  (let ((obj (erbutils-stringifyd args)))
    (capitalize obj)))


(ignore-errors (require 'calc))


(defun fsi-calc-eval (&optional str)
  "
Note that  even though this function has a with-timeout built into it,
that doesn't save us from a DOS attack..since emacs polls only when
waiting for user input.. 

which is why turned off by default.

"
  (unless (and erbn-calc-p  (not erbot-paranoid-p))
    (error "[calc-eval] Sorry, but i am a bot! not a calc!"))
  (unless str (error "[c-e] Eval what?"))
  (unless (stringp str)
    (setq str (format "%s" str)))
  (with-timeout 
      (erbn-calc-time "That's WAY too much math for me!")
    (calc-eval str)))
    
(defalias 'fsi-calc 'fsi-calc-eval)




(defun fsi-sreg (&rest args)
  (format "%S" 
	  (apply 'fsi-sregex args)))


(defun fsi-sregex (&rest args)
  (cond
   ((and erbn-sregex-p (not erbot-paranoid-p))
    (apply 'sregex args))
   (t
    (error "[sregex] sregexp is disabled in this bot. "))))



(defmacro fsi-ignore-errors-else-string (&rest body)
  "Like ignore-errors, but tells and returns the erros.
\(Improved for me by Kalle on 7/3/01:)"
  (let ((err (gensym)))
    `(condition-case ,err (progn ,@body)
       (error
	(let 
	    ((str 	
	      (message "IGNORED ERROR: %s" (error-message-string ,err))))
	  (ding t)
	  (ding t)
	  (ding t)
  	  (sit-for 1)
	  str)))))


;; more math functions
(erbutils-defalias-i '(mod mod*) 2)
(erbutils-defalias-i '(symbol-name) 1)

;; 2014 deego:
(defun fsi-fact (&rest ignore)
  (let ((n (fsi-notes "stallman-fact-list")))
    (if (and (list n) (> (length n) 0))
	(fsi-random-choose n)
      
      (require 'fact)
      (concat "[]" (fact)) ;; indicates reversion to default. 
      )))


(erbutils-defalias-i '(1+ 1-) 1)


;; 2014 deego: define sort
(defun fsi-sort (list &optional pred)
  (unless pred (setq pred '<))
  (if (not (symbolp pred))
      (error "[sort] PRED is only allowed to be a symbol. The symbol, when sandboxed, should point to a valid function."))
  (setf pred (erblisp-sandbox pred))
  (sort list pred)
  )



(defun erbn_prefix_run (level &rest in_args)
  "Returns expression and any remaining args."
  (error "tbi")
  (let
      (op args op_args arg_args (doing t)
    (if (not in_args)
	(list nil nil)
      (setq op_args (erbn_prefix_op level)
	    (op (car op_args))
	    (in_args (second op_args)))
      (while doing
	(if (not in_args)
	    (setq doing nil)
	  ;; meh. todo finish later.
	  ))))))

(defun erbn_polish_op (level &rest args)
  "Get an op to apply to the remaining args. 
If we are inside an already open xpression, take up to two following args, and pass the rest on to a parent expression.  
Return the result of the expression, and any remaining args that should be passed on to a parent expression."
  (error "tbi")
  )

(defun erbn_polish_arg (level &rest args)
  "Get an arg the remaining args."
  (error "tbi"))

(defun fsi-polish (&rest args)
  "Polish prefix notation evaluator.
You may optioanally skip ANY or ALL the parents in the expression that follows! 
Works like a standard two-arg polish prefix expression evaluator, but is more flexible!
Example: (prefix + 2 3 - 4 5 * 6 7). This works like a standard infix expression, but is more fault-tolerant.
Assumes each op takes two args, but tries to do the right thing otherwise.
You can, of course, fine-tune our interpretation by actually supplying parens when needed!

"
  ;;  20230225 deego
  ;; Ideally, any inside parens should also be parsed using polish, but we simply pass them using lip atm. 

  (error "under works.")
  (car (erbn_prefix_run args)))


  


(defalias 'fsi-sandbox 'erblisp-sandbox)
(provide 'erbc5)
(run-hooks 'erbc5-after-load-hook)



;;; erbc5.el ends here
