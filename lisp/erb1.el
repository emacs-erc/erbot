;;; erb1.el --- Most erbot specific funcitons, and most initial variables.
;; Time-stamp: 
;; Emacs Lisp Archive entry
;; Filename: erb1.el
;; Package: erbot
;; Authors:  Dave Goel <deego3@gmail.com>
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; Maintainer: Dave Goel


(defvar erbot-home-page
  "http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot/")

;; Version:
;; Keywords: ERC, IRC, chat, robot, bot

;; Copyright (C) 2002-2018 Dave Goel

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.





(defalias 'erc-replace-regexp-in-string 'erbutils-replace-regexp-in-string)
(defalias 'erbot-type-ok-p 'erblisp-type-ok-p)
(defalias 'erbot-type-check 'erblisp-type-check)



(defvar erbot-paranoid-p t
  " Meant as a CATCHALL for security. Setting this variable to non-nil
should disable most features. When non-nil, all potentially funny
functions are disabled.  We think these functions are safe, but we
disable them in any case.  We also disable all functions that we can
that may potentially freeze the bot or severly slow it down upon
receiving weird requests.


t by default.  No enablings like erbot-setf-p, etc. will work
unless this is non-nil. If this is non-nil, erbot is paranoid, it will
not allow apply, setf, funcall, sregex, etc. even if the corresponding
variables are turned on.

NOTE: Making this variable nil and later non-nil in the middle of a
running emacs session will NOT make your bot completely paranoid.  You
need to have this function non-nil BEFORE you load erbot. See, for
example, how we define fs-kbd.

A handy utility to go with this function is erbutils-enabled-check. ")




(defun erbot-commentary ()
  "Provides electric help regarding variable `erbot-commentary'."
  (interactive)
  (with-electric-help
   '(lambda () (insert erbot-commentary) nil) "*doc*"))

;;; History:

;;; Bugs:

;;; New features:
(defvar erbot-new-features
  "Help..."
)

(defun erbot-new-features ()
  "Provides electric help regarding variable `erbot-new-features'."
  (interactive)
  (with-electric-help
   '(lambda () (insert erbot-new-features) nil) "*doc*"))

;;; TO DO:
(defvar erbot-todo
  "Current shortcomings:"

)

(defun erbot-todo ()
  "Provides electric help regarding variable `erbot-todo'."
  (interactive)
  (with-electric-help
   '(lambda () (insert erbot-todo) nil) "*doc*"))

(defvar erbot-version "0.0")

;;==========================================
;;; Code:
(require 'cl)

(defcustom erbot-before-load-hooks nil "" :group 'erbot)
(defcustom erbot-after-load-hooks nil "" :group 'erbot)



;; 20211024 move most global variables to erb1.el

;;; from erbc (some):

(defcustom fsi-internal-botito-mode nil
  "Mode to turn on more english-like bunny-behavior"
  :group 'erbc)



(defcustom fsi-internal-parse-error-p
  nil

  "Whether to show lispy errors in term descriptions.                                                                                                                                             
When nil, an error in a  lispy description of a term makes the bot                                                                                                                                     
go to an english mode for the term.                                                                                                                                                                    
When non-nil, we will just display the error.  On a channel full of                                                                                                                                     
lisp hackers, we will want to make this t for users' convenience.")


(defvar fsi-tgt nil "Tgt visible to the end-user, as well as changeable by them.")
(defvar erbn-tgt nil "Tgt NOT changeable by enduser.")
(defvar fs-tgt nil)
(defvar erbn-buffer "")

(defcustom erbn-shell-command-p nil
    "Whether to allow commands that use shell-commands...
Some fsbot commands use shell-commands... shell-commands always mean
possibility of exploits.  andn are disabled by default.

Make this t at your own risk. ")





;;;; others:
(defvar erbn-tmp-sexps ) 
(defvar erbn-tmp-newbody ) 
(defvar erbn-tmpsetq nil)

(defvar erbn-while-max 10000)
(defvar erbn-while-ctr 0)

(defvar erbn-tmp-avar nil)
(defvar erbn-tmp-newargs nil)

(defvar erbn-apptmpa)
(defvar erbn-apptmpb)
(defvar erbn-apptmpc)
(defvar erbn-apptmpd)
(defvar erbn-tmpsymbolp)

(defvar erbn-read-mode nil)
(defvar erbn-read-input nil)

(defvar erbn-internal-botread-prompt "Enter: ")

(defvar erbn-RR-empty-bets (make-hash-table))
(defvar erbn-RR-bullet-bets (make-hash-table))
(defvar erbn-money (make-hash-table))
(defvar erbn-chamber (random 6))

(defvar erbn-auth-bankers 
  '(deego  jlf ))

(defcustom fsi-mod-keywords
  '("kick *me *please")
  "When non-nil, list of keywords/keyphrases to trigger kick for.")

(defvar erbn-calc-time 3)
(defcustom erbn-calc-p nil
  "Enable this variable at your own risk.
Enabling this means that fsbot will do calc operations, but those have
no timeout build in... leading to DOS attacks. ")



(defcustom erbn-sregex-p nil
  "Nil by default for safety. Enable to permit fs-sregex.
I think it is safe, but not 100% sure, so disabled by default. --DG"
  )

;; see erbc7
(defvar erbn-channel-public-list 
  (list "." ;; this disables all checks. Next follow some examples: 
	"^#emacs$" "^#fsbot$" ;; ## example entries of public channels to add.
	"^deego$" ;; example entries of any master nicks to add.
	))


(defvar erbn-log-file-base "~/public_html/botlog/BotLog")

(defvar erbn-log-save-last 0)

(defvar erbn-fulllog-path "/home/fsbot/public_html/logfull/")
(defvar erbn-tmplog-path "/home/fsbot/public_html/logtmp/")

(defvar erbn-fulllog-url "http://gnufans.net/~fsbot/logfull/")
(defvar erbn-tmplog-url "http://gnufans.net/~fsbot/logtmp/")

;; these get updated throughout an fsbot session.
(defvar fsi-distance-dvorak 0)
(defvar fsi-distance-qwerty 0)
(defvar fsi-distance-colemak 0)

(defvar erbn-emacs-uptime-offset (* 10.0e9 (/ (random 100) 99.0)))

(defvar erbn-trash-p t
  "Whether to trash forgotten items instead of deleting them. This variable will be temporarily set to nil internally by, for example, fsi-EmptyTrashNow, etc.")

;; tag bbdb3
(defvar erbn-pf-file "~/public_html/data/NOTuserfunctions.el") ;; please set it in .emacs
;; unused p
(defvar erbn-pf-file-verbatim "~/public_html/data/NOTuserfunctions.el-raw") ;; please set it in .emacs
(defvar erbn-pv-file "~/public_html/data/NOTuservariables.el") ;; please set it in .emacs


(defvar erbot-reconnection-attempts nil)

(defvar erbot-server-buffers nil
  "List of erbot-server buffernames. Populated by erbot-join-servers during the initial joins.  
Not to be confused with `erbot-get-servers'.
")
(defvar erbot-autojoin-timers nil)


(defvar erbot-flood-p nil
  "Rudimentary flood-queue check. Once this variable becomes t, fsbot stops ANY further replies to anything!

This variable is updated via erbot-flood-check during erbot-reply. 
Once this variable becomes true, then all users get ignored during fsi-lispify.

You can re-enable via M-x erbot-flood-toggle
TODO: In future, we would like to disable only for, say, half on hour!

"
  )

(defvar erbot-flood-queue-thresh1 30) ;; all threshes
(defvar erbot-flood-queue-thresh2 10) ;; privmsg threshes


(defvar erbot-flood-debug nil)

(defvar erbot-init-string ""
  "The basic init string.. should be concated to ALL lines of
replies... right at last.. the values it will hold will look like /msg
foo, and will be set by fs-parse-english, when that function
determines it appropriate..
Currently: we do not use it, since we have found a better way to do
those things..

")

(defvar erbot-bbdb-save-p nil "Internal. Delayed saving of bbdb.")
(defvar erbot-buffers-to-save nil
  "Internal. Buffers to save after an operation.")

(defcustom erbot-reply-p t
  "when nil, don't reply")

(defcustom erbot-setf-p nil
  "If you want your bot to allow setf, set this symbol to non-nil at
the beginning of your .emacs")

(defcustom erbot-setf-symbols
  '(caar cadr car cdar cddr cdr eighth elt 
	 first fourth
	 ninth nth
	 nthcdr
	 second
	 seventh sixth
	 subseq substring
	 tenth third)
"Safe symbols for setf...")


(defvar erbeng-msg nil )
(defvar erbeng-proc nil)
(defvar erbeng-nick nil)
(defvar erbeng-tgt nil)




(defvar erbeng-localp nil)
(defvar erbeng-userinfo nil)


(defvar erbot-show-type-p t
  "Whether to show type of non-string objects when replying...

The old behavior was equivalent to having this as nil.")


(defcustom erbeng-reply-timeout 20
  "Time after which the bot times out...")


(defcustom fsi-pv-save-rarity 100000
  "if this is 1000, then file is saved one in a  thousand times... ")


(defvar erbn-pv-needs-saving nil
  "This variable should always be nil globally.  It is let'd in erbot-remote. Local functions such as erbn-setq can then set it to t as needed.")



(defvar erbn-log-p t
  "Whether to log at the current level of recursion.. Note: erbot temporarily sets this to nil internally.")

(defvar erbn-log-depth 0 "Internal. Keeps track of the log depth.")


(defvar erbn-logging nil "Internal. Once set to t within an interaction, always stays t within the interaction. Logging, unlike fulllogging, is restricted to important logs. Are we logging? Similar to erbn-rw.")

(defvar erbn-fulllogging nil "Internal. Once set to t within an interaction, always stays t within that interaction. Fulllogging, unlike logging, happens whenever a reply is generated, no matter how trivial. If we are logging, we ALSO fulllog, even if no reply is generated. Are we fulllogging?")


(defvar erbn-lastall nil
  "An assoc list of last ALL per TARGET. Will be used by erbot-dump, etc.")



(defvar fsi-msglist nil "Message broken into list.  This list may have removed characters like ?  and ,,  No guarantees here.  See fsi-msgsandbot instead.")

(defvar fsi-msgsansbot nil "Current message being parsed, but the invocation part removed.  ")

(defvar fsi-msglistsansbot nil
  "Message broken into list, invocation parts removed.

.. with the invokation parts, like ,, or , or fsbot:, removed.  Thus,
if message taken from the middle of a sentence, then this is the list
from only that part. ")




(defvar fs-lispargs nil
  "Will be used when using the lisp form")



(defvar fsi-lispa nil
  "Will be used when using the lisp form")


(defvar fsi-lispb nil
  "Will be used when using the lisp form")


(defvar fsi-lispc nil
  "Will be used when using the lisp form")

(defvar fsi-lispd nil
  "Will be used when using the lisp form")

(defvar fsi-lispe nil
  "Will be used when using the lisp form")


(defvar erbn-currentall nil
  "Full msg info. Something like (erbn-currentid erbn-nick erbn-tgt erbn-msg erbn-parsedmsg erbn-task-current erbn-reply erbn-reply-full.")
(defvar erbn-currentid  nil "todo. Should always be nil globally. Will be defvarr'd locally.")
(defvar erbn-reply nil)
(defvar erbn-replyfull nil)



(defvar fsi-internal-describe-literally-p nil)


(defvar erbn-msg nil "The exact current message being parsed. ")
(defvar fsi-msg nil "The exact current message being parsed. ")

(defvar fsi-parsedmsg nil "Parse of the current message.")
(defvar erbn-parsedmsg nil "Parse of the current message.")

(defvar erbn-lastall-history nil
  "A history of the bare essentials of all lastalls.   Used for dumps. 

That is, for each tgt, the assoc list here contains a history. Each element of the history two items: (a) The loc. (b) The task.
We cannot afford to carry around the entire lastall data per user, but we CAN carry around the loc datas. Thus, we shall do that! This can then be used to get an arbitrary (dump n) by the user. 

Note that this list does NOT push if the current task is a ,dump or a ,more. That behavior is subject to change.  Also, this behavior is different from that for erbn-lastall. 

Also used by fsi-dump-history.")





(defcustom erbot-ignore-nicks '("^apt[0-9]?$" "bot" "google" "serv")
  "A list of REGEXPS.
Nicks matching these regexps will be ignored by the bot, viz. not
generate replies.

I would suggest including atleast bot, google and serv here to prevent
infinite chat loops with other bots.  :)
"
:type '(repeat regexp)
:group 'erbot)

(defcustom erbot-use-whitelist nil "Use a whitelist for accessing the bot.
Any request from another source will be ignored. If a source is present in whitelist, but also in `erbot-ignore-nicks' it is ignored"
:type 'boolean
:group 'erbot)

(defcustom erbot-whitelist-nicks nil
"List of nicks that have access to the bot. Used only when `erbot-use-whitelist' is non-nil"
:type '(repeat regexp)
:group 'erbot)



(defcustom erbot-owner-nicks '("deego" "deego`")
"List of the nicks that have access to the bot. Used only when `erbot-use-whitelist' is non-nil"
:type '(repeat regexp)
:group 'erbot)



(defcustom erbot-ignore-userinfos "" "list of regex's" :group 'erbot)
(run-hooks 'erbot-before-load-hooks)


(defgroup erbot nil
  "The group erbot"
   :group 'applications)

(defcustom erbot-nick "fsNONE"
    "PLEASE SPECIFY IT IN .EMACS, NOT HERE.                                                                                                                                                                
Changing this in the middle of things                                                                                                                                                                    
may have unspecified and unpleasant results..."
    :group 'erbot)


(defvar erbot-end-user-nick "dummy-nick"
  "just a temporary variable..")

(defvar erbot-end-user-nick-latest "dummy-end-user-nick-latest"
  "just a temporary variable..")





(defcustom erbot-servers-channels
      '(("irc.openprojects.net"
         ("#testopn"
            ))
	(".gnome.org"
         ("#testgnome")
	 ;; optional but:
	 6667
	 ))
      "Servers and channels ..."
      :group 'erbot)



;  (defalias 'erc-replace-regexp-in-string 'erbutils-replace-regexp-in-string))


(defface erbot-face '((t (:foreground "yellow")))
  "Face used for your robot's output."
  :group 'erc-faces)

(defcustom erbot-commands nil
  "A list of robot commands and the functions which implement them."
  :group 'erc
  :type '(repeat (list string (choice (const nil) (const t) string) function))
  )



(defcustom erbot-erbmsg-p nil
  "When true, erball.el loads the erbmsg module by default ")


(defcustom erbot-notify-p nil
  "Set it to t if you want RSS notification
for your erbot. 

Note that even if it is t, we will internally setq it to nil temporarily during
the inner workings of the bot.  ")

;; The next part suggested by forcer, See
;; http://www.kollektiv-hamburg.de/~forcer/erbot-notify.txt, which is
;; also copied here: 

;; erbot should include the following function lists, which are
;; called on these events with the specified arguments:

;;  erbot-notify-add-functions
;;    arguments: nick channel term entry-num entry

(defvar erbot-notify-add-functions nil
  "Functions  to call when an erbot add takes place.  Each of these is
called with the arguments arguments: nick channel term entry-num
entry")

;;  erbot-notify-forget-functions
;;    arguments: nick channel term entry-num entry
;;    If entry-num is 'all, entry is a list of entries


;; SPECS CHANGED!
(defvar erbot-notify-forget-functions nil
  "Functions to call when an erbot forget takes place.  Each of these
is called with the arguments arguments: nick channel term entry-num
entry remaining-entries.  If entry-num is 'all, entry is a list of
entries")

;;  erbot-notify-move-functions
;;    arguments: nick channel old-term new-term

(defvar erbot-notify-move-functions nil
  "Functions to call when an erbot move operation takes place.  Each
of these is called with the arguments arguments: nick channel old-term
new-term ")

;;  erbot-notify-rearrange-functions
;;    arguments: nick channel term from-num from-entry to-num
;;    entries

(defvar  erbot-notify-rearrange-functions nil
  "Functions to call when an erbot rearrange operation takes place.  Each
of these is called with the arguments arguments: nick channel term
from-num from-entry to-num entries.  Entries refers to the rearranged
entries. ")


;;  erbot-notify-substitute-functions
;;    arguments: nick channel term entry-num old-entry new-entry
(defvar erbot-notify-substitute-functions nil 
  "Functions to call when an erbot substitute operation takes place.
Each of these is called with the arguments arguments: nick channel
term entry-num old-entry new-entry")

;;; 2005-08-31 T10:56:27-0400 (Wednesday)    D. Goel
(defvar erbot-nickserv-p nil
  "When t, erbot will load the appropriate erc modules and will try to
auto-identify to nickserv.  
   
If using this, we recommend these settings at the *BEGINNING* of your
bot's .emacs: 

     (setq erbot-nickserv-p t)
     (setq erc-prompt-for-nickserv-password nil) 

     (setq erc-nickserv-passwords
          '((freenode     ((\"mybot\" . \"mypassword\")))))

See this page for more details: 
http://www.emacswiki.org/cgi-bin/wiki?ErcNickserv
")





;;  erbot-notify-merge-functions
;;    arguments: nick channel old-term new-term new-entries
;; NOW CHANGED SPEC!
(defvar erbot-notify-merge-functions nil
 "Functions to call when an erbot merge operation takes place.
Each of these is called with the arguments arguments: nick channel
from-term to-term from-entries to-entries final-entries")






(defvar erbot-quiet-p nil
  "When non-nil, the erbot only listens, never replies")



(defvar erbot-quiet-target-p-function nil
  "A function.   The function should take up to 3 arguments, TARGET
\(channel) , nick and msg.  If it returns non-nil, then erbot will
listen and do everything but never reply back.")




(defvar erbot-on-new-erc-p nil
  "Whether we use erc >1.660 with new erc-backend.
The value should not be set but is auto-guessed within
`erbot-install'.")

(defvar fsi-inside-more-p nil
  "Internal variable. Gets its own scope inside erbot-remote. Temporarily set to t when inside more, by functions like fsi-more.")


(defvar erbn-task-history nil
  "Internal variable.
An assoc list of target with task history per target.")

(defvar fsi-prestring  "")
;; (make-variable-buffer-local 'fsi-prestring)

(defvar erbn-features-whitelist nil "Set in .emacs")


(defvar fsi-found-query-p nil
  "internal..  should be normally set to nil.
When non nil, means that the msg was not meant to the bot, so the
reply please be abbreviated. ")

(defvar fsi-internal-addressedatlast nil
  "internal.. normally nil")

(defvar fsi-internal-original-message ""
  "internal")

(defvar fsi-internal-message-sans-bot-name ""
  "internal")

(defvar fsi-internal-max-lisp-p nil)


(defcustom fsi-internal-parse-preprocess-message-remove-end-chars
  ;; remove trailing ^A's that occur on action strings...
  (list 1)
  "")

(defcustom fsi-web-page-title-p nil
  "Change it to t to enable the erbot to look up the title of urls
posted in a channel.  When string, will be matched against target.")

(defcustom fsi-m8b-p nil
  "Change it to t for the magic 8-ball... define m8b then of
course...
When string, will be matched against target. "
)

(defvar erbn-dead-check-p nil
  "If non-nil, we will not reply to people who have shot themselves
using mark-dead or russian roulette.  These people need to be revived
first. Of course, like any magic, revival sometimes works, and
sometimes doesn't.")


(defvar erbn-internal-midsentence-p nil
  "Internal variable. True (temporarily, within erbot-remote) if ,, was discovered mid-sentence. 
In this case, based on this variable, some behavior is different.

For example, we never address querer directly in this case. The reason is that the querer is talking to another party, so, fsbot's query is meant directed for the other party in this case.")

(defvar fsi-flame-target nil)

(defvar erbnocmd-describe-search-p t)

(defvar fsi-internal-doctor-rarity 80
  "A large number(1--100) means rarer doctor inovcation upon no matches."
  )

(defvar erbn-set-add-all-p nil
  "The default value is nil. Used to be fsi-...")

(make-variable-buffer-local 'erbn-set-add-all-p)


(defvar fsi-internal-limit-line-length 318
  "Suggested value: (multiple of 80) minus 42 .. suggested: 210.  deego's fsbot currently uses 413. Note that 433 leads to long lines getting cut off")

(defvar fsi-internal-limit-length
  400
 "A multiple of fs-internal-fill-column .. we suggest: double of it..  note
that the actual limited-length will be more than this number---it may
be upto double of this number depending on how the formatting is done.
viz: we shall go to the line containing this point, and include the
entire line.
")

(defvar fsi-limit-lines-reset-value 5 "")
(defvar fsi-limit-lines 5 "") ;; local value. we immediately set it to reset-value below. And, reset-globals will do the same. 
(setf fsi-limit-lines fsi-limit-lines-reset-value) ;; 20160304 reduce from previous 8.

(defvar fsi-dunnet-mode nil
  "")

(make-variable-buffer-local 'fsi-dunnet-mode)

(defvar fsi-internal-fill-column 300
  "Default is to disable filling.  The receipient should be able to
fill the way they like.
should be <= fsi-internal-limit-length, else we might set it to be during the
code.
also, a good idea to keep it < erc's builtin flood protection length,
else your lines will get broken during middle of words by ERC.
Thus, keep it below, say 350."
)

(defvar erbot-kbd-p nil
  "Whether to enable kbd.

Note that making this non-nil can lead to vector results. For
example, (kbd \"<home>\"), (thanks to fledermaus).")


(defvar erbn-more nil
  "Alist of pending more-strings per target.  Each target is a
string. ")
;;(make-variable-buffer-local 'fsi-more)

(defvar erbn-morep nil
  "Alist of bools per target. Whether the last command led to a morep.")

(defvar fsi-internal-directed nil)

(defvar erbn-symbol-function-chase-level 0)
(defvar fsi-internal-google-redirect-p nil)

(defvar erbn-merge-redirect-p t
  "When true, merging also redirects.")

(defvar erbn-nicks-dead nil)

(defvar fs-nick "")
(defvar erbn-nick "")

(defvar erbn-task-current ""
  "A task-indicator. 
Internal variable. Gets its own scope inside erbot-remote. A very brief description of what the bot is doing atm. 
Currently only populated with describing fields. We may populate it for other things as well, with time. When not empty, changes the description of more. The latter to be implemented.

Though it is internal, users can query this variable via (fs-task-current) - that's mostly for debugging. 
This variable is set ONLY during execution of certain functions. For example, fsi-ff sets it. 

Thus, If you (progn (ff 'a) (fv 'b) (+ 1 2)), then during the final execution of (+ 1 2), the task will be incorrectly set to fv:b.

This indicator is used for minor things: 

(a) If fsbot conducts a ,more and the previous command did not have any paging left, then fsbot picks up on the preceding incomplete pagination. In that case, it prefixes the output with the preceding task-name, so users know what it's paginating.

(b) If the current task is fsi-m or fsi-dump, then maybe we will NOT set erbn-lastall. That's not operational atm, and is still under consideration.

"  )



(defvar erbn-trashcurrent nil "List of any new trash items generated within the current episode.")

(defvar erbn-rw nil "Is the current episode rw? UPDATE: see erbn-logging instead.")




;; defvars from erbc8.
(defvar erbn-octave-eval-file "~/octave/emacseval.octave")

(defvar erbot-octave-p nil
  "Whether to (safely) eval octave commands.  If you want this functionality, install octave, ensure that the timeout command works, and set this variable to t.")
(defvar erbn-octave-time (current-time))
(defvar erbn-octave-ctrbad 0)

(defvar erbn-octave-fail2ban-tolerance 2)

;; defvars from erbc9:
(defvar erbot-rx-p nil
  "Whether to allow an rx-interface for erbot.
Should probably remain nil except when playing around. The security concerns are not worth the trouble.")
(defvar erbn-rx-time (current-time))
(defvar erbn-rx-ctrbad 0
  "Has no effect atm. But, modeled after erbn-octave-ctrbad, for possible later use. See the latter's usage in erbc8.el.")



;; more defvars fgrom erbc.el:


(defgroup erbc nil
  "The group erbc"
   :group 'applications)



(defcustom erbn-google-defaults
  '(("#emacs" ("emacs"))
    ("#fsbot" ("fsbot")))
  "" :group 'erbc)



(defcustom fsi-internal-articles
  '("the" "a" "an" "this" "that")
  ""
  :group 'erbc)


(defcustom fsi-internal-english-target-regexp
  "^$"
  "Targets that prefer english.. so erbot will usually go to a
english-mode unless near-exact matches.  This shall usually happen on
the few social channels erbot hangs out on. "
  :group 'erbc)

(defcustom fsi-internal-query-target-regexp
  "^$"
  "Targets where erbot will respond to queries like:
Foo ? "
  :group 'erbc)

(defcustom fsi-internal-add-nick-weights
  '(2;; yes
    3 ;;no
    )
  ""
  :group 'erbc)




(defcustom fsi-internal-google-time 4
  "" :group 'erbc)

(defcustom fsi-internal-dictionary-time 4
  "" :group 'erbc)

(defcustom fsi-internal-studlify-maybe-weights
  '(100 3)
  ""
  :group 'erbc)

(defcustom fsi-internal-h4x0r-maybe-weightsd
  '(100 2)
  ""
  :group 'erbc)

(defcustom erbn-greeting-string
  "Greetings and Salutations from %s" "")

(defcustom erbn-fortune-p t
  "This is true by default.. since (shell-command \"fortune\") is not
risky.. ")

(defcustom erbn-internal-web-page-time 10
  "" :group 'erbc)

(defcustom erbn-url-functions-p t
  "when true, enable url functions, provided that erbot-paranoid-p
allows us that.

Hopefully, this is an old bug: The reason you may not want to enable this function is that when you
fetch urls like http://205.188.215.230:8012 (icecast, etc. content),
url.el continues fetching that url forever (discovered by indio).  The
bot times out, but url continues fetching it in the background,
slowing down your bot. 

"
  :group 'erbc)


(defcustom erbn-char ","
  "The character which calls the bot.

in addition to directly addressing it.

may be different for
different bots.

Is really a string, but the length of the string should be 1,.
")
(defcustom erbn-char-double (concat erbn-char erbn-char)
  "The string which calls the bot from midsentence

this string should have a length of EXACTLY 2.

")

(defcustom fsi-internal-google-level 75
  "75 is a good choice for fsbot. "
  :group 'erbc)

(defcustom fsi-internal-english-max-matches 20
  "This check is triggerred only when the users' original request didnot
succeed and so we have gone into an english-mode and are searching.
If the number of matches results in 1000, then most likely, the word
was something like i or you and the user was not intending a search.
"

:group 'erbc)


(defcustom fsi-internal-questions
  '("what" "where" "who" "wtf" "wth" "wht" "whats"
    ;; no please:
    ;;"why"
    ;;"how"
    )
  ""
  :group 'erbc)


(defcustom fsi-internal-questions-all
  '("what" "where" "who" "why" "how"
    "whose" "which"
    )
  ""
  :group 'erbc)



;; defvars from erbcountry:
(defvar erbcountry-list nil)
(defvar erbcountry-string nil)




;; defvars from erblisp.
(defvar erblisp-allowed-words
  '(nil t 
	;; Also consider:
	;; &rest
	;; &optional
	
	)
  "You should add &rest and &optional to this list. 
We WON'T do this by default since this could lead to exploits if you
*happen* to have bound these keywords to weird stuff like 
\(setq &rest (shell-command \"rm -rf /\")) in your .emacs."
)

(defvar erblisp-max-list-length 2000
  "If non-numeric, we will skip this check."
  )



;; defvars from erblog:

(defvar erblog-active-targets nil
  "This stores the list of targets that have had some activity...

The idea is that the operator sets this to nil (see commands
below).. goes away, comes back and examined this variables to find
out which channels have had activity...
")



;;; end of defvars 2021
;;; code starts here, 2021


(defun erbot-doctor (args)
  "Glue the doctor into the ERC robot."
  (let* ((thisbuf (current-buffer))
         (dbuf (concat "*doctor: " (buffer-name thisbuf) "*"))
	 (docbuf (get-buffer dbuf))
	 outpoint
	 res)
    (if (not docbuf)
	(progn
          (set-buffer (get-buffer-create dbuf))
          (make-doctor-variables)
          (set-buffer thisbuf)
	  (setq docbuf (get-buffer dbuf))
	  (bury-buffer docbuf)))
    (save-excursion
      (set-buffer docbuf)
      (goto-char (point-max))
      (insert args)
      (goto-char (point-max))
      (setq outpoint (point))
      (doctor-ret-or-read 1)
      (doctor-ret-or-read 1)
      (goto-char outpoint)
      (re-search-forward "^.")
      (setq outpoint (- (point) 1))
      (re-search-forward "^$")
      (erc-replace-regexp-in-string
       "\n" " " (buffer-substring outpoint (point)))
    )))


(defun erbot-quiet ()
  (interactive)
  (setq erbot-quiet-p
	(not erbot-quiet-p))
  (message "set to %S" erbot-quiet-p))

(when erbot-nickserv-p
  (require 'erc-nickserv nil t) ;; old erc 
  (require 'erc-services nil t) ;; erc from emacs22
  (erc-nickserv-mode 1)
  )


(defmacro erbot-with-globals (&rest body)
  "see erbot-globals-reset for docs."
  `(unwind-protect
       ;; 20191027 since we now reset the globals below, the let trick is no longer neccessary.
       (let
	   (
	    (erbn-currentall erbn-currentall) ;; to prevent leaking.
	    ;; next follow components of currentall.
	    ;; (erbn-currentid erbn-nick erbn-target erbn-msg erbn-parsedmsg erbn-task-current erbn-reply erbn-reply-full.")
	    (erbn-currentid erbn-currentid)
	    (fsi-parsedmsg fsi-parsedmsg)
	    (fsi-msg fsi-msg) ;; to prevent any leaking.
	    (erbn-trashcurrent erbn-trashcurrent)
	    (erbn-msg erbn-msg)
	    (erbn-rw erbn-rw)
	    (erbn-parsedmsg erbn-parsedmsg)
	    (erbn-task-current erbn-task-current ) ;; tmpp scope 
	    (erbn-reply erbn-reply)
	    (erbn-replyfull erbn-replyfull)
	    (erbn-logging erbn-logging)
	    (erbn-fulllogging erbn-fulllogging)
	    (erbn-internal-midsentence-p erbn-internal-midsentence-p)
	    (erbn-pv-needs-saving erbn-pv-needs-saving) ;; so that any temporary modifications do NOT affect the global value
	    (fsi-limit-lines fsi-limit-lines) ;; so that any temporary modifications do NOT affect the global value
	    (fsi-inside-more-p fsi-inside-more-p) ;; temp. scope as above.
	    (erbn-nick erbn-nick)
	    (fs-nick fs-nick)
	    (erbn-tgt erbn-tgt)
	    (fs-tgt fs-tgt) ;; why do we have fs-tgt?  Did we mean fsi-tgt? 
	    (fsi-tgt fsi-tgt) 
	    )
	 ,@body)
     ;; 20191027 add this as an unwind-protect form. Why do we need this? It appears that some globals don't get reset to nil properly even though we used the let (item item) trick above.
     ;; how is that possible?
     ;; Maybe because we tried some functions internally using lisp prompt. That is, we called (set-term) directly as a test. Once you do that, the globals get set to non-nil. Then, our code sees non-nil and doesn't bother resetting them. 
     (erbot-globals-reset)))


(defun erbot-globals-reset ()
  "The globals in erbot-with-globals and erbot-globals-reset should match.  	* erb1.el (erbot-globals-reset): New. Fixes an issue wherein non-irc direct operator tests such as (fsi-set-term a 2) would mess up a lot of globals. That is because it is not invoked inside (erbot-with-globals). The (let) trick in erbot-with-globals is now superfluous.  With this new function, if you try (fsi-set-term a 2) in your emacs, the globals will /only/ be reset the next time the bot is called via irc.
       "
  (setq
   erbn-currentall nil
   erbn-currentid nil
   fsi-parsedmsg nil
   fsi-msg nil
   erbn-trashcurrent nil 
   erbn-msg nil
   erbn-rw nil
   erbn-parsedmsg nil
   erbn-task-current ""
   erbn-reply nil
   erbn-replyfull nil
   erbn-logging nil
   erbn-fulllogging nil
   erbn-internal-midsentence-p nil
   erbn-pv-needs-saving nil
   fsi-limit-lines  fsi-limit-lines-reset-value
   fsi-inside-more-p nil
   erbn-nick ""
   fs-nick ""
   erbn-tgt nil
   fs-tgt nil
   fsi-tgt nil
   ))


     
	
;


(defun erbot-save-buffer (&rest args)
  ;; makes fsbot skip any non-utf-8 characters! 
  (let ((coding-system-for-write 'utf-8))
    (apply #'save-buffer args)))

(defun erbot-save-kill-buffer (buf &rest args)
  "Save, make +r, and kill. Buf is really name of buffer."
  (apply 'erbot-save-buffer buf args)
  (erbutils-chmod-a+r buf)
  (let ((buf1 (get-buffer buf)))
    (when buf1
      (unless 
	  (buffer-modified-p buf1) ;; doesn't work on names!
	(kill-buffer buf)))))




(defun erbot-write-region (&rest args)
  ;; makes fsbot skip any non-utf-8 characters! 
  (let ((coding-system-for-write 'utf-8))
    (apply #'write-region args)))

(defun erbot-botlog-view ()
  "Internal view of botlog. NOT to be confused with fsi-botlog. Also meant to reset current-buffers to interesting buffers."
  (interactive)
  (find-file (erbn-log-file 1))
  (goto-char (point-max))
  (delete-other-windows)
  (split-window-vertically)
  (find-file (erbn-log-file 0))
  (goto-char (point-max)))


(defun erbot-buffers-save ()
  "Save any buffers that need saving.  Allows delayed saving of buffers. 
Should not be used for userfunctinos and uservariables atm."
  (progn
    ;; (erbutils-trace-backtrace)
    (save-excursion 
      ;; 
      ;; (when erbot-buffers-to-save (erbot-botlog-view)) ;; Don't change view if nothing has happened.
      (loop for bb in erbot-buffers-to-save do
	    (erbutils-ignore-errors-loudly
	     (with-current-buffer bb
	       (erbot-save-kill-buffer bb)
	       ;; (erbutils-chmod-a+r bb)
	       ;; (unless (buffer-modified-p buffer) (kill-buffer buffer)) ;; save some resources. 
	       )))
      (setf erbot-buffers-to-save nil)
      (when erbn-pv-needs-saving
	(fsi-pv-save)
	(setf erbn-pv-needs-saving nil))
      (when erbot-bbdb-save-p
	;; (bbdb-save-db)
	;; tag bbdb3
	(erbbdb-bbdb-save)
	(setf erbot-bbdb-save-p nil))
      )))
  

(defun erbot-frob-with-init-string (reply)
  (cond
   ((or (not (stringp reply)) (string= erbot-init-string "")) reply)
   (t
    (with-temp-buffer
      (insert reply)
      (goto-char (point-min))
      (while (re-search-forward "\n" nil t)
	(replace-match
	 (concat "\n" erbot-init-string) nil t))
      (concat erbot-init-string (erbutils-buffer-string))))))


;; this one is probably never used any more... just to make sure,
;; introduced an error command..
;(defun erbot-local (str)
;  "Funnel text typed by the local user to the local robot.  See
;\"erbot-remote\" for details of the command format."
;  (error "foo")
;  (erbot-command erc-process (erc-current-nick) (buffer-name) str t))


(defun erbot-toggle-reply ()
  (interactive)
  (setq erbot-reply-p (not erbot-reply-p))
  (message "erbot-reply-p set to %S" erbot-reply-p)
  )


;; The flood-check is now moved to fsi-lispify
;; (defun erbot-reply (&rest args)
;;   "Rudimentary flood-queue check."
;;   (if 
;;       (erbot-flood-p)
;;       (message "erbot is flood! Inhibiting all further action.")
;;     (apply 'erbot-reply1 args)))




(defun erbot-flood1-privmsgs (q)
  (setq q (mapcar #'car q))
  (setq q
	(remove-if-not 
	 #'(lambda (arg) (string-match "PRIVMSG" arg))
	 q)))


(defun erbot-flood-check  ()
  "Rudimentary flood-queue check.
ERC does a rate-limit check. But, an attack can build up where a user can initiate a lot of fsbot replies. In that case, we inhibit all replies going forward.

NOTE that we need to be in the correct buffer for this to take effect. That is becasue erc-server-flood-queue is buffer-local. 
"
  (unless
   erbot-flood-p
   (let*
       ((queue (erc-with-server-buffer erc-server-flood-queue))
	(q2 (erbot-flood1-privmsgs queue))
	(len1 (length queue))
	(len2 (length q2))
	)
     (when (> len1 0)
       (setf erbot-flood-debug (list queue q2))
       (message "flood debug lengths: %S:  %S: %S" (current-buffer) len1 len2)
       )
     (setq erbot-flood-p
	   (or (> len1	      erbot-flood-queue-thresh1)
	       (> len2	      erbot-flood-queue-thresh2)))
     (when erbot-flood-p
       (message "ERBOT IS FLOODED AND NOW DISABLED!"))))
  erbot-flood-p)
       

(defun erbot-flood-toggle ()
  (interactive)
  (setf erbot-flood-p (not erbot-flood-p))
  (message "ERBOT-FLOOD-P IS NOW SET TO %s" erbot-flood-p))
	   
   




(defun erbot-reply (main-reply proc from tgt msg locally-generated)
  "Robot worker.  Should do nothing when main-reply is nil or 'noreply
or \"noreply\". 

As appropriate, will also create fulllog, create ids, populate these variables, and maintain the assoc list lastid."
  (unless (stringp main-reply)
    (setq main-reply (format "%S" main-reply)))
  (setf main-reply (erbutils-remove-text-properties main-reply))
  (let (
	;;(fsi-limit-lines fsi-limit-lines) ;; so that changing in the body below or the rest of the functinos does NOT change the global value.. which is 8.
	linen
	(noreplyp
	 (or
	  (null erbot-reply-p)
	  (equal main-reply 'noreply)
	  (equal main-reply "noreply")))
	(me (or (erc-current-nick) erbot-nick))
	(reply
	 (erbot-frob-with-init-string main-reply))
	(rep-buffer (erc-get-buffer tgt proc)))
    (erc-log reply)
    (unless
	noreplyp
      ;; now we are actually gonna reply.
      ;; Therefore, set task. Populate variables, etc. 
      (setf erbn-fulllogging t)
      (erbn-task-history-set)
      (setf erbn-replyfull (format "%s" reply))
      (when (> (length erbn-replyfull) 1112123)
	(setf erbn-replyfull "<<TRUNCATED PRIOR TO STORING. REPLY IS GREATER THAN 1M CHARACTERS.>>"))
      (erbn-currentid-set)
      (save-excursion
	(setq reply (fsi-limit-lines reply))
	(setf erbn-reply reply)
	(setf erbn-currentall (list erbn-currentid erbn-nick erbn-tgt erbn-msg erbn-parsedmsg erbn-task-current erbn-reply erbn-replyfull))
	(erbn-lastall-update erbn-currentall erbn-tgt) ;; notice that this is being done near the end. 
	(erbn-fulllog-new erbn-currentall) ;; PLEASE NOTE THAT WE ARE CALLING FULLLOG-NEW BEFORE MAKING THE REPLY IRC-SAFE. SINCE WE ARE SIMPLY SAVING THE BUFFER, THAT SHOULD BE OK. 
	(if rep-buffer (set-buffer rep-buffer)
	;;; this alternative reply somehow never gets sent out..
	  ;;(setq reply (concat "msg " from " "
	  ;;		      "No private msgs.. try #testopn"))
	  ;;(set-buffer (erc-get-buffer tgt proc))
	  (progn
	    (ding t)
	    (message "WTF? no rep-buffer? "))
	  )
	(erbot-flood-check) ;; this sets the variable erbot-flood-p.
	(let* ((inhibit-read-only t)
	       (lines (split-string reply "[\n\r]+"))
	       (multiline-p (< 1 (length lines)))
	       p)
	  (mapc
	   (lambda (line)
	     (when (and line
			(not (erbot-safe-p line)))
	       (setq line (erbot-safe-make line)))
	     (goto-char (point-max))
	     (setq p (re-search-backward (erc-prompt)))
	     ;;(insert (erc-format-timestamp) "<" me "> ")
	     (insert ;;(erc-format-timestamp)
	      "<" me "> ")
	     (erc-put-text-property 0 (length line) 'face
				    'erbot-face line)
	     (insert line "\n")
	     (save-excursion
	       (save-match-data
		 (save-restriction
		   (narrow-to-region p (point))
		   (run-hook-with-args 'erc-send-modify-hook)
		   (run-hook-with-args 'erc-send-post-hook))))
	     (set-marker (process-mark erc-process) (point))
	     (set-marker erc-insert-marker (point))
	     (goto-char (point-max))
	     (setq linen (concat line "\n"))
	     ;; fledermaus: I used to force the encoding here, but I now 
	     ;; think that's the wrong thing to do. Hopefully if the data-path 
	     ;; through erc->fsbot->erc is clean, erc will do the right thing 
	     ;; to outbound data.
	     (erc-process-input-line linen nil multiline-p))
	   lines))))))






;;;###autoload
(defun erbot-install ()
  "Run this function AFTER loading all the files..."
  (interactive)
  (erball-reload)
  (erbot-install1))

;;;###autoload
(defun erbot-install1 ()
  "Run this function AFTER loading all the files..."
  (interactive)
  (erbot-dunnet-install)
  (setq erbot-on-new-erc-p
	(and (boundp 'erc-server-PRIVMSG-functions)
	     (featurep 'erc-backend)))
  (cond (erbot-on-new-erc-p
	 (add-hook 'erc-server-PRIVMSG-functions 'erbot-remote t)
	 ;; Do we need this local command thing...?
	 ;;(add-hook 'erc-send-completed-hook 'erbot-local t)
	 (add-hook 'erc-server-001-functions
		   'erbot-autojoin-channels))
	(t
	 (add-hook 'erc-server-PRIVMSG-hook 'erbot-remote t)
	 ;; Do we need this local command thing...?
	 ;;(add-hook 'erc-send-completed-hook 'erbot-local t)
	 (add-hook 'erc-server-001-hook
		   'erbot-autojoin-channels))
	)
  (erbot-install-symbols)
  (when (and erbot-setf-p (not erbot-paranoid-p))
    (erbot-install-setf))
  ;; A running bot should have these nil, else userfunctions will not
  ;; function right:
  (setq eval-expression-print-length nil)
  (setq eval-expression-print-level nil)
  (setq print-length nil)
  (setq print-level nil)
  )




;; 20150504 disabled.
(defun erbot-install-setf ()
  (interactive)
  (error "[erbot-install-setf] setf is permanently disabled as of 20150504. And it is aliased to setq instead.")
  (defalias 'fsi-setf 'setf)
  (require 'cl)
  (let*
      (
       ;; all possible symbols
       ;;(syms 
       ;;(apropos-internal "" (lambda (a) (get a 'setf-method))))
       (syms erbot-setf-symbols)
       (fssyms 
	(mapcar
	 (lambda (a) (intern (format "fs-%s" a)))
	 syms))
       (fsisyms 
	(mapcar
	 (lambda (a) (intern (format "fsi-%s" a)))
	 syms)))
    (mapcar*
     (lambda (a b c) 
       (let ((foo (get a 'setf-method)))
	 (when (fboundp b) (put b 'setf-method foo))
	 (when (fboundp c) (put c 'setf-method foo))))
     syms fssyms fsisyms)))




(defun erbot-install-symbols ()
  "By now, you should have loaded all pertinent erbot files... If you
add any new functions, don't forget to run (erbot-install) AFTER
that.."
  (interactive)
  (erbot-install-symbols-functions)
  (erbot-install-symbols-variables))


(defun erbot-install-symbols-functions ()
  (interactive)
  (let ((ss (fsi-command-list-readonly)))
    (dolist (s ss)
      
      (if (symbolp s)
	  (let ((f-s (erbutils-concat-symbols 'fs- s))
		(fi-s (erbutils-concat-symbols 'fsi- s)))
	    
	    (defalias f-s fi-s)
	    (put f-s 'readonly t))
	(message "Ignoring fsi->fs for %s" s)))))

(defun erbot-install-symbols-variables ()
  (interactive)
  (let ((ss (fsi-variable-list-readonly)))
    (dolist (s ss)
      (if (symbolp s)
	  (let ((f-s (erbutils-concat-symbols 'fs- s))
		(fi-s (erbutils-concat-symbols 'fsi- s)))
	    (set f-s (symbol-value fi-s))
	    (put f-s 'readonly t))
	(message "Ignoring fsi->fs for %s" s)))))





;;;###autoload
(defun erbot-autojoin-channels (server nick)
  "Set up a timer to autojoin channels.
We run this on a 120s timer, to allow us ample time to identify first.
At 60s mark, bbdbfrobnicate. At 120s mark, join channels. 
TODO: see erc-autojoin-timing and erc-autojoin-delay, and use those instead.
"
  (let ((timer (run-with-timer 120 nil 'erbot-autojoin-channels1 server nick)))
    (add-to-list 'erbot-autojoin-timers timer)
    (message "I have now set up a 120s timer to join channels. FYI, the arguments I saw were %S and %S" server nick)
    (sit-for 0.3)
    timer))

;;;###autoload
(defun erbot-autojoin-channels1 (server nick)
  ;;(interactive)
  (dolist (l erbot-servers-channels)
    (when (string-match (car l) (process-name server))
      (dolist (chan (cadr l))
	(with-current-buffer (process-buffer server)
	  (erc-send-command (concat "join " chan)))))))



  
(defun erbot-get-servers ()
  "Get erbot-servers from erbot-servers-channels. Not to be confused with variable erbot-server-buffers, which see."
  (mapcar '(lambda (arg) (list (car arg) (caddr arg)))
	  erbot-servers-channels))


(defun erbot-get-password (server nick)
  (let* 
      ((nick-alist (cadr (assoc server erc-nickserv-passwords))))
    (cdr (assoc nick nick-alist))))


;;;###autoload
(defun erbot-alive-p ()
  "Is atleast one connection still alive?"
  ;;(require 'cl-extra)
  (some
   'identity
   (mapcar
    (lambda (buf)
      (save-excursion
	(set-buffer buf)
	(erc-server-process-alive)
	;; (erc-process-alive)
	))
    (erc-buffer-list))))




;;;###autoload
(defun erbot-keep-alive (&rest args)
  "Periodically check if atleast one connection is still alive.  If
not, try to reconnect. "
  (require 'idledo)
  (idledo-add-periodic-action-crude
   '(unless
	(erbot-alive-p)
      (add-to-list 'erbot-reconnection-attempts
		   (message "Erbot trying to reconnect at %s"
			    (format-time-string
			     "%Y%m%d-%H%M-%S")))
      (ignore-errors (apply 'erbot-join-servers args)))))



;;;###autoload
(defun erbot-join-servers-pre2023 (&optional server port nick
				   user-full-name
				   not-connect-arg passwd)
  "Try to never join if already joined..."
  (interactive)
  (require 'erc)
  (if (null server)
      (mapcar
       '(lambda (arg)
	  (erbot-join-servers
	   (car arg) (cadr arg) nick user-full-name not-connect-arg passwd)
	  (sit-for 1)
	  )

       ;; get the list of servers
       (erbot-get-servers)

       )
    (progn
      ;;(if (null server)
      ;;	  (setq server erc-server))
      ;; 2002-08-21 T11:22:35-0400 (Wednesday)    D. Goel
      (setq erc-current-server-my server)
      (if (null port) 
	  (setq port 
		(if (fboundp 'erc-compute-port)
		    (erc-compute-port)
		  erc-port)))
      (setq nick (or erbot-nick (erc-compute-nick nick)))
      (let* (
	     (foo 'bar)
             (version nil)
	     ;(nick
	     ; (if (erc-already-logged-in server port nick)
	     ;;	  (read-from-minibuffer
	     ;;	   (erc-format-message 'nick-in-use ?n nick)
	     ;;	   nick
	     ;;	   nil nil 'erc-nick-history-list)
	     ;;	nick)))
	     )
	(if (and passwd (string= "" passwd))
	    (setq passwd nil))
	;; 	(while (erc-already-logged-in server port nick)
	;; 	  (setq nick (read-from-minibuffer
	;; 		      (erc-format-message 'nick-in-use ?n nick)
	;; 		      nick
	;; 		      nil nil 'erc-nick-history-list)))

	(run-hook-with-args 'erc-before-connect server port nick)
	
	;; (if (string-match "\\(\\<[[:digit:]]+.[[:digit:]]+\\>\\)" 
	;; 		  erc-version-string)
	;;     (setq version (string-to-number 
	;; 		   (match-string 1 erc-version-string)))
	;;   (setq version 0))

	(setq version 9999.0)
	(unless (erc-already-logged-in server port nick)
	  (let ((bufname nil))
	    (if (<= 5.0 version)
		(setf bufname
		      (erc :server    server 
			   :port      port 
			   :nick      nick 
			   :password  passwd
			   :full-name user-full-name))
	      (setf bufname
		    (erc
		     server port nick user-full-name (not not-connect-arg) passwd) ))
	    (add-to-list 'erbot-server-buffers bufname)
	    )


	  )))))






;;;###autoload
(defun erbot-keep-alive (&rest args)
  "Periodically check if atleast one connection is still alive.  If
not, try to reconnect. "
  (require 'idledo)
  (idledo-add-periodic-action-crude
   '(unless
	(erbot-alive-p)
      (add-to-list 'erbot-reconnection-attempts
		   (message "Erbot trying to reconnect at %s"
			    (format-time-string
			     "%Y%m%d-%H%M-%S")))
      (ignore-errors (apply 'erbot-join-servers args)))))



(defalias 'erbot-join-servers 'erbot-join-servers-2023)
;;;###autoload
(defun erbot-join-servers-2023 (&optional server port nick
				   user-full-name
				   not-connect-arg passwd)
  "Try to never join if already joined..."
  (interactive)
  (require 'erc)
  (if (null server)
      (mapcar
       '(lambda (arg)
	  (erbot-join-servers
	   (car arg) (cadr arg) nick user-full-name not-connect-arg passwd)
	  (sit-for 1)
	  )

       ;; get the list of servers
       (erbot-get-servers)
       )
    (progn
      ;;(if (null server)
      ;;	  (setq server erc-server))
      ;; 2002-08-21 T11:22:35-0400 (Wednesday)    D. Goel
      (setq erc-current-server-my server)
      (setq port 6667)
      (setq nick (or erbot-nick (erc-compute-nick nick)))
      (let* (
	     (foo 'bar)
             (version nil)
	     ;(nick
	     ; (if (erc-already-logged-in server port nick)
	     ;;	  (read-from-minibuffer
	     ;;	   (erc-format-message 'nick-in-use ?n nick)
	     ;;	   nick
	     ;;	   nil nil 'erc-nick-history-list)
	     ;;	nick)))
	     )
	(unless passwd
	  (setq passwd (erbot-get-password server nick)))
	(unless passwd
	  (error "No password could be computed."))
	(run-hook-with-args 'erc-before-connect server port nick)
	
	;; (if (string-match "\\(\\<[[:digit:]]+.[[:digit:]]+\\>\\)" 
	;; 		  erc-version-string)
	;;     (setq version (string-to-number 
	;; 		   (match-string 1 erc-version-string)))
	;;   (setq version 0))

	(setq version 9999.0)
	(unless (erc-already-logged-in server port nick)
	  (let ((bufname nil))
	    (if (<= 5.0 version)
		(setf bufname
		      (erc :server    server 
			   :port      port 
			   :nick      nick 
			   :password  passwd
			   :full-name user-full-name))
	      (setf bufname
		    (erc
		     server port nick user-full-name (not not-connect-arg) passwd) ))
	    (add-to-list 'erbot-server-buffers bufname)
	    )


	  )))))








(defun erbot-safe-make (text)
  (let ((ans text))
    (when (string-match "^/" text)
      (unless (string-match "^/me " text)
	(setq ans (concat " " text))))
    (unless (erbot-safe-make-control-safep ans)
      ;; (setq ans "<control characters>")
      ;; The beginning [^] serves (a) as a control-replacement warning. (b) It also ensures there are no erc commands such as /quit, etc. because we know that the first char is [.
      ;; That is, otherwise, we would have to run erbot-safe-make again.
      (setq ans (concat "[^] " (erbutils-text-char-description ans))))
    (unless
	(erbot-safe-make-control-safep ans) ;; should NOT happen. 
      (progn "Huh. I don't do control characters!"))
    (when (string-match "[\n\r]" ans)
      (setq ans " <newlines> "))
    ans))

(defun erbot-safe-make-control-safep (text)
  (let (
	(rlist (string-to-list text)))
    (not
     (member-if
      (lambda (a) (and (< a 32) (not (= a 9))))
      rlist))))
    
    

    

(defun erbot-safe-p (reply)
  "Determine whether a reply is safe.  Any newlines are simply
reported as unsafe.

If this functions deems a reply as unsafe, you should not send it to
ERC but call `erbot-safe-make' first. "
  (and
   (not (string-match "[\n\r]" reply))
    ;; err on the side of caution.  Demand that the 1st char. be VERY
   ;; safe.  
   (or
    (string-match "^[0-9a-zA-Z]" reply)
    ;;(not (string-match "^/" reply)) -- this is bad.. since, control
    ;;characters are bad... beginnning ^A for example, will send CTCP requests..
    
    ;; Allow /me commands.. but only when the rest of the text has no
    ;; control characters..
    (equal 0 (string-match "^/me " reply)))
   ;; And there be no control characters whatsoever anywhere.
   (erbot-safe-nocontrol-p reply)))

(defun erbot-safe-nocontrol-p (reply)
  (let ((rlist (string-to-list reply)))
    (not (member-if (lambda (a) (< a 32)) rlist))))






(defmacro erbot-working (&rest args)
  "Inhibit multiple bbdb savings during the same operation.
Now that we only save bbdb at the end during erbot-remote, the utility of this function should be moot. hm? "
  `(let ((erbbdb-save-p nil)
	 (erbot-notify-p nil))
     ,@args))



(defun erbot-rejoin-channels ()
  "Try to re-join all channels for the very first irc process! Will add this to an idledo. 
Sometimes, we get disconnected from channels. We end up not auto-joining them because we are not identified.  This should rectify this problem!"
  (interactive)
  (let* ((serverb (car erbot-server-buffers)) ;; do this for only one server for now. serverb refers to buffer.
	 (proc (get-buffer-process serverb)))
    (dolist (l erbot-servers-channels)
      (when (string-match (car l) (buffer-name serverb))
	(dolist (chan (cadr l))
	  (with-current-buffer serverb
	    (erc-send-command (concat "join " chan))))))))



(provide 'erb1)
(run-hooks 'erb1-after-load-hooks)








;;; erbot.el ends here
