;;; erbcspecial.el --- Special/dangerous implementation functions.
;; Many fs-functions can simply be defined in terms of other
;; fs-functions (and always should be!, for security.)
;; This file is for the remaining few, that can't be.
;; Thus, CODE IN THIS FILE SHOULD BE CONSTRUCTED VERY CAREFULLY.
;; Time-stamp: <2007-11-23 11:30:12 deego>
;; Copyright (C) 2004 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbcspecial.el
;; Package: erbcspecial
;; Author: D. Goel <deego@glue.umd.edu>
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:


 
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;; See also:

(defconst erbcspecial-version "0.0dev")

;;==========================================
;;; Requires:
(eval-when-compile (require 'cl))

;;; Real Code:

;; in order to allow fsi-seq-filter.
(require 'seq)

(defun erbn-special-quote-function-old-broken (fcn)
  ;; A little broken. Does not handle variables correctly.. 
  (error "[esqfob] Being fixed atm.")
  (when erbot-paranoid-p
    (error "this function  is disabled atm: paranoid-p is true."))
  (cond
   ((symbolp fcn)
    (erblisp-sandbox-quoted fcn))
   ;; If it's a lamba, it needs to be sandboxed carefully! It could have been a quoted lambda.. which then means verbatim evaluations!
   ;; Otherwise, the user can supply us '(lamda (x) (shell-command... )) and we will happily do it!
   ((and (listp fcn)
	 (equal (first fcn) 'lambda)
	 (erblisp-sandbox fcn)))
   ;; notice the recursion below:
   ((listp fcn) (erbn-special-quote-function-old-broken (fsi-eval fcn)))
   (t (error "Cannot apply this as a function!"))))


;; (defun fs-mapcar-old (sym seq)
;;   "only symbols allowed at this time. "
;;   (unless (symbolp sym)
;;     (error "Function argument to mapcar for this bot can only be a symbol."))
;;   (setq sym (erblisp-sandbox-quoted sym))
;;   ;; everything should already be boxquoted.. cool
;;   (mapcar sym seq))

(defun OLDfsi-mapcar (fcn ls)
  (apply 'mapcar 
	 (erbn-special-quote-function-old-broken fcn)
	 ls nil))





(defvar erbn-lastargs nil)
(defvar erbn-currenterrorp nil)

(defmacro fsi-apply (sym &rest args)
  " NB: sym need not be just a sym. Variables, lambdas, and quoted lambdas etc are correctly  processed as well!"
  (when erbot-paranoid-p 
    (error "[apply] This function is disabled atm: erbot-paranoid-p"))
  (unless sym (error "No function to fs-apply!"))
  (let (erbn-tmpargs
	(erbn-tmplen (length args))
	(erbn-currenterrorp nil)
	erbn-tmpfirstargs
	erbn-lastargs
	erbn-tmpspecialp ;; denotes: NIL: no arguments at all.
	erbn-tmpnoinitialp ;; denotes the case when the len args =1..
	) 
    (cond
     ((= (length args) 0)
      (setq erbn-tmpspecialp t))
     ((= (length args) 1)
      (setq erbn-tmpnoinitialp t)))
    (cond
     ((> (length args) 300)
      (setq erbn-currenterrorp t)))
    (cond
     ((null args)
      (setq erbn-tmpargs nil)
      (setq erbn-tmplastargs nil)
      (setq erbn-tmpspecialp nil))
     (t
      (setq erbn-tmpargs
	    (append (subseq args 0 (- erbn-tmplen 1))))
      (setq erbn-tmplastargs
	    (first (last args)))))
    (setq erbn-tmpargs (erbn-apply-sandbox-args erbn-tmpargs))
    (setq erbn-tmplastargs 
	  (if (and (listp erbn-tmplastargs)
	       (equal (car erbn-tmplastargs) 'quote))
	      erbn-tmplastargs
	    (erbn-apply-sandbox-args erbn-tmplastargs)))
    ;; if quoted lambda, just make it a lambda, please.
    (setf sym (fsi-sandbox-standardize-function sym))

    (cond
     (erbn-currenterrorp
      `(apply "Error encountered in fsi-apply. Did you use too many args?"))
     (erbn-tmpspecialp
      `(apply ,sym nil))
     (erbn-tmpnoinitialp
      `(apply ,sym ,erbn-tmplastargs))
     (t
      `(apply ,sym ,@erbn-tmpargs ,erbn-tmplastargs))))
  )



(defmacro erbn-mapcar-etc (mapfunction sym ls)
  "Utility macro meant to be called by fsi-mapcar, fsi-mapc, etc.


Caution: This macro should NOT be called except [from fsi- or fs- functions, or] from immediate one-line constructors such as fsi-mapcar. It should only be evalled when the expression has run through a fs-sandbox. 

The reason is that it converts all variables, etc. supplied to it to fs-vars.
The calling function may have done something like (let ((v 'foo)) (erbn-mapcar-etc ... v ... )).
erbn-mapcar-etc will then try to eval fs-v. If, for some reason, the calling function has fs-v bound to unsavory stuff, we could end up evaling that.

Such a problem will never happen from user functions, of course,
because by the time they reach us, even the let binding should
have been applied to fs-v. 


You may suspect that another reason userfunctions are safe is
that userfunctions can never directly call erbn-mapcar-etc in the
first place. But, that reasoning doesn't really hold because they
can still call the macro fsi-mapcar, which in turn calls us.



20180529 As you can see, we only allow one ls arg, and not multiple arguments.
"


  (assert (atom mapfunction))
  ;; NO MAPCONCAT HERE, please. That needs to be created separately.
  ;; no destructive functions are allowed. 
  (assert (member mapfunction '(mapcar mapl maplist mapc seq-filter)))

  ;; (assert (member mapfunction '(mapcar mapl mapcon maplist mapc seq-filter)))
  (when erbot-paranoid-p 
    (error "[fme] This function is disabled atm: erbot-paranoid-p"))
  (unless sym (error "[fme] No function to fs-apply!"))
  (let (
	;;(erbn-tmplen (length ls))
	(erbn-tmplastls ls)
	) 

    (setq erbn-tmplastls 
	  (if (and (listp erbn-tmplastls)
	       (equal (car erbn-tmplastls) 'quote))
	      erbn-tmplastls
	    (erbn-apply-sandbox-args erbn-tmplastls)))
    ;; if quoted lambda, just make it a lambda, please.

    (setf sym (fsi-sandbox-standardize-function sym))

    
    `(progn
       ;; 20180528 
       ;; lambdas are used atm by several userfunctions... Therefore, do not insert this check.
       ;; (assert (symbolp ,sym))
       (,mapfunction ,sym ,erbn-tmplastls))))






(defun fsi-mapcar* (&rest args)
  (error
   (fsi-random-choose
    '("mapcar*? I don't do no mapcar*! Try apt-get install emacs and (require 'cl)"
      "[mapcar*] I am a public IRC bot, not your personal lisp prompt!"))))

(defmacro fsi-mapc (sym args &rest ig)
  `(erbn-mapcar-etc mapc ,sym ,args))

(defmacro fsi-seq-filter (sym args &rest ig)
  "Thankfully, seq-filter looks ok and non-destructive. So, ok to provide here. 
It looks easy enough to simulate here directly. That might be a better and safer path for any such future additions."
  `(erbn-mapcar-etc seq-filter ,sym ,args))


(defmacro fsi-mapcar (sym args &rest ig)
 "Mapcar.


It works as expected by end-users because everything that reaches here is already sandboxed.

But, if you use it in other functions in, say, this file, be
prepared for a nasty surprise unless all your arguments start
with fs-. That is because this (and similar) functions first
sandbox everything. 

NOTE that unlike elisp, this function will ONLY take one list as args. Any second and subsequent arguments will be ignored
If you do have multiple lists, making a map first will prove handy: example: (fs-mapcar 'f (fs-mapcarlists list1 list2....))).


" 
  `(erbn-mapcar-etc mapcar ,sym ,args))

(defalias 'fsi-seq-map 'fsi-mapcar)


(defmacro fsi-mapl (sym args &rest ig)
  `(erbn-mapcar-etc mapl ,sym ,args))

(defmacro fsi-maplist (sym args &rest ig)
  `(erbn-mapcar-etc maplist ,sym ,args))



(defun fsi-mapconcat (fs-f fs-ls &optional fs-sep)
  "
Programming notes: Note that 'fsi-mapcar-etc will change any args to fs-args. Thus, the args have to all be in fs-arg form. 
That doesn't apply to fs-sep since it's not fed to mapcar-etc, so we could use seq here. "
  (when fs-sep
    (assert (stringp fs-sep)))
  (let 
      ((ls2 (fsi-mapcar fs-f fs-ls)))
    (mapconcat 'identity ls2 fs-sep)))





;; 20180529 And, now, aliases for destructive functions.  Note that fsbot never allows destructive functions! 
(defalias 'fsi-nconc 'fsi-append)

(defun fsi-mapcan (fs-f fs-l)
  "Simulates mapcan. But uses the nondestructive append instead of nconc. Since this is a function, not a macro, there is no need to use fs-f here."
  (apply 'append (fs-mapcar fs-f fs-l)))

(defun fsi-mapcon (fs-f fs-l)
  "a non destructive simalution, by using append instead of nconc."
  (apply 'append (fs-maplist fs-f fs-l)))




(defun fsi-mapcarlists (&rest lists)
  "See fsi-mapcar for why this function is provided.
In principle, you could directly try (fs-apply 'fs-mapcar <your lists>) but an apply is not possible because 'fs-mapcar is a macro. Thus, we provide this function for convenience."
  (require 'cl)
  (apply 'cl-mapcar #'list lists)
  )
 


(provide 'erbcspecial)
(run-hooks 'erbcspecial-after-load-hook)



;;; erbcspecial.el ends here
