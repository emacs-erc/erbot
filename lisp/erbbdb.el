;;; erbbdb.el --- 
;; Time-stamp: <2007-11-23 11:30:13 deego>
;; Copyright (C) 2002 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbbdb.el
;; Package: erbbdb
;; Author: D. Goel <deego3@gmail.com>
;; Version: 0.0dev
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
 

;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;; See also:




(defvar erbbdb-version "0.0dev")

;;==========================================
;;; Code:
(ignore-errors (require 'bbdb))
(ignore-errors (require 'bbdb-com))
(ignore-errors (require 'bbdb-hooks))

(require 'erbc)



;; tag bbdb3
(defvar erbbdb-bbdb3 nil "Internal. If 3, we divert some functions to erbbdb3.. ")
;; tag bbdb3
(let ((major
       (first (split-string bbdb-version "\\."))))
  (setq erbbdb-bbdb3 (not (string= major "2"))))



(defgroup erbbdb nil 
  "The group erbbdb"
   :group 'applications)
(defcustom erbbdb-before-load-hooks nil "." :group 'erbbdb)
(defcustom erbbdb-after-load-hooks nil "" :group 'erbbdb)
(run-hooks 'erbbdb-before-load-hooks)


(defun erbbdb-get-exact-notes (string)
"BUG: If foo redirects to, say, bar.el, and user asks foo? without invoking the bot, we expect fsbot to get bar.el's first result. 
However, that doesn't work because fsi-describe calls erbbdb-get-exact-notes on bar\\.el instead of bar.el.

That is, if the redirected term's name has a regex char in it, this will fail.
"

  (erbbdb-get-regexp-notes (concat "^" (regexp-quote 
					(erbbdb-frob-main-entry string)
					) "$")))

(defun erbbdb-get-exact-name (string)
  (erbbdb-get-regexp-name (concat "^" (regexp-quote 
					(erbbdb-frob-main-entry string)
					) "$")))



(defun erbbdb-get-regexp-record (expr)
  "dsfdfdf"
  (let ((records
	 (bbdb-search (bbdb-records)
	  expr)))
    (first records)))

(defun erbbdb-get-record (str)
  (erbbdb-get-regexp-record 
   (concat "^" (regexp-quote 
		(erbbdb-frob-main-entry str)) "$")))

(defun erbbdb-get-regexp-name (expr)
  "used to get exact name, eg: the exact name of tmpa may be TmpA."
  (let ((record (car 
		 ;; this basically does an M-x  bbdb-name 
		 (bbdb-search (bbdb-records)
			      expr))))
    (if record
	(aref record 0)
      nil)))



;; tag bbdb3: name change.
(defun erbbdb2-get-regexp-notes (expr)
    "currently: Assumes that there will be only one match for the expr ;
in bbdb...  Discards any further matches...                                                                                                                                                            
                                                                                                                                                                                                         
If the notes are (), we want it to return nil, not a string.. so that ;
the calling function knows there's (effectively) no such record...                                                                                                                                       
                                                                                                                                                                                                         
That is why we have the read below..                                                                                                                                                                     
                                                                                                                                                                                                         
This of course, also means that the notes field had better
contain a lisp sexp.. and anythign after the sexp gets
discarded...
                                                                                                                                                                                                         
If record exists but no notes exist, \"\" is returned.                                                                                                                                                   
Else the string containing the notes is returned.                                                                                                                                                        
If no record exists, then a nil is returned.                                                                                                                                                             
"
    (let ((record (car
		   ;; this basically does an M-x  bbdb-name
		   (bbdb-search (bbdb-records)
				expr))))
      (if record
	  (let* ((notes-notes (assq 'notes (bbdb-record-raw-notes record)))
		 (notes-string (cdr notes-notes)))
	    (or notes-string "")
	    ;;(if foo (read foo) nil)
	    )
	nil)))



(defun erbbdb-frob-main-entry (givenname)
    (let* ((sname (format "%s" givenname))
	   ;;(dname (downcase sname))
	   (dname sname)
	   (bname (split-string dname))
	   (name (mapconcat 'identity bname "-")))
      name))

(defun erbbdb-change (givenname notes)
  "also used by other functions in here.."  
  (bbdb-records)

  (let* ((sname (format "%s" givenname))
	 ;;(dname (downcase sname))
	 (dname sname)
	 (bname (split-string dname))
	 (name (mapconcat 'identity bname "-")))
    ;;(let ((record
    ;;	 (vector 
    ;;	  ;; first name
    ;;	  name
    ;;	  ;;lastname
    ;;	  nil
    ;;	  nil
    ;;	  nil ;;company
    ;;	  nil ;;phones
    ;;	  nil ;; addrs
    ;;	  nil ;;net
    ;;	  (format "%s" notes)
    ;;	;  (make-vector bbdb-cache-length nil))))
    ;;    (bbdb-change-record record t))
    (let* ((record (erbbdb-get-record name)))
      ;; tag bbdb3
      (erbbdb-bbdb-record-set-notes record notes)
      (bbdb-change-record record t)
      (erbbdb-save))))

(defun erbbdb-save ()
  "This used to be a conditional save, based on erbot-working. The idea was to avoid multiple saves. Now, it just sets a variable in any case instead of saving. That is, any saving is done at the end by erbot-remote."
  (when 
      erbbdb-save-p
    ;;(bbdb-save-db)
    (setf erbot-bbdb-save-p t)
    ))




;; tag bbdb3
(defun erbbdb-bbdb-save ()
  (if erbbdb-bbdb3
      (bbdb-save nil t) ;; bbdb3
    (bbdb-save-db) ;; old
    ))






     

(defvar erbbdb-save-p t
  "Should normally be t, except inside special constructions. ")

;; tag bbdb3 name change.  
(defun erbbdb2-create (name newnotes)
  "also used by other functions in here.."
  (bbdb-records)
  (let ((record
	 (vector 
	  ;; first name
	  name
	  ;;lastname
	  nil
	  nil
	  nil ;;company
	  nil ;;phones
	  nil ;; addrs
	  nil ;;net
	  nil ;; (format "%s" newnotes)
	  (make-vector bbdb-cache-length nil))))
    (erbbdb-bbdb-record-set-notes record nil)
    (erbbdb-add-multi name newnotes)
    ;;(mapcar '(lambda (arg)
    ;;(erbbdb-add name arg))
    ;;newnotes)
    )
  (erbbdb-save))


(defun erbbdb-add-multi (name innotes)
  ;; Old, proof of concept. 
  ;; 2014 dg: add see erbbdb-add-multi.
  ;; (error "not functional yet.")
  ;;(error "Erbbdb-Add: Please use erbbdb-add-multi going forward.")
  (bbdb-records)
  (let* ((oldnotes
	  (erbbdb-get-exact-notes name))
	 (newnotes nil))

    ;; should almost always be the case.. except when nil..
    (if (stringp oldnotes)
	(setq oldnotes 
	      (ignore-errors (erbn-read oldnotes))))
    (setq newnotes (format "%S" (append oldnotes innotes)))
    (erbbdb-remove-not-really name)
    (erbbdb-change name newnotes)))


(defun erbbdb-add (name note)
  ;; Old, proof of concept. 
  ;; 2014 dg: add see erbbdb-add-multi.
  (assert (not (null note)))
  (assert (stringp note))
  (erbbdb-add-multi name (list note)))

  ;; (let* ((oldnotes
  ;; 	  (erbbdb-get-exact-notes name))
  ;; 	 (newnotes nil))

  ;;   ;; should almost always be the case.. except when nil..
  ;;   (if (stringp oldnotes)
  ;; 	(setq oldnotes 
  ;; 	      (ignore-errors (erbn-read oldnotes))))
  ;;   (setq newnotes (format "%S" (append oldnotes (list note))))
  ;;   (erbbdb-remove-not-really name)
  ;;   (erbbdb-change name newnotes)))


(defun erbbdb-remove-not-really (name)
  "Set notes to nil temporarily."
  (erbbdb-change name nil))

(defun erbbdb-remove (givenname)
  "Remove the record implied by givenname from bbdb.."
  ;;(erbbdb-change name nil)
  (bbdb-records)
  (let* ((sname (format "%s" givenname))
	 ;;(dname (downcase sname))
	 (dname sname)
	 (bname (split-string dname))
	 (name (mapconcat 'identity bname "-")))
    (let* ((record (erbbdb-get-record name)))
      (when record
	;; tag bbdb3
	(erbbdb-bbdb-delete-current-record record)
	;;(bbdb-record-set-notes record notes)
	;;(bbdb-change-record record t)
	(erbbdb-save)))))



;; tag bbdb3
;;;; 20191026 compat stuff to try to migrate to bbdb3.
;; tag bbdb3
;; (defun erbbdb3-bbdb (regexp)
;;   "20191026 Hack in progress. Like (bbdb), but does NOT display. Merely returns the records.
;; in either the name(s), organization, address, phone, mail, or xfields."
;;   ;; (interactive (list (bbdb-search-read) (bbdb-layout-prefix)))

;;   (bbdb-search (bbdb-records) :all-names regexp
;;                :organization regexp :mail regexp
;;                :xfield (cons '* regexp)
;;                :phone regexp :address regexp :bool 'or))



;; tag bbdb3
(defun erbbdb3-get-exact-notes (string)
  (erbbdb3-get-regexp-notes
   (concat "^" (regexp-quote
		(erbbdb-frob-main-entry string)
		) "$")))


;; tag bbdb3
(defun erbbdb3ee-get-regexp-notes (reg)
  "NOT USED. instead, we use erbbdb3-get-regxp-notes. "
  (let*
      (
       (fullrecord (erbbdb3-bbdb reg)) ;; looks like (["vi"...] [...] ) I think.
       rec1 xfields notes1 notes
       )
    (when fullrecord
      (setq rec1 (first fullrecord)) ;; looks like ["vi" nil nil ... ((notes . ...)) ]. now, the 9th field is notes.
      (setq xfields (aref rec1 8)) ;; looks like ( ( notes .  ...)  (otherfields? . ...))
      (setq notes1 (assoc 'notes xfields)) ;; looks like ( notes . "notes")
      (setq notes (cdr notes1))
      )
    notes
    ))


;; tag bbdb3
(defun erbbdb-get-regexp-notes (expr)
  (if  erbbdb-bbdb3
      (erbbdb3-get-regexp-notes expr)
    (erbbdb2-get-regexp-notes expr)
    ))



;; tag bbdb3
(defun erbbdb3-get-regexp-notes (expr)
    "                                                                                                                                                                                                      
Migrated from erbbdb2-get-regexp-notes.                                                                                                                                                                  
Old notes from erbbdb2-get-regexp-notes: currently: Assumes that there will be only one match for the expr                                                                                               
in bbdb...  Discards any further matches...                                                                                                                                                              
                                                                                                                                                                                                         
If the notes are (), we want it to return nil, not a string.. so that                                                                                                                                    
the calling function knows there's (effectively) no such record...                                                                                                                                       
                                                                                                                                                                                                         
That is why we have the read below..                                                                                                                                                                     
                                                                                                                                                                                                         
This of course, also means that the notes field had better contain a                                                                                                                                     
lisp sexp.. and anythign after the sexp gets discarded...                                                                                                                                                
                                                                                                                                                                                                         
If record exists but no notes exist, \"\" is returned.                                                                                                                                                   
Else the string containing the notes is returned.                                                                                                                                                        
If no record exists, then a nil is returned.                                                                                                                                                             
"
    (let ((record (car
		   ;; car because we assume atmost one answer!
		   ;; this basically does an M-x  bbdb-name
		   (bbdb-search (bbdb-records)
				expr)))
	  xfields notes1 notes
	  )
      (when record
	(setq xfields (aref record 8)) ;; looks like ( ( notes .  ...)  (otherfields? . ...))
	(setq notes1 (assoc 'notes xfields)) ;; looks like ( notes . "notes")
	(setq notes (cdr notes1))
	(setq notes (or notes "")) ;; should always return a string when we have fullrecord exists.
	notes
	)))





;; tag bbdb3
(defun erbbdb-bbdb-delete-current-record (record)
  (if (not erbbdb-bbdb3)
      (bbdb-delete-current-record record t)
    (bbdb-delete-field-or-record (list record) (list 'name (aref record 0)) t)))


;; tag bbdb3
(defun erbbdb-create (name newnotes)
  (if erbbdb-bbdb3
      (erbbdb3-create name newnotes)
    (erbbdb2-create name newnotes)))

;; bbdb3
(defun erbbdb3-create (name newnotes)
  "also used by other functions in here.."
  ;; (bbdb-records)
  (let ((record (bbdb-empty-record)))
    (setf (aref record 0) name)
    ;; It seems from that when we inherit the legacy bbdb db, name goes in the zero'th enttry.
    ;; note that by default, in bbdb3, name seems to go in the 1st entry. there's also some kind of a provision for separate first and last names.
    (bbdb-change-record record nil t) ;; this creates an empty record. 
    ;; (erbbdb-bbdb-record-set-notes record nil)
    (erbbdb-add-multi name newnotes)
    ;;(mapcar '(lambda (arg)
    ;;(erbbdb-add name arg))
    ;;newnotes)
    )
  (erbbdb-save))





;; bbdb3
(defun erbbdb-bbdb-record-set-notes (record value)
  (if erbbdb-bbdb3
      (bbdb-record-set-xfield record 'notes value)
    (bbdb-record-set-notes record value)))


(provide 'erbbdb)
(run-hooks 'erbbdb-after-load-hooks)



;;; erbbdb.el ends here
