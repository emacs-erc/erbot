;;; erbc3.el ---erbot lisp stuff which should be PERSISTENT ACROSS SESSIONS.
;; Time-stamp: <2007-11-23 11:30:12 deego>
;; Copyright (C) 2003 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc3.el
;; Package: erbc3
;; Author: D. Goel <deego3@gmail.com>
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:

(defconst erbc3-home-page
  "http://gnufans.net/~deego")


 
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

(defconst erbc3-version "0.dev")
(defun erbc3-version (&optional arg)
   "Display erbc3's version string.
With prefix ARG, insert version string into current buffer at point."
  (interactive "P")
  (if arg
      (insert (message "erbc3 version %s" erbc3-version))
    (message "erbc3 version %s" erbc3-version)))

;;==========================================
;;; Requires:
(eval-when-compile (require 'cl))

;;; Code:

(defgroup erbc3 nil
  "The group erbc3."
  :group 'applications)
(defcustom erbc3-before-load-hook nil
  "Hook to run before loading erbc3."
  :group 'erbc3)
(defcustom erbc3-after-load-hook nil
  "Hook to run after loading erbc3."
  :group 'erbc3)
(run-hooks 'erbc3-before-load-hook)


;;; Real Code:
;; pf stands for persistent functions.
;; pv stands for persistent variables.


(defun fsi-pfpv-load ()
  (erbn-log 'pfpv-load)
  (erbn-unlog
   (fsi-pf-load)
   (fsi-pv-load)))

(defun fsi-pf-load ()
  "fsbot's .emacs should call erbn-pf-load instead of fsi-pf-load."
  (erbn-log 'pf-load)
  (erbn-unlog
   (erbn-pf-load)))

(defun erbn-pf-load ()
  (erbn-log-inhibit
   (if (file-exists-p erbn-pf-file)
       ;;(fsi-ignore-errors-else-string (load erbn-pf-file))
       (progn (load erbn-pf-file))
     (message "File does not exist: %s" erbn-pf-file))))


(defun fsi-pv-load ()
  (interactive)
  (erbn-log 'pv-load)
  (erbn-log-deepen
   (erbn-pv-load)))

(defun erbn-pv-load ()
  (erbn-log-inhibit
   (when (file-exists-p erbn-pv-file)
     (ignore-errors (load erbn-pv-file)))
   ;; But, whenever you load any fs- variables, ensure that any applicable fsi- variables immediately overwrite them:
   (erbot-install-symbols-variables)
   t))
  

(defun fsi-user-function-p (fcn)
  (member 
   fcn 
   (erbutils-functions-in-file erbn-pf-file)))


(defun erbn-create-defun-new (sexps body)
  (cons body sexps))

(defun erbn-create-defun-overwrite (sexps body fcn)
  (cons body
	(remove
	 (first (member-if
		 (lambda (arg) (equal (second arg) fcn))
		 sexps))
	 sexps)))

			 

(defun erbn-write-sexps-to-file (file sexps &optional backup-rarity)
  (unless backup-rarity (setq backup-rarity 1))
  (when (zerop (random backup-rarity)) (erbutils-mkback-maybe file))

  (find-file file)
  (widen)
  (delete-region (point-min) (point-max))
  (insert "\n\n\n")
  (insert
   (mapconcat
    (lambda (arg) (pp-to-string arg)) sexps "\n\n\n"))
  (insert "\n\n\n")
  (erbot-save-buffer))







(defun fsi-get-uservariables ()
  (remove-if (lambda (sym) (get sym 'readonly))
	     (apropos-internal "^fs-" 'boundp)))


(defun fsi-pv-get-variables-values ()
  (let 
      ((vars (fsi-get-uservariables)))
    (mapcar
     (lambda (v)
       `(when (not (boundp ',v))
	  (setq ,v
	    (quote ,(symbol-value v)))) )
     vars)))

  


(defun fsi-pv-get-variables-values-pre2015 ()
  (let 
      ((vars
	(apropos-internal "^fs-" 'boundp))

    (mapcar
     (lambda (v)
       `(ignore-errors 
	  (defvar ,v
	    (quote ,(eval v)))))
     vars))))




;;;###autoload
(defun fsi-pv-save ()
  (interactive)
  (erbn-write-sexps-to-file 
   erbn-pv-file 
   (fsi-pv-get-variables-values) 1000))
   ;; this should lead to a few saves every day... not too many, one hopes..
;;1000))


   
(defun erbn-readonly-check (sym)
  (if (get sym 'readonly)
      (error "Getting symbol '%S modified is super-easy! Simply submit three copies of a detailed petition, signed in triplicate, with notarization, medallion and apostille."
	     sym)))




(defmacro fsi-defun (fcn args &rest body)
  
  ;; the given fcn icould be a number or string, in which
   ;; case sandboxing won't touch it, so we need to override that case.
  (erbn-log 'defun fcn args body)
  ;;fsi-internal-original-message
  (erbn-unlog
   (let ((docp nil))
     (unless 
	 (and (listp body)
	      (> (length body) 0))
       (error "[defun] Function body should have a length of 1 or more"))
     (unless (and (symbolp fcn) (not (fsi-constant-object-p fcn)))
       (error "[defun] Defun symbols only! :P"))
     ;; doc string exists, and is followed by more stuff..
     (when (functionp fcn)
       (error 
	"[defun] %s is already a function. ,ff it, EXAMINE IT, and if applicable, fmakunbound it first. "  fcn))
     (when (and (> (length body) 1)
		(stringp (first body)))
       (setq docp t))
     (erbn-readonly-check fcn)
     
     (erbn-write-sexps-to-file
      erbn-pf-file
      (erbn-create-defun-overwrite
       (erbutils-file-sexps erbn-pf-file)
       (if docp
	   
	   (cons 'defun 
		 (cons fcn 
		       (cons args 
			     (cons 
			      (first body)
			      (cons
			       `(erblisp-check-args ,@args)
			       (cons
				'(sit-for 0)
				(cdr body)))))))

	 (cons 'defun 
	       (cons fcn 
		     (cons args 
			   (cons
			    `(erblisp-check-args ,@args)
			    (cons 
			     '(sit-for 0)
			     body))))))
       
       fcn))
     (fsi-pf-load)
     `(quote ,fcn))))



     

(defun fsi-defalias (sym1 sym2)
  "Huh, shouldn't this be a macro instead?

20210218 todo: defalias leads to two log entries .. we need to erbn-unlog here before calling defun."
  (let ((meta (list sym1 sym2)))
    (apply 'erbn-log meta)
    ;; should we erbn-unlog here?
    (eval `(fs-defun 
	    ,(erblisp-sandbox-quoted sym1) (&rest fs-defaliasREST)
	    (fs-apply (quote ,(erblisp-sandbox-quoted sym2)) fs-defaliasREST)))))
  






(defun fsi-set (&optional sym value)
  (let ((meta (list 'set sym)))
    (apply 'erbn-log meta)
    (erbn-unlog
     (unless sym (error "%s" "[set] Syntax: , (set 'symbol value)"))
     (setq sym
	   (erblisp-sandbox sym))
     (erbn-readonly-check sym)
     (ignore-errors
       (when (symbol-value sym)
	 (erbn-trash
	  meta (list 'variable sym)   (list (format "%S" (symbol-value sym))))))
     (let ((result (set sym value)))
       (fsi-pv-save)
       result))))



(defun fsi-makunbound (&optional sym)
  (let ((meta (list 'makunbound sym)))
    (apply 'erbn-log meta)
    (erbn-unlog
     (unless sym (error "%s" "Syntax: , (makunbound 'symbol)"))
     (setq sym
	   (erblisp-sandbox sym))
     (ignore-errors
       (erbn-trash
	meta (list 'variable sym)   (list (format "%S" (symbol-value sym)))))
     (let ((result (makunbound sym)))
       (fsi-pv-save)
       result))))
    

(defun fsi-fmakunbound (&optional sym)
  (let ((meta  (list 'fmakunbound sym)))
    (apply 'erbn-log meta)
    (erbn-unlog
     (unless sym (error "%s" "Syntax: , (fmakunbound 'symbol)"))
     
     (setq sym
	   (erblisp-sandbox sym))
     
     (erbn-readonly-check sym)

     (ignore-errors
       (erbn-trash
	meta (list 'function sym)   (list (format "%S" (symbol-function sym)))))
     
     (let 
	 ;; this is to be returned..
	 ((result (fmakunbound sym))
	  (sexps       (erbutils-file-sexps erbn-pf-file)))
       
       ;; now we want to remove any definition of sym from the user
       ;; file: 
       
       (erbn-write-sexps-to-file
	erbn-pf-file
	(remove 
	 (first 
	  (member-if
	   (lambda (arg) (equal (second arg) sym))
	   sexps))
	 sexps))
       (fsi-pf-load)
       result))
    ))


(defmacro fsi-setq (&rest args)
  (erbn-log 'setq args)
  (erbn-unlog
  ;; (progn
   ;; (assert (= (length args) 2))
   (assert (= 0 (% (length args) 2)))

   ;; (erbn-readonly-check (first args))
   ;; All odd-numbered symbols must be readonly.
   (let ((firsts (number-sequence 0 (- (length args) 1) 2)))
     (loop for n in firsts do (erbn-readonly-check (nth n args))))

   ;; now do the actual setq
   `(let ((erbn-tmpsetq
	   (setq ,@args)))
      ;;(fsi-pv-save)
      (setf erbn-pv-needs-saving t) ;; This t is set inside a (let ) from erbot-remot.
      ;; the actual pv-save now happens via erbot-remote->buffers-save at the end. 
      erbn-tmpsetq)))





(defun fsi-constant-object-p (object)
  "If the object is a symbol like nil or t, a symbol that cannot be
redefunned, return true. "
  (or (member object (list nil t))
      (keywordp object)))



(erbutils-defalias-i '(type-of pp-to-string) 1)

(provide 'erbc3)
(run-hooks 'erbc3-after-load-hook)



;;; erbc3.el ends here
