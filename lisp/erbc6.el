;;;; erbc6.el --- Good fsbot functions originally contributed by freenode users that are now built-in and immutable.
;; The aim of this file is to collect good fsbot functions that have become an important part of fsbot, standardize them, save them here, and save them from any sabotage. 
;; NOTE that many fsbot functions, especially those in this file,  are completely implementable in userspace using the provided building blocks. 
;; Time-stamp: <2007-11-23 11:30:12 deego>
;; Copyright (C) 2003 D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc6.el
;; Package: erbc6
;; Author: D. Goel <deego3@gmail.com>, fsbot-users across channels such as #emacs.*
;; Keywords:
;; Version:
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; For latest version:
;; This file is NOT (yet) part of GNU Emacs.
 
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
 
;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.
 

;;; Real Code:






(defun fsi-C-h (sym &rest thing)
  "Syntactic sugar for some fsbot functions.
Coded by bojohan on #emacs, and is now part of erbc6.el."
  (cond
   ((eq sym 'f)
    (apply 'fsi-df thing))
   ((eq sym 'k)
    (apply 'fsi-dk thing)) 
    ((eq sym 'c)
     (apply 'fsi-describe-key-briefly thing))
    ((eq sym 'w)
     (apply 'fsi-dw thing))
    ((eq sym 'v)
     (apply 'fsi-dv thing))))


(defun fsi-apropos-term (&rest args)
  "Look for input string in the term. First arg is the TERM. The rest is the input string.
fsbot users had defunned various apropos-foos. deego: Generalize to apropos-term,  make part of erbc6 (userfunctions), and also return the index of the matched terms."
  (let*
      ;;((re (mapconcat 'symbol-name (cdr args) " "))
      ((re (fsi-stringify (cdr args)))
       (matches1 
	(mapcar 
	 ;; return x at last if it matches.. that way, we end up returning the matching entry.
	 (lambda (x) (and (string-match re x ) x))
	 ;; put (car args) in a list below, because that's when stringify does its thing. 
	 (fsi-notes (fsi-stringify (list (car args))))))
       (ctr -1)
       (matches
	(mapcar 
	 (lambda (item) (incf ctr) (if item (format "%d: %s" ctr item) nil))
	 matches1))
       (matches (remove nil matches)))
    (if matches 
	(erbutils-itemize matches)
	"No matches found.")))


(defun fsi-m8b (&rest args)
  (fsi-random-choose 
   (fsi-notes "m8b-list")))

   '("Yes!"
     "No!"
     "Definitely!"
     "Of course not!"
     "Highly likely!" 
     "Ask yourself, do you really want to know?" 
     "I'm telling you, you don't want to know!"
     "mu!"
     "Probably!"
     "Improbable!"
     "Inconceivable!"
     "Cannot predict now"
     "Ask again later"
     "Reply unclear, try again"
     "Don't count on it!"
     "My sources say no!"
     "Signs point to yes!"
     "Without a doubt!"
     "It is decidedly so!"
     "It is certain!"
     "Highly likely!"
     )



		     
(defun fsi-hello (&rest whos)
  "Programmed by emacsers! Was a lisp- term. Promoted to a built-in function. Greet and hi also alias to this. Heavily modified by deego since then."
  (let
      ((who
	(if whos (fsi-stringifyd whos))))
    (unless who
      (setq who (fsi-random-choose (list nick tgt))))
    (setq who (format "%s" who))
    (fsi-format (fsi-random-choose-term "greetings-list" t)
		who)))


(defun fsi-random-choose-term
    (str &optional skip-first &rest fsi-ignore)
  (sit-for 0)
  (fsi-random-choose
   (if skip-first
       (cdr
	(fsi-notes str))
     (fsi-notes str))))




(defalias 'fsi-hi 'fsi-hello)
(defalias 'fsi-greet 'fsi-hello)


;; A promoted userfunction.
(defun fsi-stringify-symbol-list (fs-ls &rest fs-ignore)
  (error "[fsi-stringify-symbol-list] use strigifyd instead.")
  (fs-mapconcat 'identity
		(fs-stringifyc fs-ls)
		" "))

;; 20160709 These functions are defuned here using fs-. The automatic conversion from fsi- doesn't seem to work for them.
(defun fsi-\, (&rest args)
    (error "[fs-,] Either you used too many commas, or you both (addressed me + used a comma)"))


(defun fsi-\\\, (&rest args)
  (error "[fs-\,] Either you used too many commas, or you both (addressed me + used a comma)"))



(defun fsi-\\\\\, (&rest args)
  (error "[fs-\\,] Either you used too many commas, or you both (addressed me + used a comma)"))


(defun fsi-\\\\\\\, (&rest args)
  (error "[fs-\\\,] Either you used too many commas, or you both (addressed me + used a comma)"))



;; 20201001 deego Tweak, add fs-ops, make it operate PER CHANNEL. Provide hints about any missing items, example, the relevant opslist.
(defun fsi-ops (&optional miscreant &rest reason)
  (if miscreant
      (let* ((listname (concat "ops-list-" fsi-tgt))
	     (notes (fsi-notes listname))
	     (ops
	      (if notes (mapconcat 'identity notes ", ")
		(format "<,%s>" listname)))
	     )
	(format "%s: %s is complaining about %s: %s" ops nick miscreant (fsi-stringifyd reason)))
    "To call the ops, use ,ops <nick-to-complain-about> [reason]; ops can add themselves using: ,ops-list-<#channel> is also <nick>"))



(provide 'erbc6)
(run-hooks 'erbc6-after-load-hook)



;;; erbc6.el ends here
