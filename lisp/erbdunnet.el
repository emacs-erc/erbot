;;; erbdunnet --- Another robot for ERC.
;; Time-stamp: <2006-08-24 11:11:11 deego>
;; Emacs Lisp Archive entry
;; Filename: erbdunnet
;; Package: erbdunnet
;; Authors:  Dave Goel <deego3@gmail.com>
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; Maintainer: Dave Goel



;; Version:
;; Keywords: ERC, IRC, chat, robot, bot

;; Copyright (C) 2018 Dave Goel

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



(defun erbot-dunnet (arg)
  "Glue the dunnet into the ERC robot."
  (with-timeout
      (3 "Dunnet error")
    (erbot-dunnet1
     (erbot-dunnet-failsafe arg))))

(defun erbot-dunnet-failsafe (arg)
  ;; die when inserting card!
  (if (or (string-match "put" arg)
	    (string-match "insert" arg))
      "die"
    arg))


(defun erbot-dunnet-reset-state ()
  "This will become a fsi function!"
  ;; (ignore-errors (kill-buffer "*dungeon*"))
  ;; to restart the game upon death, we can try to reset the state 
  (setq dun-visited '(27))
  (setq dun-current-room 1)
  (setq dun-exitf nil)
  (setq dun-badcd nil))

(defalias 'fsi-dunnet-reset-state  'erbot-dunnet-reset-state)

 
(defun erbot-dunnet1 (arg)
  (save-excursion
    (let ((freshp dun-dead)
	  (dun-batch-mode t)
	  outpoint res ans
	  (pre "")
	  full
	  (dunnet-mode-note
	   ;;"I am in dunnet-mode. For regular bot, type ,(dunnet-mode). For dunnet commands, just type them, or use (dunnet-command...)." )
	   ;; a CYA message. We don't really know if we are in dunnet mode.
	   "I MAY be in dunnet-mode. To toggle, type , (dunnet-mode). For dunnet commands, just type them when in dunnet-mode, or use (dunnet-command...)." )
	  )
      (when (or (not (boundp 'dun-dead)) dun-dead
 		(not (get-buffer "*dungeon*"))
 		)
 	(setq freshp t)
 	(setq dun-dead nil))
      (when freshp (dunnet))
      (when freshp (setf arg "look") (setf freshp nil))
      ;; (when dun-dead
      ;; 	(progn
      ;; 	  (ignore-errors (kill-buffer "*dungeon*"))
      ;; 	  ;; to restart the game upon death, we can try to reset the state 
      ;; 	  (setq dun-visited '(27))
      ;; 	  (setq dun-current-room 1)
      ;; 	  (setq dun-exitf nil)
      ;; 	  (setq dun-badcd nil)
	  
      ;; (when freshp (dunnet) dun-dead (sit-for 0.2))
      (set-buffer "*dungeon*")
      ;; switching is important, otherwise dunnet tends to insert its stuff in current buffer! 
      (switch-to-buffer "*dungeon*")
      (goto-char (point-max))
      (when (string-match "save" arg)
	(setq arg "save ~/pub/dunnet/dunnet.game")
	(setq pre "Will save to ~/pub/dunnet/dunnet.game"))
      (when (string-match "^.?reset" arg)
	(erbot-dunnet-reset-state))
       (when (string-match "reset" arg)
	(setq arg "die"))
      (cond
       ((string-match "^.?more" arg)
	(setq ans (fsi-more)))
       (t
	(insert arg)
	(sit-for 0.1)
	(goto-char (point-max))
	(setq outpoint (point))
	(dun-parse 1)
	(setq ans
	      (buffer-substring-no-properties
	       outpoint (- (point-max) 1)))
	(when (equal arg "quit")
	  (progn (kill-buffer "*dungeon*")))))
      (setq full (concat pre ans))
      (when
	  ;; we could test for dunnet-mode-i-ness here, but that's hard because that's a buffer-local variable.
	  (string-match
	   "I don't understand that"
	   full)
	(setq
	 full
	 (fsi-unfill 
	  (concat full dunnet-mode-note))))
      full)))

(defun erbot-dunnet-install ()
  "Defines some dunnet specific aliases. "
  (interactive)
  (require 'dunnet)
  ;; (defalias 'dun-read-line 'erbn-botread)
  (defun dun-type (&rest args) (dun-mprinc "You tried to type through a bot.") (dun-die "bot"))
  ;;(defalias 'dun-mprinc
  ;;'fs-dun-mprinc))
  (defun dun-read-line (&rest args) (dun-mprinc "Tried to read through a bot.") (dun-die "bot"))
  )




(provide 'erbdunnet)

;;; erbdunnet.el ends here

