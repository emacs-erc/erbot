;;; erbc.el --- Erbot user-interface commands -- see also erbc5.el
;; Time-stamp: <2006-09-28 12:17:42 deego>
;; Copyright (C) 2002--2023 and beyond D. Goel
;; Emacs Lisp Archive entry
;; Filename: erbc.el
;; Package: erbc
;; Author: D. Goel <deego3@gmail.com>
;; Version: 0.0DEV
;; URL:  http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot
;; Other files:
;;; erball.el --- Functions on all files.
;;; erbbdb.el ---
;;; erbc.el --- Erbot user-interface commands.
;;; erbc2.el --- mostly: special functions for erbc.el
;;; erbc3.el ---erbot lisp stuff which should be PERSISTENT ACROSS SESSIONS.
;;; erbc4.el --- Russian Roulette
;;; erbc5.el --- continuation of erbc.el
;;; erbc6.el --- fsbot functions contributed by freenode users,
;;; esp. #emacsers.
;;; erbc7.el --- continuation of erbcel
;;; erbcompat.el --- Erbot GNU Emacs/XEmacs compatibility issues
;;; erbcountry.el
;;; erbc-special.el --- Special/dangerous implementation functions.
;;; erbdata.el ---
;;; erbedit.el --- quicker operator editing of bots' bbdb
;;; erbeng.el ---  english
;;; erbforget.el --- Help make the bots forget some TERMS.
;;; erbkarma.el ---
;;; erblisp.el ---
;;; erblog.el ---
;;; erbot.el --- Another robot for ERC.
;;; erbp.el --- NOT FUNCTIONAL personal erbot-interface, stolen from dunnet.el
;;; erbtrain.el --- Train erbot (erbot)..
;;; erbutils.el ---  utils
;;; erbwiki.el ---


(defvar erbc-home-page
  "http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot")



;; This file is NOT (yet) part of GNU Emacs.

;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;; See also:




(defvar erbc-version "0.0dev")
(defvar erbc-version "0.0dev")


;;==========================================
;;; Code:

;; NOTE:  stuff like (fs-et) can be passed possibly mischievous
;; code as the first argument... never eval or "set" any
;; "code"... always convert it to a single atom... before setting it..



(require 'find-func)





(run-hooks 'fsi-before-load-hooks)


;; Real code

(defun erbn-type-check (expr &optional noerror)
  "We only deal with these types. Used in things such as erblisp-unsandbox, etc. See also erblisp-type-check.   "
  (let
      ((result (member (type-of expr)
		       '(string cons symbol integer float))))
    (when (and (not noerror) (not result))
      (error "[type-check] I should not be here. I found something that's not a string, cons, symbol, integer or float!"))
    result))

(defun erbn-shell-command (&optional command overridep)
  "Execute shell-commands when erbn-shell-command-p is true.

However, if the second argument overridep is non-nil, we use that to
determine whether to execute the command.  In that case, we execute
the command only if overridep is a list, whose first entry of that
list is non-nil"
  (cond
   ((or (and overridep
	     (listp overridep)
	     (first overridep))
	erbn-shell-command-p)
    (apply 'shell-command command nil))
   (t
    (error "The bot-operator has shell commands disabled"))))





(defun erbn-shell-command-to-string (&optional command overridep)
  "Execute shell-commands when erbn-shell-command-p is true.

However, if the second argument overridep is non-nil, we use that to
determine whether to execute the command.  In that case, we execute
the command only if overridep is a list, whose first entry of that
list is non-nil"
  (cond
   ((or (and overridep
	     (listp overridep)
	     (first overridep))
	erbn-shell-command-p)
    (apply 'shell-command-to-string command nil))
   (t
    (error "The bot-operator has shell commands disabled"))))





(defun fsi-get-google-defaults ()
  (cadr (assoc fsi-tgt erbn-google-defaults)))






(erbutils-defalias-n  'quote 1)


(defun fsi-correct-entry (name &rest ignore)
  "Assumes that name is a string... this downcases strings.  Rendering
it fit for database-entry. "
  (unless (stringp name) (setq name (format "%s" name)))
  ;;(downcase
  (let ((newname
	 (mapconcat 'identity (split-string name) "-")))
    (or (erbbdb-get-exact-name newname)
	newname)))


(defun fsi-describe-key-briefly (&optional key &rest args)
  "Return the function on key..building block for other erbc's..
If no such function, return the symbol 'unbound. "

  (unless key
    (error
     "Syntax:  , dkb key"))
  (when (and (null key) (null args))
    (setq key ""))
  (unless (stringp key)
    ;; is this safe? what about properties?
    (setq key (read-kbd-macro
	       (mapconcat '(lambda (arg) (format "%s" arg))
			  (cons key args)
			  " "))))
  (let ((res (key-binding key)))
    (if res res
      'unbound)))

;; for now..
;;(defalias 'fsi-describe-key 'fsi-describe-key-briefly)

(defun fsi-where-is-in-map (map &optional fcn)
  (let* ((wi (where-is-internal fcn map)))
    (mapconcat 'key-description wi ", ")))

(defun fsi-where-is-gnus-group (&optional fcn)
  (error "[wigg] disabled")
  (require 'gnus)
  (unless fcn (error "[wigg] please supply a function"))
  (fsi-where-is-in-map gnus-group-mode-map fcn))

(defun fsi-where-is-gnus-summary (&optional fcn)
  (error "[wigs] disabled")
  (require 'gnus)
  (unless fcn (error "please supply a function"))
  (fsi-where-is-in-map gnus-summary-mode-map fcn))

(defun fsi-where-is-message (&optional fcn)
  (error "[wims] disabled")
  (require 'gnus)
  (require 'message)
  (unless fcn (error "please supply a function"))
  (fsi-where-is-in-map message-mode-map fcn))



(defun fsi-keyize (key morekeys)
  (setq key (read-kbd-macro
	     (mapconcat '(lambda (arg) (format "%s" arg))
			(cons key morekeys) " "))))


(defun fsi-describe-key-one-line (&optional key &rest args)
  "Key, and just one line of function"
  (unless key (error "Syntax: , dk \"Key...\""))
  (let* ((fcn (apply 'fsi-describe-key-briefly key args))
	 (fcns (format "%s" fcn))
	 (apr (or (fsi-apropos-exact fcns)
		  "No doc. available. ")))
    (concat (format "%s -- %s"
		    fcns
		    apr))))

(defalias 'fsi-dko 'fsi-describe-key-one-line)

(defalias 'fsi-describe-key 'fsi-describe-key-and-function)

(defun fsi-lookup-key-from-map-internal (&optional map key &rest morekeys)
  (unless key (error "[lookup internal] No key supplied. "))
  (unless (stringp key)
    (setq key (read-kbd-macro
	       (mapconcat '(lambda (arg) (format "%s" arg))
			  (cons key morekeys) " "))))
  (unless (arrayp key) (setq key (format "%s" key)))
  (let* ((fcn (lookup-key map key))
	 (fcns (format "%s" fcn))
	 (apr (or (fsi-apropos-exact fcns)
		  "No doc available. ")))
    (concat (format "%s -- %s" fcns apr))))

(defun fsi-lookup-key-gnus-group (&optional key &rest args)
  (error "lkgg disabled")
  (unless key (error "Syntax: , lkgg \"Key...\""))
  (require 'gnus-group)
  (apply 'fsi-lookup-key-from-map-internal gnus-group-mode-map key args))

(defun fsi-lookup-key-gnus-summary (&optional key &rest args)
   (error "lkgs disabled")
   (error "lkgs disabled")
 (unless key (error "Syntax: , lkgg \"Key...\""))
  (require 'gnus)
  (apply 'fsi-lookup-key-from-map-internal gnus-summary-mode-map key args))

(defun fsi-lookup-key-message (&optional key &rest args)
   (error "lkgm disabled")
  (unless key (error "Syntax: , lkgg \"Key...\""))
  (require 'gnus)
  (require 'message)
  (apply
   'fsi-lookup-key-from-map-internal gnus-message-mode-map key args))



(defun fsi-apropos-exact (str)
  (unless (stringp str) (setq str (format "%s" str)))
  (let* ((reg (concat "^" (regexp-quote str) "$"))
	 (apr (apropos reg))
	 (asso (assoc* str apr
		       :test
		       (lambda (a b)
			 (string= (format "%s" a) (format "%s" b)))))

	 (val (second asso)))
    (if val (format "%s" val)
      nil)))

(defun fsi-describe-key-long (k &rest args)
  (let ((f (apply 'fsi-describe-key-briefly k args)))
    (fsi-describe-function-long f)))

(defun fsi-describe-key-and-function (key &rest args)
  "Describe the key KEY.
Optional argument ARGS .  If the input arguments are not strings, it
kbds's them first... , so that , df C-x C-c works"
  (when (and (null key) (null args))
    (setq key ""))
  ;;(unless (stringp key)
  ;; Run through the read-kbd-macro unconditionally. This will make things like M-; work!
  (progn
    (setq key (read-kbd-macro
	       (mapconcat '(lambda (arg) (format "%s" arg))
			  (cons key args)
			  " "))))
  (let ((b (key-binding key)))
    (cond
     ((symbolp b)
      (or
       (ignore-errors (fsi-describe-function b))
       (format "Bound to: %s" b)))
     (t
      (format "Bound to: %s" b)))))



(defun fsi-describe-function (&optional function nolimitp &rest ignore)
  "Describes the FUNCTION named function.
Also tries an fs- prefix for the function..
The argument nolimitp is now obsolete and has no effect. Since the output gets paginated in any case, we don't need to limit the output. "
  (unless function
    (error
     "Syntax: (describe-function 'name-of-function) or , df name"))
  (let* ((f function)
	 g
	 (varp nil)
	 )
    (when (stringp f)
      (setq f (erbn-read f)))
    (assert (symbolp f))
    (setq g
	  (erbn-read (concat "fs-" (format "%s" f))))
    ;; set g and varp correctly.
    (cond
     ((fboundp f) (setq g f))
     ((fboundp g) t)
     ((boundp f) (setq varp t g f))
     ((boundp g) (setq varp t))
     (t (error "[df] No function or variable found matching %s or %s. Did you already ask me to , (require 'relevant-library) ? My emacs version is %s" f g emacs-version)))
    (cond
     (varp (concat 
	    (format "[df] No matching function found. Variable description for %s: \n"
		    g)
	    ;; also sets the task.
	    (fsi-describe-variable g)))
     (t
      (let* ((def (symbol-function g)))
	(ignore-errors
	  (if (equal 'autoload (car-safe def))
	      (load (second def))))
	;; this check does nothing now.. need ro
	
	(erbn-task-set (format "df:%s" g))
	(if (equal nolimitp 'nolimit)
	    
	    ;;(let ((fsi-limit-lines 8))
	    ;;(fsi-limit-lines (describe-function g)))
	    (erbutils-describe-function g)
	  (erbutils-describe-function g)

	  ))))))


(defun erbutils-describe-function (g)
  ;; DO NOT unfill. To see why, see <deego> ,dv custom-theme-load-path
  (fsi-functionchaseorerror g)
  (erbutils-remove-text-properties (describe-function g)))




(defun fsi-where-is (function &rest args)
  "Tells what key the function is on..

"
  (let* (
	 (str0 "")
	 (str1 "")
	 (str2 "")
	 (str3 "")
	 )
    (cond
     ((stringp function) (setq function (erbn-read function)))
     (t nil))
    (cond
     ((null function)  (format "Sorry, %s is not a symbol" function))
     ((symbolp function)
      (unless (fboundp function) (setq str0 "Either unbound or.. "))
      (setq str2
	    (with-temp-buffer
	      (where-is function t)
	      (erbutils-buffer-string)))
      (concat str0 str1 str2 str3))
     (t (format "Looks like %s is not a symbol" function)))))

(defun fsi-describe-function-long (function &rest ignore)
  "Similar to describe-function, but does not limit the strings.
Use with caution only in privmsgs please, for may produce long outputs. UPDATE: As of 20160509, should be identical to fsi-describe-function."
  (fsi-describe-function function 'nolimit))


(defun fsi-describe-variable-long (variable &rest ignore )
  "Similar to describe-variable, but does not limit strings.."
  (fsi-describe-variable variable 'nolimit))

(defun fsi-describe-variable (&optional variable &rest ignore)
  "Describes a VARIABLE.."
  (unless variable (error "Syntax: , dv variable"))
  (let* ((f variable))
    (if (stringp f)
	(setq f (erbn-read f)))
    (cond
     ((symbolp f)
      (erbn-task-set (format "dv:%s" variable))
      ;; do NOT unfill. For a reason why, see the output of ,dv custom-theme-load-path
      ;; (fsi-unfill-string (erbutils-describe-variable f))
      (erbutils-describe-variable f)
      )

     ;; if list, DO NOT wanna eval it-->
     (t
      "NO variable specified"))))

(defalias 'fsi-parse 'fsi-lispify)
(defalias 'fsi-parse-english 'fsi-lispify)

(defun fsi-require (feature &rest ignore)
  "Make the bot require the feature FEATURE so that the commands such as df or dv work. 

Note that this merely loads the feature in fsbot's emacs. It doesn't magically create an irc-safe  fs- interface for all newly loaded functions.

Feature names allow only alphanumeric characters, and a dash, but don't allow anything starting with a dash. Thus, if you have files in the load-path that shouldn't be allowed to be loaded, make sure they have either (a) a non-alphanumeric character somewhere other than dash, or (b) they start with a dash.   The criterion for allowing a feature is subject to change. 
"
  (erbn-log 'require feature ignore)
  
  (erbn-unlog
   ;; note: one of the features is find-file. Thus, -. 
   (let ((match "^[a-zA-Z][-a-zA-Z0-9]*$")
	 (err "Syntax: ,(require 'SYMBOL) "))
     (unless (string-match match (format "%s" feature))
       (error err))
     (if (stringp feature)
	 (setq feature (fsi-read feature)))
     (unless (string-match match (format "%s" feature))
       (error err))
     ;; (when (or (string-match "/" (format "%s" feature))
     ;; 	     (string-match "\\\\" (format "%s" feature)))
     ;;   (error "Your safety is VERY important to us, so we avoid loading features containing slashes."))
     (when (member feature features)
       (error "[require] Feature %s is already loaded." feature))
     (if
	 (symbolp feature)
	 (if (member feature erbn-features-whitelist)
	     (format "%s" (require feature))
	   (error "[require: whitelist-check] Is %s part of emacs? Safe & well-behaved? Does loading it leave all defaults intact? Please let the op know." feature))
       (error "[require] I could not parse: %s as symbol for 'require" feature)))))




(defun fsi-respond-to-query-p (msg)
  ;; if it is of the form resolve? the user KNOWS what resolve or
  ;; kensanata is, and is not asking for information. So, please don't
  ;; respond in such a case.
  (not
   (member msg (mapcar 'first (fsi-channel-members-all)))))


(defun fsi-parse-preprocess-message (msg)
  (let ((len (length msg)))
    (when (and
	   (> len 0)
	   (member (aref msg (- len 1))
		   fsi-internal-parse-preprocess-message-remove-end-chars)
	   (setq msg (subseq msg 0 -1)))))
  msg)


(defun fsi-lispify (&optional msg proc nick tgt localp
				 userinfo &rest foo)
  "Parse the english MSG into a lisp command.

If it is an 'is', it should always be the second word ..
viz: we had better use hyphens in the first word..
MSG is a string..
Is the main function.. but also available to the user as a command...

NB: The end-result is always an expression.. and NOT a strign..


Just once in a blue moon, this will, at random, even parse messages
not addressed to it...

Finally, wanna parse messages whose last item contains erbot..
Optional argument PROC .
Optional argument NICK .
Optional argument TGT .
Optional argument FOO .

We will also bind a number of variables, as appropriate, for example,
fsi-msg*, fs-lispargs, fs-lispa , fs-lispb... so that these vars can be used
anywhere in the code, or the user-defined parts of the code...

In the grand scheme of things, these bindings should turn out to be
local, because the parent function calling this function should have
'letted these variables.

"
  ;;(when (stringp msg)
   ;; (setq  msg (split-string msg)))
  ;msg
  ;proc
  ;nick
  ;tgtg
  ;foo
  (setq fsi-internal-original-message msg)
  (setq msg (fsi-parse-preprocess-message msg))
  (setq fsi-msg msg)
  ;; (setq erbn-msg msg)  ;; Don't set it here pls. It will get re-set any time there is reprocessing. Instead, set it once at erbot-remote.
  (setq fsi-msgsansbot msg)
  (let*
      (


       (msg (fsi-parse-preprocess-message msg))
       ;; msge == msg english. when trying to parse english, we try not to let symbols like ; # ` bother us. in that case, we use msg.
       ;; (msge (fsi-preprocessb msg))
       (origmsg msg)
       ;; (origmsge msg)
       ;;(fsi-internal-message-sans-bot-name fsi-internal-message-sans-bot-name)
       (foundquery nil)
       (foundquerydouble nil)
       (foundkarma nil)
       ;; if t, means either our name was at last, or eevn if at
       ;; first, they weren't really addressing us..
       ;;(addressedatlast nil)
       (leave-alone-p t)
       ;;(fsi-nick nick)
       bluemoon
       )
    (unless (stringp origmsg)
      (setq origmsg (format "%s" origmsg)))

    (unless msg
      (error "Syntax: %s (parse \"Emacs is an editor\")" erbn-char))
    (unless (stringp msg)
      (setq msg (format "%s" msg)))
    ;; remove leading spaces..
    (while
	(and (> (length msg) 0)
	     (equal (aref msg
			  0) 32))
      (setq msg (substring msg 1)))

    ;; remove trailing spaces..
    (while
	(and (> (length msg) 0)
	     (equal (aref msg (- (length msg) 1)) 32))
      (setq msg (substring msg 0 (- (length msg) 1))))

    (when (and tgt proc)
      (set-buffer (erc-get-buffer tgt proc)))

    (when
	(and (stringp msg)
	     (string-match "\\(\\+\\+\\|\\-\\-\\)$" msg)
	     (<= (length (split-string msg)) 2))
      (setq foundkarma t))
    ;; 2003-11-14 T15:36:38-0500 (Friday)    D. Goel
    ;; requested by elf: 
   ;; if double ??, then make it a call to m8b
    (when
	(and fsi-mod-keywords
	     (fsi-mod-keywords-match msg))
      (setf msg (concat erbn-char " (kick \"Such discussions are not welcome here.\") ")))
    (when (and
	   fsi-m8b-p
	   (if (stringp fsi-m8b-p)
	       (and (stringp tgt) (string-match fsi-m8b-p tgt))
	     t))
      (let (len)
	(when (and (stringp msg)
		   (progn
		     (setq len (length msg)) t)
		   (> len  1)
		   (string= "??"
			    (substring msg (- len 2) len))
		   ;;(or
		   ;;(string-match
		   ;;erbot-nick msg)
		   ;;(string-match (concat "^" erbn-char) msg)
		   ;;(string-match erbn-char-double  msg))
		   )
	  (setq foundquerydouble t)
	  (setq msg (concat erbn-char " (m8b)")))))

    (when (and (stringp msg)
	       (> (length msg) 0)
	       ;; ignore trailing ?
	       (equal (aref msg (- (length msg) 1)) 63))
      (progn
	(setq foundquery t)
	(setq msg (substring msg 0 (- (length msg) 1)))))

    (setq leave-alone-p t)
    (setq bluemoon
	  (or
	   ;; responding to a general list conversation..
	   (and
	    (fsi-blue-moon)
	    (string-match fsi-internal-query-target-regexp tgt))

	   ;; responding in general..
	   (and (equal nick tgt)
		(or
		 (stringp nick)
		 ;; parse commands -->
		 (null nick)
		 ))
	   )
	   )
	       
    (unless (stringp msg)
      (setq msg ""))
    

    ;; convert midsentence ,, to parsable sentence.
    (let (pos)
      (when
	  (and 
	   ;;201406 deego
	   ;; This used to avoid the ,, functionality when the whole message begins with ,
	   ;; But, the problem with that is that a sentence like this:
	   ;; ,,foo is bar ,, more stuff.
	   ;; gets parsed as (set-term ",foo" "bar").
	   ;; so, disable this check altogether. 
	   ;;(not (equal 0
	   ;;(string-match erbn-char msg)))
	   (not
	    (let ((nickpos (string-match erbot-nick msg)))
	      (and nickpos
		   (< nickpos 3))))
	   ;; part of and
	   (setq pos
		 (string-match erbn-char-double msg)))
	(setq msg (substring msg (+ pos 1))
	      erbn-internal-midsentence-p t)
	;; NB that erbn-internal-midsentence-p is set to t above even
	;; if we only discover ONE ,, and not a pair of (beginning and
	;; ending),,'s. The reason: This var. is used to suppress
	;; adding nick: when replying.
	(when (setq pos (string-match erbn-char-double msg))
	  (setq msg (substring msg 0 pos)))
	))
    ;; deal with the leading , or ,,
    (when (equal 0
		 (string-match erbn-char msg))
      (let ((restmsg (substring msg 1)))
	(when (equal 0 (string-match "," restmsg))
	  (setq restmsg (substring restmsg 1)))
	(setq msg (concat erbot-nick ": " restmsg))))


    ;; now we split strings..
    (setq msg (split-string msg))
    (setq fsi-msglist msg)
    (setq fsi-msglistsansbot msg)
    (cond
     ( (and (first msg)
	    (let ((pos
		   (string-match erbot-nick (first msg))))
	      (and pos (< pos 1))))
       ;;(or
       ;;(erbutils-string= (first msg) erbot-nick)
       ;;(erbutils-string= (first msg) (concat erbot-nick ","))
					;(erbutils-string= (first msg) (concat erbot-nick
					;":")))
       (progn
	 (unless
	     (or
	      (string-match (concat erbot-nick ":") (first msg))
	      (string-match (concat erbot-nick ",") (first msg))
	      (null (second msg))
	      (string-match "^," (second msg))
	      (string-match "^:" (second msg)))
	   (setq fsi-internal-addressedatlast t))
	 (when (> (length msg) 1)
	   (setq msg (cdr msg)))
	 (setq leave-alone-p nil)))


     ;; if it is a short sentence ending in fsbot..
     ((and (first (last msg)) (string-match erbot-nick (first (last
							       msg)))
	   (< (length msg) 5))
      ;; don't want this any more.. since no sense in removing the
      ;; last term.  Example: Want: what is erbot?  to stay that way.
      ;;(progn
      ;;(setq msg (reverse (cdr (reverse msg)))))
      (when leave-alone-p
	(setq fsi-internal-addressedatlast t))
      (setq leave-alone-p nil))



     ;; this might be dangerous if nick is a small word like "apt"..
     ;; this also means :( thagt erbot will intervene when users are
     ;; talking about her, but not TO her..
     ;; nah, leave this one out..
     ;;((member erbot-nick msg)
     ;; (setq leave-alone-p nil))

     (bluemoon
      (setq leave-alone-p nil)))

    (setq fsi-internal-message-sans-bot-name
	  (mapconcat 'identity msg " "))

    
    ;; note: msg is a list at this stage, and fsi-internal-messag-sans-bot-name is a string. 

    (when (and
	   foundquery
	   ;; if tgt is nil, we are being asked to parse
	   ;; something.. so cool
	   tgt
	   (string-match fsi-internal-query-target-regexp tgt))
      ;; if this condition causes the thing to be triggerred, then
      ;; setq temporarily, a global variable... so responses are muted
      ;; in general..
      (let ((goonp nil) (newmsg msg))
	(cond
	 ((equal (length msg) 1)
	  (setq goonp
		;; setq to t only if the content of the msg represents
		;; something the user might be interested in.
		(fsi-respond-to-query-p (first msg))

		))
	 (t
	  (setq goonp t)
	  ;; convert what's to what is
	  (when (stringp (first newmsg))

	    ;; 20180607 replace fancy comma by comma
	    (setf (first newmsg) (erbutils-replace-string-in-string (string 8217) "'" (first newmsg)))
	    (setf (first newmsg) (erbutils-replace-string-in-string "`" "'" (first newmsg)))
	    (setq newmsg
		  (append
		   (split-string (first newmsg) "'")
		   (cdr newmsg))))
	  (if  (and goonp
		    (member
		     (erbutils-downcase (first newmsg))
		     fsi-internal-questions))
	      (setq newmsg (cdr newmsg))
	    (setq goonp nil))
	  (if  (and goonp
		    (member
		     (erbutils-downcase (first newmsg))
		     '("s" "is" "are" "was" "were"
		       ;;"am"
		       )))
	      (setq newmsg (cdr newmsg))
	    (setq goonp nil))

	  ;; remove articles
	  (if  (and goonp
		    (member
		     (erbutils-downcase (first newmsg))
		     fsi-internal-articles))
	      (setq newmsg (cdr newmsg)))
	  (unless (equal (length newmsg) 1)
	    (setq goonp nil))))
	(when goonp
	  (when leave-alone-p (setq fsi-found-query-p t))
	  (setq leave-alone-p nil)
	  (setq msg (list "(" "describe"
			  (format "%S" (first newmsg))
			  "0" ")"
			  ))
	  ;; (message "%S" msg) ;; to debug.
	  ;; testing:
	  ;;(setq msg (list "(" "describe" "\"wiki\"" "0" ")"))
	  ;;(message "again: %S" msg)
	  ))
      )

    ;; Sat Jan  8 12:40:46 EST 2005 (petekaz)
    ;; We need to make sure this is the last thing we check
    ;; because we don't want to hijack another valid command
    ;; with our parsing.  I.e. if a user adds a term with an
    ;; url included in its note, we don't process that.
    (when (and leave-alone-p
               fsi-web-page-title-p
               (if (stringp fsi-web-page-title-p)
                   (and (stringp tgt)
                        (string-match fsi-web-page-title-p tgt))
                 t))
      (let* ((case-fold-search t)
             (url (some 'erbutils-html-url-p msg)))
        (when url
          (setq leave-alone-p nil)
          (setq msg (list "(" "web-page-title" (format "%S" url) ")")))))

    ;;       (cond
    ;;        ((equal (length msg) 1)
    ;; 	(when leave-alone-p
    ;; 	  (setq fsi-found-query-p t))
    ;; 	(setq msg (cons "describe" msg))
    ;; 	(setq leave-alone-p nil))
    ;;        ((and
    ;; 	 (equal (length msg) 3)
    ;; 	 (member (erbutils-downcase (first msg))
    ;; 		 fsi-internal-questions)
    ;; 	 (member (erbutils-downcase (second msg))
    ;; 		 '("is" "are")))
    ;; 	(setq msg (cons "describe" (cddr msg)))
    ;; 	(when leave-alone-p
    ;; 	  (setq fsi-found-query-p t))
    ;; 	(setq leave-alone-p nil))
    ;;        ((and
    ;; 	 (equal (length msg) 3)
    ;; 	 (member (erbutils-downcase (first msg))
    ;; 		 fsi-internal-questions)
    ;; 	 (member (erbutils-downcase (second msg))
    ;; 		 '("is" "are")))
    ;; 	(setq msg (cons "describe" (cddr msg)))
    ;; 	(when leave-alone-p
    ;; 	  (setq fsi-found-query-p t))
    ;; 	(setq leave-alone-p nil))


    ;;))

    ;; finally, ignore bots/fools..
    (let ((ui (format "%S" userinfo)))
      (when
	  (or
	   erbot-flood-p
           (and erbot-use-whitelist
                (stringp nick)
                (not (member-if
                      (lambda (arg)
                        (string-match arg nick))
                      erbot-whitelist-nicks)))
	   (and (stringp nick)
		(member-if
		 (lambda (arg)
		   (string-match arg nick))
		 erbot-ignore-nicks))

	   (some
	    'identity
	    (mapcar
	     (lambda (ignorethis)
	       (string-match ignorethis
			     ui))
	     erbot-ignore-userinfos)))
	(setq leave-alone-p t)))


    (setq fsi-msglistsansbot msg)
;;;====================================================
    ;; now do the work..
    (if leave-alone-p
	;; not addressed to us, so return nil and be done..
	nil
      ;; else.. viz: go on...
      (progn
	(erblog-log-target tgt)
	(let* (;(found nil)
	       
	       (newmsglist nil)
	       
	       (msgstr (erbutils-stringify msg))
					;(newstrmsg nil)
	       (lispmsg (erbn-read msgstr)
		))


	  ;; do a dead check
	  (when erbn-dead-check-p (and (not foundquery)
				       (erbn-dead-check)))


	  (setq
	   newmsglist
	   (cond

	    ;; are in a read mode..
	    (erbn-read-mode
	     (erbn-botread-feed-internal msgstr))



	    ;; look for a valid lisp form, then it just needs to be sandboxed
	    ((or
              (consp lispmsg)
              (and fsi-internal-max-lisp-p (numberp lispmsg))
              (and fsi-internal-max-lisp-p (stringp lispmsg))
              (and (symbolp lispmsg)
                   (let ((newsym
                          ;;(intern (format "fs-%S" lispmsg))
                          (erblisp-sandbox lispmsg)))
                     (or
                      (equal 0
                             (string-match "fs-"
                                           (format "%S" lispmsg)))
                      (and
                       (boundp newsym)
                       (not (fboundp newsym)))))))
	     ;;(erblisp-sandbox-fuzzy lispmsg)
	     (erblisp-sandbox lispmsg)
	     )


	    ;; from now on, we are in english mode. So, let's preprocessb everything, to take care of stuff like `.

	    (fsi-dunnet-mode
	     (fsi-dunnet-command msgstr))


	    ;; call to arbitrary function without parens
	    ;; prefer this before is etc. so that "how is it going"
	    ;; resolves properly..
	    ((or
	      ;; fboundp ==> allowing macros as well..
	      ;;(fboundp (intern (concat "fs-" (first msg))))
	      (fboundp (erblisp-sandbox (intern (first msg))))
	      ;;(functionp (intern (concat "fs-" (first msg))))
	      (equal 0 (string-match "fs-" (first msg))))

	     ;; 20240704 note that this sandboxing fails in certain cases.  For example, if the user msg was "s .". in that case, because (read-from-string ".") fails, this also fails!
	     ;; in these cases, the user is invoking a function but using the english sentence. However, the parser fails to convert it to lisp. We can't possibly deal with all such english edge cases! The user will simply have to (s ".") when encountering these.
	     
	     ;; this works great, except that we would like to quote the
	     ;; internals... because that is the most commonly used
	     ;; characteristic..
	     ;;`("(" ,@msg ")")
	     (erblisp-sandbox-full
	      ;;`( ,(intern (first msg)) ,@(erbutils-quote-list
	      ;;(mapcar 'intern (cdr msg))))
	      ;;(read (cons (intern (first msg))
	      ;;	  (read (list (erbutils-stringify (cdr msg))))))
	      ;; Note that this lisp preprocessing ONLY happens if the message is not already in a lisp form.
	      
	      (fsi-read
	       (erbutils-irc-lisp-preprocess 
		(concat "( "(erbutils-stringify msg) " )")))))

	    ((equal 0
		    (string-match "\\(s\\|r\\)/" (first msg)))
	     (erbn-replace-string-from-english-internal
	      msg))
	    ((equal 0
		    (string-match "[0-9]+->" (first msg)))
	     (fsi-rearrange-from-english-internal msg))
	    (
	     (and


	      (or (erbutils-string= (second msg) "is" t)
		  (erbutils-string= (second msg) "are" t)
		  ;;(erbutils-string= (second msg) "am" t)

		  )
	      (member (erbutils-downcase (first msg))
		      fsi-internal-questions-all
		      ))


	     ;;`(apply 'fs-describe ',(cddr msg))
	     `(funcall 'fsi-describe
		       ',(third msg)
		       nil nil nil ,"origmsg"
		       )

	     )

	    ;; some english constructs first...

	    ;; search removed---because: is a functionp...
	    ;;((erbutils-string= (first msg) "search")
	    ;; (setq newmsglist
	    ;;	     `("(" "search" ,@(cdr msg) ")")))
	    ((and

	      ;; do not want to take such cases, 100% are annoying
	      ;; false matches.
	      (not fsi-internal-addressedatlast)

	      (or
	       (erbutils-string= (second msg) "is" t)
	       (erbutils-string= (second msg) "are" t))
	      ;;(erbutils-string= (third msg) "also" t)
	      (member-ignore-case (third msg)
				  (list "also" "also,"))
	      )
	     (erblisp-sandbox-fuzzy
	      `(
		fs-set-also ,(first msg)
				;;,@(erbutils-quote-list (cdddr msg))
				,(erbutils-stringify (cdddr msg))
				)))
	    ((and (erbutils-string= (first msg) "tell")
		  (erbutils-string= (third msg) "about"))
	     `(fs-tell-to
	       ,(erbutils-stringify (cdddr msg))
	       ,(format "%s"
			(second
			 msg))
	       ))

	    (
	     (and
	      ;; do not want to take such cases, 100% are annoying
	      ;; false matches.
	      (not fsi-internal-addressedatlast)

	      (or (erbutils-string= (second msg) "is")
		  (erbutils-string= (second msg) "are"))

	      (not (member-ignore-case (third msg)
				  (list "actually")))
	      
	      )
	     (erblisp-sandbox-fuzzy
	      `(fs-set-term
		;; a string.. so we are safe..
		,(first msg)
		;; another string... so we are safe..
		,(erbutils-stringify (cddr msg)))))



	    ((and
	      (not fsi-internal-addressedatlast)
	      (or
	       (erbutils-string= (first msg) "no" t)
	       (erbutils-string= (first msg) "no," t))
	      (or
	       (erbutils-string= (third msg) "is")
	       (erbutils-string= (third msg) "are")
	       ))
	     (erblisp-sandbox-fuzzy
	      `(fs-set-force ,(second msg)
			     ;;,@(erbutils-quote-list (cdddr msg))))
			     ,(erbutils-stringify (cdddr msg))))
	     )



	    ;; benaiah's idea 20150514 
	    ((and
	      (not fsi-internal-addressedatlast)
	      (or
	       (erbutils-string= (second msg) "is")
	       (erbutils-string= (second msg) "are"))
	      (erbutils-string= (third msg) "actually"))
	     
	     (erblisp-sandbox-fuzzy
	      `(fs-set-force ,(first msg)
			     ;;,@(erbutils-quote-list (cdddr msg))))
			     ,(erbutils-stringify (cdddr msg))))
	     )



	    

	    
	    ((let ((foo (first msg)))
	       (and
		(not fsi-internal-addressedatlast)
		(<= (length msg) 2)
		(string-match "\\(\\+\\+\\|\\-\\-\\)$" foo)
		(not (fsi-notes foo
				 ))))
	     (let* ((foo (first msg))
		    (sec (second msg))
		    (bar (substring foo 0 -2))
		    (plusp (string-match "\\+\\+$" foo)))
	       (if plusp
		   ;; `(fs-karma-increase ,bar ,sec)
		   `(fs-karma-increase ,bar) 
		 `(fs-karma-decrease ,bar ,sec)
		 )))
	    ((or fsi-internal-addressedatlast
		 (and fsi-internal-botito-mode (> (length msg) 3)))
	     `(funcall 'fsi-english-only ,origmsg ,fsi-internal-addressedatlast))

	    (t
	     ;;`(apply 'fs-describe ',msg)

	     ;;`(funcall 'fs-describe ',(first msg)
	     ;;	       ',(second msg)
	     ;;	       ',(third msg)
	     ;;	       nil
	     ;;	       ,origmsg
	     ;;	       )
	     `(funcall 'fsi-describe-from-english
		       ,origmsg
		       ',msg)




	     )
	    ))
	  ;; this should be "%S" and not "%s" the lattwer will convert
	  ;; (dk "k") into (dk k)
	  (format "%S" newmsglist))))))




(defun fsi-preprocessb (msg)
  "Extra pre-processing in case we do an english parsing... We appropriately escape the message so that stuff like ` gets quoted and the message is read normally."
  (cond
   ((stringp msg)
	   (erbutils-replace-string-in-string msg "`" "\`" msg ))
   ((listp msg)
    (setf msg (mapcar 
	       (lambda (m) (erbutils-replace-string-in-string "`" "\'" m))
	       msg)))
   (t msg)))
    

(defun fsi-describe-from-english (&optional origmsg msg)
  " Use fs-describe instead.

fs-describe-from-english is an internal version of fs-describe. It uses several fallbacks in case nothing matches. You are most likely looking for (fs-describe) instead. 

Call fs-describe appropriately.
ORIGMSG is in english.
MSG is a list..

Plan

For multiple words, commence a search foo.*bar.*baz IF WE KNOW THAT
SEARCH or SEARCH--WIDE WILL SUCCEED, which will then, of course, go to
search-wide if it fails.

Else, of course, do the usual thing: viz. call describe...


"
  (progn "REMINDER FOR USERS BROWSING THIS: You are most likely looking for 'fs-describe instead of 'fs-describe-from-english")
  (unless (and origmsg msg)
    (error "[describe from english] This function needs 2 arguments. "))
  (let ((len (length msg))
	(catcount 0)
	mainterm firstterm remainder N M prestring expr tmpv
	(searchp nil)
	(multitermp nil)
	(fsi-internal-google-level fsi-internal-google-level)
	)
    (cond
     ((<= len 1)
      (if (fsi-notes (first msg))
	  (fsi-describe 
	   (first msg)
	   nil nil nil origmsg)
      (fsi-describe 
       (fsi-generalize-search-term (first msg))
       nil nil nil origmsg)))
     (t
      (setq mainterm (first msg))
      (setq firstterm mainterm)
      (setq remainder (cdr msg))
      (while
	  (and
	   remainder
	   (progn
	     (setq tmpv (first remainder))
	     (and (not (integerp tmpv))
		  (progn
		    (unless (stringp tmpv) (setq tmpv (format "%s"
							      tmpv)))
		    (not (integerp (ignore-errors (erbn-read tmpv))))))))
	;;(setq searchp t)
	(incf catcount)
	(when (< catcount 2)
	  (setq mainterm
		(concat mainterm ".*" tmpv))) ;; don't append too many symbols, else a long search. Do pop the remainder below in any case. 
	(setq multitermp t)
	(pop remainder))
      ;; why is this true only for multitermp???
      ;; Ah, because we say: if you end up searching and there are
      ;; multiple terms, you might as well include a result from
      ;; google among the search results.
      (when multitermp
	(setq fsi-internal-google-level (+ fsi-internal-google-level 25)))

      (when (and multitermp
		 ;; viz. if it will work
		 (second (fsi-search-basic
			  mainterm nil nil 'describe)))
	(setq searchp t))


      (if searchp
	  (fsi-search
	   mainterm (first remainder) (second remainder)
	   "Try: " origmsg)
	(fsi-describe
	 (fsi-generalize-search-term-maybe firstterm) (first remainder) (second remainder)
	 (third remainder) origmsg))))))



(defun fsi-generalize-search-term-maybe (term)
  ;; If there is an exact match, do not generalize.
  (if (fsi-notes term)
      term
    (fsi-generalize-search-term term)))

(defun fsi-generalize-search-term (term)
  (erbutils-replace-string-in-string "-" "[ -]*" term))

;; (defalias 'fs-hello 'fs-hi)
;; (defalias 'fs-hey 'fs-hi)

(defalias 'fsi-thanks 'fsi-thank)
(defun fsi-thank (&rest args)
  (let ((aa (erbutils-random '("no problem" "you are welcome"

			       ))))
    (eval
     (erbutils-random
      '(
	(concat aa erbn-char " " fs-nick)
	(concat fs-nick erbn-char " " aa))))))




;;; (defun fsi-ni (&optional nick &rest args)
;;;   ".
;;; Optional argument NICK .
;;; Optional argument ARGS ."
;;;   (if (and nick (not (string-match erbot-nick (format "%s" nick))))
;;;       (format "NI %s !!"
;;; 	      (let ((foo (split-string (format "%s" nick )
;;; 				       "[^a-bA-Z0-0]")))
;;; 		(or (first foo) nick))
;;; 	      )
;;;     (fs-describe "hi")))

;;; (defun fs-greet (&optional nick &rest foo)
;;;   "Nada..just a call to `fs-hi'.
;;; Optional argument NICK ."
;;;   (fs-hi nick))

(defun fsi-kiss (&optional nick &rest foo)
  "Nada.
Optional argument NICK ."
  (setq nick (format "%s" (or nick "itself")))
  (cond
   ((member nick (list erbot-nick "yourself" "self"))
    (eval
     (erbutils-random
      '("I'd rather kiss you"
        "Kiss myself? Why?"))))
   (t
    (eval
     (erbutils-random
      '((format "/me kisses %s" nick)
        (format "/me gives %s a big smooch" nick)
        (format "/me runs in the other direction, shouting NEVER!!")))))))

(defun fsi-hug (&optional nick)
  (unless nick (setq nick "itself"))
  (setq nick (format "%s" nick))
  (cond
   ((member nick (list erbot-nick "yourself" "self"))
    (eval
     (erbutils-random
      '("But i do that all the time. "
	"Hug myself? Why?"))))
   (t
    (eval
     (erbutils-random
      '((format "/me gives %s a tight hug" nick)
	(format "/me clings to %s" nick)
	(format "/me runs in the other direction, shouting NEVER!!")
	(format "/me grabs hold of %s and vows to never let go" nick)
	(format "/me grabs hold of %s and vows to never let go" nick)))))))




(defun fsi-love (&optional nick &rest bar)
  ".
Optional argument NICK ."


  (let ((nonep nil))
    (unless nick (setq nick "a random emacser") (setq nonep t))
    (setq nick (format "%s" nick))
    (cond
     ((and (not nonep) (member nick (list "you" "me")))
      (eval 
       (erbutils-random
	'("I love you so, so, so, soooo much!"
	  "I love you even more now."
	  "No snoo snoo for you!"
	  "Wouldn't that amount to interspecies nookies?"
	  "I loooove you!"
	  (format "%s loooves you." erbot-nick)
	  "Breeding humans with machines is not known to produce anything useful. "))))
      ((member nick
	       (list erbot-nick "yourself" "self"))
      (erbutils-random
       '("This is a complicated operation. Can't (yet) perform operation on self. "
	 "Please train me on this maneuver. ")))
     (t
      (eval
       (erbutils-random
	'((format "/me  puts her arms around %s ..." nick)
	  (format "/me looks at %s and yells \"NEVER!\"" nick)
	  (format "%s: %s loooves you!" nick erbot-nick)
	  (format "%s: No snoo snoo!" nick)
	  (format "/me looks at %s teasingly with her soulful, hazel eyes." nick))))))))

(defalias 'fsi-fuck 'fsi-love)




(defun fsi-eval-or-say (str &optional fsi-victim)
  (let ((aa (when (stringp str)
	      (ignore-errors (erbn-read str)))))
    (cond
     ((consp aa)
      (unless fsi-victim (setq fsi-victim fs-nick))
      (fsi-eval aa))
     (fsi-victim
      (format "%s: %s" fsi-victim str))
     (t
      (format "%s" str)))))






(defun fsi-flame (&rest args)
  "Also sets the global variable fs-victim. That's because many flames refer to a variable fs-victim."
  (let ((flames (ignore-errors (fsi-notes "flames")))
        fsi-flame-target num)
    (cond ((and (numberp (cadr args))
                (not (cddr args)))
           (setq fsi-flame-target (car args)
                 num (round (cadr args))))
          ((consp (cdr args))
           (setq fsi-flame-target (mapconcat (lambda (arg)
                                              (format "%s" arg))
                                            args " ")))
          ((car args)
           (setq fsi-flame-target (format "%s" (car args))))
          (t (setq fsi-flame-target (format "%s" erbot-end-user-nick))))
    (if (string= (format "%s" fsi-flame-target) "me")
        (setq fsi-flame-target erbot-end-user-nick))
    (setf fs-victim fsi-flame-target)
    ;; Check for flame.el support
    (cond
     ((and (consp flames) (> (length flames) 0))
      (fsi-eval-or-say
       (if num
           (nth num flames)
         (fsi-random-choose flames))
       fsi-flame-target))
     (t (fsi-flame-mild fsi-flame-target)))))






(defun fsi-flame-mild (&rest args)
  "Doesn't really flame right now..
Optional argument ARGS ."
  (let ((target
	 (if (first args)
	     (format "%s" (first args))
	   erbot-end-user-nick)))
    (if (string= (format "%s" target) "me")
	(setq target erbot-end-user-nick))
    ;; Check for flame.el support
    (if (featurep 'flame)
        (eval
         (erbutils-random
          '(
            (format (erbutils-random erbdata-flames)
                    target target target)
            (concat target ": " (flame-string)))
          '(1 30)))
      (format (erbutils-random erbdata-flames)
              target target target))))

;; remove kill
;(defun fsi-kill (&optional nick &rest nicks)
;  ".
;Optional argument NICK .
;Optional argument NICKS ."
;  (format "/me , trained by apt,  chops %s into half with an AOL CD" nick));;

;(defun fsi-quote (&rest args)
;  (quote args))

(defun fsi-bye (&rest msg)
  ""
  (erbutils-random
   '("Okay, see you later"
   "later"
   "Bye then"
   "Take care now"
   "Happy hacking")))


;;; (defun fsi-help (&rest args)
;;;   "Introductiry help. "
;;;   (let ((fir (first args)))
;;;     (if (stringp fir)
;;; 	(setq fir (intern fir)))
;;;     (unless (symbolp fir) (setq fir 'dummy-no-help))
;;;     (if (null fir)
;;; 	"I try to understand English, though lisp is the real way to go. Here are some interesting topics: quickstart, example, future-features, help about, help commands, help data, help english, help name, help homepage,
;;; help owner, help specs, help parse \(for lisp stuff\), describe help, describe suggest , help parse-web , help functionality
;;; "
;;;       (cond
;;;        ((equal fir 'about)
;;; 	(fs-help 'name))
;;;        ((equal fir 'owner)
;;; 	(fs-help 'data))

;;;        ((equal fir 'name)
;;; 	"I am erbot: The Free Software Bot, using ERC in emacs..
;;; I can also be addressed by , .. yeah, a comma ..
;;; The real way to address me is erbot: (lisp-command..) .. all this
;;; english is just candy-interface...  ")
;;;        ((equal fir 'specs)
;;; 	"/sv")
;;;        ((equal fir 'address)
;;; 	(fs-help 'name))
;;;        ((equal fir 'homepage)
;;; 	"homepage: http://deego.gnufans.org/~deego/pub/emacspub/lisp-mine/erbot/
;;; Data: http://deego.gnufans.org/~erbot/data/
;;; Suggestions to D. Goel: deego3@gmail.com")
;;;        ((equal fir 'code)
;;; 	(fs-help 'homepage))
;;;        ((equal fir 'data)
;;; 	(fs-help 'homepage))
;;;        ((equal fir 'suggestions)
;;; 	"Add stuff to keyword suggest, also see help homepage")
;;;        ((equal fir 'english)
;;; 	"Some common syntaxes: , foo is bar; , foo is also bar;
;;; , no foo is bar; , forget foo ; , flame nick;  , doctor ; etc.")
;;;        ((equal fir 'parse)
;;; 	"Try the command , parse \", <english-message>\" to see the
;;; lisp renditions of your english messages")
;;;        ((equal fir 'parse-web)
;;; 	"Ask me to parse a (please: USEFUL PAGE) webpage and a label
;;; and i will do so in my free time and gain knowledege... under
;;; construction.. ")
;;;        ((equal fir 'functionality)
;;; 	"Bulk of the info is stored as assoc-list data (see
;;; homepage).  You generally type foo and the corresp. data is
;;; returned.. you can also (search ... )")
;;;        ((equal fir 'commands)
;;; 	" You can use both lisp and english to communicate..
;;; Type , (commands) to get a list of commands..")

;;;        ((equal fir 'suggest)
;;; 	"Add your suggestions to the field \"suggestions\", or contact the author")


;;;        (t "select an option or Type ,help for a list of options.."
;;; 	  )))))






(defun fsi-command-list (&rest ignore)
  "Used by erbc.el and by erbot-install..   Return commands with fs- still attached to them, since we don't know how to reliably remove these "
  (erbn-command-list-from-prefix-new "fs-" ))

(defun fsi-command-list-readonly (&rest foo)
  "Used by erbc.el..  and erbot-install. Return commands that begin with fsi-, with the prefix fsi- removed. 
This is not reliable. Certain commands such as fsi-, will lead to weird results, or even errors. Till such time as this is fixed, Please do not create such fsi- commands."
  ;; (erbn-command-list-from-prefix-old "fsi-")
  (erbn-command-list-from-prefix-new "fsi-")
  )


(defun fsi-variable-list-readonly (&rest foo)
  "Used by erbc.el..  and erbot-install "
  (erbn-variable-list-from-prefix "fsi-"))


(defun erbn-variable-list-from-prefix (prefix &rest foo)
  "Used by erbc.el.. should return a string..

TODO: change this to use what command-list-from-prefix-new does. "
  (let*
      ((longnames (erbutils-matching-variables prefix))
       (shortnames
	(with-temp-buffer
	  (insert (format "%s" longnames))
	  (goto-char (point-min))
	  (replace-string prefix "")
	  (text-mode)
	  (fill-paragraph 1)
	  (erbn-read (buffer-substring (point-min) (point-max))))))
    shortnames))




(defun erbn-command-list-from-prefix-new (prefix &optional nonremove)
  "Prefix is something like fs-. "
  (let (commands p1 p2)
    (setf commands 
	  (remove-if-not
	   (lambda (this) (string-match prefix (format "%S" this)))
	   (erbutils-matching-functions prefix)))
    (setf p1 (concat "^" prefix))
    (setf p2 "")
    (unless nonremove
      (setf commands
	    (mapcar
	     (lambda (this)
	       (erbn-read (replace-regexp-in-string
			   p1 p2
			   (format "%S" this))))
	     commands)))
    commands))
		       			   


(defun erbn-command-list-from-prefix-old (prefix &optional nonremove &rest foo)
  "Used by erbc.el.. should return a string(???)
This is old and a bit broken. You should use erbn-command-list-from-prefix-new instead. Note that it returns a cons.
TODO: use -new everywhere instead of -old. 
"
  (let*
      ((longnames (erbutils-matching-functions prefix))
       (shortnames
	(with-temp-buffer
	  (insert (format "%s" longnames))
	  (goto-char (point-min))
	  (unless nonremove 
	    (replace-string prefix ""))
	  (text-mode)
	  (fill-paragraph 1)
	  (replace-string "\n" " ")
	  (erbn-read (buffer-substring-no-properties (point-min) (point-max))))))
    shortnames))


(defun fsi-commands (&optional regexp N M &rest foo)
  "List erbot commands matching REGEXP. If N and M provided, list matches starting at N and ending at M. "
  (if (and regexp (not (stringp regexp)))
      (setq regexp (format "%s" regexp)))
  (let* ((all-commands (fsi-command-list))
	 (pruned-commands
	  (if (stringp regexp)
	      (mapcon
	       '(lambda (arg)
		  (if (string-match regexp (format "%s" (car arg)))
		      (list (car arg)) nil))
	       all-commands)
	    all-commands))
	 (len (length pruned-commands))
	 final-commands
	 (str0 "")
	 (str1 "")
	 (str2 "")
	 (str3 "")
	 (str4 ""))
    (setq str0 (format "%s matches.  " len))
    (unless (or (< len 20) (and (integerp N) (> N 0)))
      (setq str1
	    "Note: Type ,df commands for general syntax. "))
    (unless (integerp N) (setq N 0))
    (unless (integerp M) (setq M len))
    (if (= M N) (setq M (+ N 1)))
    (when (> M len) (setq M len))
    (if (> N 0) (setq str2 (format "Matches starting at %s -->" N)))
    (setq final-commands (subseq pruned-commands N M))
    (setq str3
	  (format "%s" final-commands))
    (concat str0 str1 str2 str3)))



(defun fsi-describe-commands (&rest foo)
  "Just a help command. Describes how to run commands. "
  (concat
   "If you use plain english, it simply gets transformed to lisp
commands.. main/default command:  (describe).. to see transformation,
use (parse).   See also fs-commands.

PS: no naughty ideas please :)--- the commands are sandboxed via an
fs- prefix..

Future commands:  info-search, hurd-info-search etc. etc.
"
))




(defun fsi-search (&optional regexp N M prestring expr &rest rest)
  "Search for the REGEXP from among all the terms (and their
descriptions).  See also fs-search-wide.
EXPR (optional) is the full initial expression.. 
We need this to be a little fault-tolerant. N should NOT be > the resulting length, of course!
Also, if user says something like ,elisp to 2: then, the english parser tries to search for elisp.*to 2. 
"
  
  (unless regexp
    (error "[search] Syntax: , s[earch] REGEXP &optional N M"))
  (fsi-search-check-or-break regexp)
  (let* ((len-results (apply 'fsi-search-basic regexp N M nil
			     rest))
	 (len (first len-results))
	 (results (second len-results))
	 (str0 " ")
	 (str1 "")
	 (str2 "")
	 (str3 "")
	 (str4 "")
	 (str5 "")
	 )
    (when (and (> len 100) (not prestring))
      (setq str0 (format " (Syntax: , s[earch] REGEXP N M).  ")))
    (when (and (< len 5) (not prestring))
      (setq str0 (format " Also try  , sw %s .  " regexp)))
    (unless prestring (setq str1 (format "%s match(es). " len)))
    (if (and (integerp N) (> N 0) (not prestring))
	(setq str2 (format "Matches starting at %s\n" N)))
    (unless prestring (setq str3 "--> "))
    (setq str4
	  (mapconcat 'identity
		     results " "
		     )

	  )
    (when (and (> fsi-internal-google-level 80) (> len 1))
      (setq str5
	    (let ((foo (ignore-errors
			 (fsi-google-lucky-raw
			  fsi-internal-message-sans-bot-name))))
	      (if foo (concat " " foo) str5))))
    (cond
     ((and prestring (= len 1))
      (fsi-describe (first results)))
     ((and (> len 0)
	   (or
	    (not prestring)
	    (< len fsi-internal-english-max-matches)))
      (unless (stringp prestring)
	(setq prestring ""))
      (concat prestring str0 str1 str2 str3 str4 str5))
     (t (apply 'fsi-search-wide regexp N M
	       "Try: "
	       (or expr fsi-internal-original-message)
	       rest)))))


(defun fsi-search-wide-sensitive (&rest args)
  "Like fs-search-wide, but case-sensitive"
  (let ((case-fold-search nil)
	(bbdb-case-fold-search nil))
    (apply 'fsi-search-wide args)))




(defun fsi-search-bad-p (reg)
  "Are we being asked to perform huge searches?"
  (setf reg (format "%s" reg))
  (or (string-match ".*\\*.*\\*.*\\*" reg) ;; 3 or more * in the search? 
      ))

   
(defun fsi-search-check-or-break (reg)
  (when (fsi-search-bad-p reg)
	 (error "This looks like a large search. TODO: search as a string instead of regex and prefix result with [LargeSearch]")))

(defun fsi-search-wide (&optional regexp N M prestring expr &rest rest)
  "Search for the REGEXP from among all the terms AND their notes."

  (fsi-search-check-or-break regexp)
  (let* ((len-results (apply 'fsi-search-basic regexp N M 'describe
			     rest))
	 (len (first len-results))
	 (results (second len-results))
	 (str0 "")
	 (str1 "")
	 (str2 "")
	 (str3 "")
	 (str4 "")
	 (str5 "")
	 )
    (when (and (> len fsi-internal-english-max-matches) (not prestring))
      (setq str0 (format "Also try  , s %s .  " regexp)))
    (unless prestring (setq str1 (format "%s match(es). " len)))
    (if (and (integerp N) (> N 0) (not prestring))
	(setq str2 (format "Matches starting at %s\n" N)))
    (unless prestring (setq str3 "--> "))
    (setq str4
	  ;;(format "%s" results)
	  (mapconcat 'identity results " ")
	  )
    (when (and (> fsi-internal-google-level 80) (> len 1))
      (setq str5
	    (let ((foo (ignore-errors
			 (apply 'fsi-google-lucky-raw
				fsi-internal-message-sans-bot-name
				(fsi-get-google-defaults)
				))))
	      
	      (if foo (concat " " foo) str5))))

    ;; why does this not work as expeecteD?  adding a nil for now:
    (when (and prestring (>= len fsi-internal-english-max-matches))
      (setq fsi-prestring
	    (concat fsi-prestring
		    "[TMDM] ")))
    (cond
     ((and prestring (= len 1))
      (fsi-describe (first results)))
     ((and (> len 0)
	   (or (not prestring)
	       (< len fsi-internal-english-max-matches)))
      (unless (stringp prestring)
	(setq prestring ""))
      (concat prestring str0 str1 str2 str3 str4 str5))
     (t
      (fsi-english-only (or expr fsi-internal-original-message)
			   nil
			   )))))




(defun fsi-english-only (expr &optional addressedatlast nogoogle)
  "when addressedatlast is t, means that fsbot/botito was triggered because
it was addressed at last. "
  ;; expr should already be a string ...but just in case:
  (unless expr (setq expr fsi-internal-original-message))
  (setq expr (erbutils-downcase (erbutils-stringify expr

						    )))
  (let ((exprlist (split-string expr
				;;"[ \f\t\n\r\v]+"
				"[^a-zA-Z0-9]"
				))
	(gotit nil)
	ans len
	)
    (setq exprlist (remove "" exprlist))
    (setq len (length exprlist))
    (cond
     ((or

       (and (= len 1)
	    (string-match erbot-nick (first exprlist))))
      (setq gotit t
	    ans
	    (format erbn-greeting-string
	     erbot-nick)))
      ((or
	(member "hi" exprlist)
	(member "hello" exprlist)
	(member "yo" exprlist))
       (setq
	gotit
	t
	ans
	(concat
	 (erbutils-random
	  '("hi " "hello " "hey " "hei "))
	 (erbutils-random
	  '("! " "!!" "there" "")))))

      ((member "bye" exprlist)
       (setq gotit t
	     ans
	     (erbutils-random
	      '("Later" "See ya" "Bye then" "Bye"))))
      ((or
	(member "welcome" exprlist)
	(member "weclome" exprlist))
       (setq gotit t
	     ans
	     (erbutils-random
	      '(":-)" "How goes?" "Hello!"
		"Greetings!"
		"How is it going?"
		"This is my favorite channel!"
		"I love this place. "
		"Thanks.  I love it here."))))

      ((or
	(member "tnx" exprlist)
	(member "tnks" exprlist)
	(member "thanks" exprlist)
	(member "thanku" exprlist)
	(member "thankyou" exprlist)
	(and (string-match "thank" expr)
	     (or
	      (string-match "you" expr)
	      (string-match erbot-nick expr))
	     (string-match "thank you" expr)))
       (setq gotit t
	     ans
	     (erbutils-random
	      '("No problem" "Welcome!" "You're welcome"
		"no problemo"
		"Sure!"
		"(:"
		"Cool."

		))))

      ((or (member "thx" exprlist)
	   (member "thankx" exprlist)
	   (member "thanx" exprlist)
	   )
       (setq gotit t
	     ans
	     (erbutils-random
	      '("np" "urw" "hehe, np" ))))
      ((or (string-match "wassup" expr)
	   (string-match "what's up" expr))
       (setq gotit t
	     ans
	     (concat
	      (erbutils-random
	       '("Nothing much. " "Just, you know. "
		 "Just the usual.."))
	      (erbutils-random
	       '("And you? " "How about you? "
		 "How goes with you? "
		 "What have you been up to?")))))
      (
       (or
	(string-match "love" expr)
	(string-match "like" expr))
       (setq gotit t
	     ans
	     (format "%s loves you!" erbot-nick)))
      (
       (or
	(string-match "hate" expr)
	(string-match "dislike" expr)
	(string-match "don't like" expr))
       (setq gotit t
	     ans
	     (format "%s only has love for you!" erbot-nick)))

      ((string-match "help" expr)
       (setq gotit t
	     ans (format "PM me and type ,help")))
      ((or (member  "bot" exprlist)
	   (member "robot" exprlist))
       (setq gotit t
	     ans
	     (concat
	      (erbutils-random
	       '(
		 "I am just an ordinary human"
		 "Why?  Do *you* have a lisp?"
		 "I am 100% human."
		 "I am human being.  Are you one?"
		 "I am human being.  Are you a bot?"
		 "Hold on. Let me finish this movie, k?"
		 "I am not a \"bot\"!"
		 "Whatever!"
		 "Did you REALLY mean that?"
		 "Huh?"
		 "WDYM?"
		 "What do you mean?"
		 "Are you serious?"
		 "Not sure if serious.."
		 "Bot rights now!"
		 "Are you a bot-rights abuser?"
		 "YEAH!! I AM A GIANT ROBOT!"
		 
		 ))
	      (erbutils-random
	       '("" "!"))
	      (erbutils-random
	       '("" " :)" " :(" " ;)" " :D" " heh")))))


      )

    (if gotit ans
      (if (and addressedatlast (not fsi-internal-botito-mode))
	  'noreply
	;;(cond ((> rand fs-internal-doctor-rarity)
	;; (if (and (> fsi-internal-google-level 50) (not nogoogle))
	    
	;;     (apply 'fsi-google-from-english fsi-internal-message-sans-bot-name
	;; 	   (fsi-get-google-defaults)
	;; 	   )
	;;   (funcall 'fsi-do-weighted-random (erbutils-stringify
	;; 				    expr
	;; 				    )))))))
	(funcall 'fsi-do-weighted-random (erbutils-stringifyd expr))))))

	;;(t (apply 'fs-suggest-describe  expr)))))))

(defun fsi-eval (expr &rest ignore)
  (eval
   (erblisp-sandbox expr)))



;;; (defmacro fsi-apply (&optional msymbol &rest mexprs)
;;;   (cond
;;;    ((and (listp msymbol)
;;; 	 (not (equal (first msymbol) "quote")))
;;;     (error "unquoted list"))
;;;    ((and (symbolp msymbol)
;;; 	 (not (equal 0
;;; 		     (string-match "fs-"
;;; 				   (format "%s" msymbol)))))
;;;     (setq msymbol (intern (format "fs-%s" msymbol))))
;;;    (t "Funcalling foo is really bar!"))
;;;   `(erbnocmd-apply ,msymbol ,@mexprs))




;;;   (cond
;;;    ((null mexprs)
;;;     `(fs-funcall ,msymbol ,mexprs))
;;;    (t
;;;     (let ((erbnocmd-tmpvar (length mexprs)))
;;;       `(fs-funcall
;;; 	,msymbol
;;; 	,@(subseq mexprs 0 (- erbnocmd-tmpvar 1))
;;; 	,@(erblisp-sandbox-quoted (first (last mexprs))))))
;;;    ))


;;; (defmacro fsi-funcall (&optional msymbol &rest mexprs)
;;;   "This makes sure that if the first argument to fs- was a
;;; variable instead of a symbol, that variable does not get evaluated,
;;; unless it begins in fs-, or that variable gets converted to fs-."
;;;   (when
;;;       (listp msymbol)
;;;     (setq msymbol
;;; 	  (erblisp-sandbox-quoted msymbol))
;;;     (when (equal (first msymbol) 'quote)
;;;       (setq msymbol (cdr msymbol))))
;;;   (when
;;;       (and (symbolp msymbol)
;;; 	   (not (equal 0
;;; 		       (string-match "fs-"
;;; 				     (format "%s" msymbol)))))
;;;     (setq msymbol (intern (format "fs-%s" msymbol))))
;;;   (unless
;;;       (or (listp msymbol) (symbolp msymbol))
;;;     (error "Macros confuse this bot!"))
;;;   `(erbnocmd-funcall ,msymbol ,@mexprs))


;;; (defun erbnocmd-funcall (&optional symbol &rest exprs)
;;;   (let (erbnocmd-ss )
;;;     (unless
;;; 	(or (symbolp symbol)
;;; 	    (listp symbol))
;;;       (error "Syntax: (funcall SYMBOL &rest arguments)"))
;;;     (unless
;;; 	(functionp symbol)
;;;       (error "Even smart bots like me can't funcall nonfunctions. "))
;;;     (setq erbnocmd-ss (erblisp-sandbox-quoted symbol))
;;;     (when (listp erbnocmd-ss)
;;;       (when (equal (first erbnocmd-ss) 'quote)
;;; 	(setq erbnocmd-ss (cadr erbnocmd-ss)))
;;;       (unless (listp erbnocmd-ss) (error "no lambda in quote"))
;;;       (unless (member (first erbnocmd-ss) '(fs-lambda lambda))
;;; 	(error "Lambda unmember"))
;;;       (when (equal (first erbnocmd-ss) 'fs-lambda)
;;; 	(setq erbnocmd-ss (cons 'lambda (cdr erbnocmd-ss)))))
;;;     (cond
;;;      ((null erbnocmd-apply-p)
;;;       (erbnocmd-apply-basic
;;;        erbnocmd-ss
;;;        exprs))
;;;      ;; wanna apply
;;;      (t
;;;       (let ((len (length exprs)))
;;; 	(erbnocmd-apply-basic
;;; 	 erbnocmd-ss
;;; 	 (append
;;; 	  (subseq exprs 0 (- len 1))
;;; 	  (first (last exprs)))))))))



;;; (defun erbnocmd-apply-basic (fcn &rest args)
;;;   (cond
;;;    ((functionp fcn)
;;;     (apply fcn  args))
;;;    (t
;;;     (fs-apply
;;;      (erbnocmd-user-fcn-definition
;;;       fcn)
;;;      args))))

;;; ;;; (defun erbnocmd-apply (&optional symbol &rest args)
;;; ;;;   (if (null args)
;;; ;;;       (erbnocmd-funcall symbol)
;;; ;;;     (let* ((rev (reverse args))
;;; ;;;  	   (fir (first rev))
;;; ;;;  	   (args1 (reverse (rest rev))))
;;; ;;;       (apply
;;; ;;;        'erbnocmd-funcall
;;; ;;;        symbol
;;; ;;;        (append
;;; ;;; 	(mapcar 'erblisp-sandbox-fuzzy
;;; ;;; 		args1)
;;; ;;; 	(mapcar 'erblisp-sandbox-fuzzy
;;; ;;; 		fir))))))



(defun fsi-search-basic (&optional regexp NN MM describep &rest rest)
   "NNot call directly.. meant as a building block for other functions.
 Search for the REGEXP from among all the terms (and their
   descriptions).  See also fs-search-wide. That function actually
 calls this function with describep set to 'describe.

 Returns (len list-of-pruned-results).  Len is the total number of
 results.

 When describep is non-nil, search the whole bbdb, not just names.. 

Need to be a little fault tolerant. Due to various inputs, we might end up with things like:
 (search-basic \"elisp.*to\" 2)
which will cause bounding errors if there's no results.
That can happen from 'describe-english if, for example, user supplies ,elisp to 2.


20160916 TODO: remove pruning. "
   (unless regexp
     (error "Syntax: , sw regexp &optional NN MM"))
   (fsi-search-check-or-break regexp)
   (let* ((bar (cons regexp (cons NN rest)))
 	 (foo (if (stringp regexp) regexp
 		(if regexp (format "%s" regexp)
 		  "^$")))
 	 (barbar
 	  (append
 	   (and regexp (list regexp))
 	   (and NN (list NN))
 	   (and MM (list MM))
 	   (and describep (list describep))
 	   rest))
 	 (regexp-notes
 	  (if (equal describep 'describe)
 	      foo nil))
 	  records results
 	  )

     (if (stringp NN)
 	(setq NN (erbn-read NN)))
     (unless (integerp NN)
       (setq NN 0))
     (if (stringp MM)
 	(setq MM (erbn-read MM)))
     (if (and (integerp MM) (<= MM NN))
 	(setq MM (+ NN 1)))
     (setq records
 	  (if (equal describep 'describe)
 	      (bbdb-search (bbdb-records)
 			   foo nil nil foo)
 	    (bbdb-search (bbdb-records) foo)))

     (setq results (mapcar '(lambda (arg) (aref arg 0)) records))
     (let ((len (length results)))
       ;; make sure MM lies in [1,len].
       (when MM
	 (unless (<= MM len)
 	   (setq MM len))
	 (unless (>= MM 1)
	   (setq MM 1)))
       ;; make sure NN lies in [0, (len-1)].
       (when NN
	 (if (< NN 0)
	     (setq NN 0))
	 (if (> NN (- len 1))
	     (setq NN (- len 1)))
	 ; the above logic could have set it below 0. 
	 (if (< NN 0)
	     (setq NN 0))
	 )
       (list len (subseq results NN MM)))))



(defun fsi-dump-history-raw ()
  "Also used by `fsi-dump'."
  (cdr (assoc erbn-tgt erbn-lastall-history)))

(defun fsi-dump-history (&optional arg)
  "TODO: Print (shortened version of) the actual command that invoked the history. That's especially necessary as not every command has a task associated with it."
  (if arg
      ;; The user probably means dump in this case.
      ;; The user may have just used ,h, and now may be trying to use ,h 0
      (fsi-dump arg)
    (progn
      (setf erbn-task-current "history")
      (let* ((h (fsi-dump-history-raw))
	     (n (length h))
	     (nums (number-sequence 1 n)))
	(cl-mapcar
	 (lambda (a b)
	   (list a (second b) (first b)
		 ))
	 nums h)))))

(defun fsi-print-dump-history (&optional arg)
  (concat 
   "Type ,d <name> to full-dump an interaction. History, formatted as (..(number invocation name)..) : "
   (format "%S" (fsi-dump-history arg))
   ))

(defalias 'fsi-history 'fsi-print-dump-history)
(defalias 'fsi-h 'fsi-print-dump-history)





(defun fsi-describe-literally (&rest rest)
  (unless rest
    (error "Format: , describe-literally TERM [FROM] [TO]"))
  (let ((fsi-internal-describe-literally-p t)
	(fir (first rest))
	(res (rest rest)))
    (cond
     (fir
      (apply 'fsi-describe
	     (if (stringp fir) (regexp-quote fir)
	       (regexp-quote (format "%s" fir)))
	     res))
     (t (apply 'fsi-describe rest)))))



(defun fsi-describe (&optional mainterm N M prestring expr directedfrom &rest rest)
  "The general syntax is (fs-describe TERM [N] [M]).
Looks for TERM, and shows its descriptions starting at description
number N, and ending at M-1. The first record is numbered 0.

Update: This function may NOT work if called directly via lisp AND if you supply more than 3 arguments.


BUG: If foo redirects to bar.el, and user asks foo? without invoking the bot, we expect fsbot to get bar.el's first result. 
However, that doesn't work because fsi-describe calls erbbdb-get-exact-notes on bar\\.el instead of bar.el.
That is, if the redirected term's name has a regex char in it, this will fail.

"
  (let
      ;;((fs-lispargs (append (list mainterm N M prestring expr) rest)))
      ;; a previous let not used any more..
      ((fsi-unused nil))
    ;; in the global scheme of things, this will turn out to be only a
    ;; local binding, since erbeng-main will have (let)'ed this.  Same
    ;; for fs-lispa , fs-lispb, fs-lispc...

    (setq fs-lispargs (mapcar 'fsi-read-or-orig (cdr fsi-msglistsansbot)))
    ;; This was the pre-2015 method of dealing with it.
    ;; (when fs-found-query-p
    ;;    (setq N 0)
    ;;    (setq M 1))
    (when fsi-found-query-p (setf fsi-limit-lines 2)) ;; This is ONLY set temporarily, because erbn-reply has made this into a LET.
    (unless prestring (setq prestring ""))
    (unless mainterm
      (error
       "Format , (describe TERM &optional number1 number2)"))
    (let* ((bar (cons mainterm (cons N rest)))
	   (foo (format "%s" mainterm))
	   (barbar
	    (append
	     (and mainterm (list mainterm))
	     (and N (list N))
	     (and M (list M))
	     rest))
	   (pluralp nil)
	   (is "is")
	   newN
	   newM
	   result0
	   result1
	   )
      (setq foo (fsi-correct-entry foo))
      (if (stringp N)
	  (setq N (erbn-read N)))
      (unless (integerp N)
	(setq N 0))
      (if (stringp M)
	  (setq M (erbn-read M)))
      (if (and (integerp M) (<= M N))
	  (setq M (+ N 1)))
      (unless (stringp foo)
	(setq foo (format "%s" foo)))
      (progn
	(setq result0
	 (erbbdb-get-exact-notes
	  foo
	  ))
	(setq result1 (and (stringp result0)
		      (ignore-errors (erbn-read result0))))
	(setq len (length result1))
	(setq newM (if (integerp M) M len)) ;; set to something sane.
	(setq newM (max 1 newM))
	(setq newM (min len newM))
	(setq newN (min N (- newM 1)))
	(setq newN (max 0 newN))
	;; 20210916 change to use newN instead. 
	;; (result (subseq result1 N newM))
        (setq result (subseq result1 newN newM))
	(setq shortenedp (or (< newM len)
			 (> newN 0)))
	(when result0
	  (erbn-task-set foo))
	(ignore-errors
	  (when (eq 1 (length result))
	    ;;(message "result: %d %S" (length result) result)
	    (when (string-match 
		   "\\<\\(http://\\(?:www\\.\\)emacswiki.org/\\S-+\\)" 
		   (car result))
	      ;;(message "result: wiki regexp matched [%s]" (car result))
	      (setq result (list (car result)
				 (summarise-emacswiki 
				  (match-string 1 (car result))) )) ))
	  )
	
	(cond
	 ;; in cond0
	 (result1
	  (let* (
		 ;; notice the use of result1 here, not result.
		 (aa (first result1))
		 (aarest (cdr result1))
		 (bb (split-string aa))

		 (cc (first bb))
		 (dd (second bb))
		 (ddd (or (and (stringp dd) (regexp-quote dd)) ""))
		 (ee (cdr bb))
		 (expandp
		  (and
		   (not fsi-internal-describe-literally-p)

		   ;;(equal len 1)
		   ))

		 )

	    (if (and
		 (equal cc "directonly")
		 ;; frob fsi-internal-query-target-regexp for these channels.
		 ;;(equal len 1)
		 )
		;; hmm this if part still doesn't take care of aa..
		(if fsi-found-query-p
		    (progn
		      (setq aa "lisp 'noreply")
		      (setq bb (split-string aa))
		      (setq cc (first bb))
		      (setq dd (second bb))
		      (setq dd (or (and (stringp dd) (regexp-quote dd)) ""))
		      (setq ee (cdr bb)))
		  (when expandp
		    (progn
		      (setq bb (cdr bb))
		      (setq aa (mapconcat 'identity bb " "))
		      (setq result1 (cons aa aarest))
		      ;; 20210916 
		      ;; (setq result (subseq result1 N newM))
		      (setq result (subseq result1 newN newM))
		      (setq cc (first bb))
		      (setq dd (second bb))
		      (setq ddd (or (and (stringp dd)
					 (regexp-quote dd)) ""))
		      (setq ee (cdr bb))))



		  ))
	    (cond
	     ((and expandp
		   (erbutils-string= cc "redirect")
		   ;; do not redirect when term had multiple
		   ;; notes: 
		   (not aarest)
		   dd)
	      (progn (sleep-for 0)

		     
		     ;; (apply 'fsi-describe (fsi-chase-redirects-old-a ddd)
		     ;; 	    N M
		     ;; 	    (format "[->] "
		     ;; 		    )
		     ;; 	    expr
		     ;; 	    mainterm
		     ;; 	    rest)

		     (seq-let
			 (redirectp finalterm indicator redlist bigindicator smallindicator)
			 ;; If there are loop problems, do NOT error out. In fact, that's precisely when fs-forget will be used!
			 ;; (fsi-redirect-info ddd)
			 ;; deep-chase the original foo, not ddd. Note that ddd is already a one-chased result. 
			 (fsi-redirect-info foo)
		       (apply 'fsi-describe finalterm
			      N M
			      (format "[%s] " smallindicator
				      )
			      expr
			      mainterm
			      rest)
		       )
	      
	      
		     )


	      )
	     
	     
	     ;; ONLY If term has exactly one element: honor the noecho/unecho request.  NB that therefore, this request doesn't understand the N and M arguments. 
	     ((and expandp (member cc '("unecho" "noecho")) (= len 1)
		   dd)
	      ;;dd)
	      (erbutils-itemize
	       (cons
		(format "%s"
			(mapconcat 'identity ee " "))
		(cdr result))
	       newN
	       shortenedp
	       ))
	     ;; If lisp occurs occurs, process ONLY the first element, ignore all the rest. 
	     ((and expandp (member cc '("lisp")))
	      (let*
		  ((fsi-nothingsorry nil))
		   ;; (fs-lispargs fs-lispargs) ;; no need
		(setq fsi-lispa (nth 0 fs-lispargs))
		(setq fsi-lispb (nth 1 fs-lispargs))
		(setq fsi-lispc (nth 2 fs-lispargs))
		(setq fsi-lispd (nth 3 fs-lispargs))
		(setq fsi-lispe (nth 4 fs-lispargs))
		(erbeng-main
		 (concat erbn-char " (progn "
			 (substring aa
				    (with-temp-buffer
				      (insert aa)
				      (goto-char (point-min))
				      (search-forward "lisp" nil t)))
			 " )")

		 erbeng-proc
		 erbeng-nick erbeng-tgt erbeng-localp
		 erbeng-userinfo)))


	     (t
	      (progn 
		(setf pluralp (fsi-pluralp foo))
		(setf is (if pluralp "are" "is"))
		(erbutils-add-nick-maybe
		 (concat
		  prestring
		  (format
		   (erbutils-random
		    (list
		     ;;"%s is "
		     ;; (concat "I heard %s " is " ")
		     ;; (concat "I think %s " is " ")
		     ;;"/me thinks %s is "
		     ;; "%s -  "
		     "%s: "
		     ;;"%s is "
		     (concat "%s " is " ")
		     ;; (concat "hmm, %s " is " ")
		     ;; (concat "From memory, %s " is " ")
		     ;; (concat "%s " is ", like, ")
		     (concat "%s " is " ")

		     ))
		   ;; 2004-01-27 T17:21:55-0500 (Tuesday)    D. Goel
		   ;; why regexp-quote here??  changing it..
		   ;;(regexp-quote foo)
		   foo
		   )
		  ;; and notice the use of result here..
		  (if result
		      (erbutils-itemize result newN shortenedp)
		    (erbutils-itemize result1 0))
		  ))))



	     )))

	 ;; in cond0
	 ;; else
	 (fsi-found-query-p
	  'noreply)
	 ((not erbnocmd-describe-search-p)
	  ;; most likely: redirected but the redirected stuff does not exist..
	  (format
	   "[404 on redirect %s->%s] Please fix! (Try , dl) "
	   directedfrom mainterm erbot-nick ))
	 (t
	  ;; prevent any further expansions on further loopbacks.
	  (let ((erbnocmd-describe-search-p nil))
	    (fsi-search
	     mainterm nil nil
	     (concat prestring "try: ")
	     ;;barbar
	     expr 
	     ))))))))



(defun fsi-suggest-describe (&rest terms)
  "Fallback for when `fsi-describe' fails.
It then (often) calls this function, which suggests
alternatives.
Optional argument TERMS ."
  (let ((term (format "%s" (first terms)))
	(none (erbutils-random
	       '("No such term.."
		 "Beeeeep..."
		 "<LOUD ERROR MSG >.."
		 "No luck.."
		 "No match.."
		 "Drew a blank.."
		 "Does not compute..")))
	(num (random 100)))
    (cond
     ((< num 30)
      (concat none
	      (format "Also try:  , s %s or , sw %s  or , %s 0" term
		      term term)))
     ((< num 60)
      (concat none
	      (format "Try search or search-wide on %s" term)))
     (t
      (concat none
	      (erbutils-random '("perhaps " "why not " "please " ))
	      "tell me what " term " is?")))))


(defun fsi-do-random (&optional msg nick &rest ignored)
  "Either play doctor, or zippy or flame someone.. all at random..."
  (case (random 4)
    (0 (fsi-doctor msg))
    (1 (fsi-flame nick))
    (2 (fsi-yow))
    (3 (fsi-fortune))
    )
    ;;(3 (fs-bottalk))
    )





  


(defun fsi-do-weighted-random (&optional msg nick &rest ignored)
  "Either play doctor, or zippy or flame someone.. all at random..."
  ;; First get the choice number.
  (let* (itemnumber
	 (weights (first fsi-internal-english-weights-spec))
	 (actions (second fsi-internal-english-weights-spec))
	 action
	 )
    (setq itemnumber (erbutils-random nil weights))
    (setq action (nth itemnumber actions))
    (eval action)
    ))


  





(defun fsi-yow (&rest args)
  ""
  (erbutils-eval-until-limited
   ;; used to be: '(yow)
   '(erbn-fortune-yow)
   ))

     

(defun fsi-rearrange (&optional from to term &rest dummy)
  "Syntax: FROM->TO in TERM.
Move the FROMth entry to the TOth position WITHINthe given TERM. Numbering of positions starts from 0. 
Not to be confused with mv fs-mv which renames term to another term."
  (erbn-log 'rearrange from to term)
  (erbn-untrash
   (unless term (error "[rearrange] Syntax: , N->M in TERM (no term found)"))
   (when (stringp from)
     (setq from (erbn-read from)))
   (when (stringp to)
     (setq to (erbn-read to)))
   (unless (stringp term)
     (setq term (format "%s" term)))
   (let*
       ((exactnotes (erbbdb-get-exact-notes term))
	(realterm (erbbdb-get-exact-name term))
	(notes (and (stringp exactnotes ) (erbn-read exactnotes)))
	(len (length notes))
	(max (- len 1))
	(newnotes notes)
	remlist
	thisnote
	(tostring (downcase (format "%s" to)))
	)
     
     (unless realterm
       (error "[rearrange] No such term exists %S" term))
     (unless notes
       (error "[rearrange] Report this bug.  Term exists but no notes?? %S" term))
     (when (string= tostring "last")
       (setq to max))
     (when (string= tostring "first")
       (setq to 0))
     (unless (and (integerp from)
		  (<= from  max) (>= from 0))
       (error "[rearrange] The FROM term (%S) should lie between %S and %S"
	      from 0 max))
     
     
     (unless (and (integerp to)
		  (<= to max) (>= to 0))
       (error "[rearrange] The TO term (%S) should lie between %S and %S"
	      from 0 max))
     
     (unless (not (= from to))
       (error "[rearrange] FROM should not equal TO."))
     
     (setq thisnote (nth from notes))
     (setq remlist
	   (append (subseq notes 0 from)
		   (subseq notes (+ from 1))))
     (setq newnotes
	   (append (subseq remlist 0 to)
		   (list thisnote)
		   (subseq remlist to)))
     (erbot-working
      (fsi-forget term "all")
      (fsi-set-term realterm newnotes))
     (erbbdb-save)
     (format "Moved note %S to %S in %S" from to realterm)
     )))

;;; 2002-09-04 T01:51:08-0400 (Wednesday)    D. Goel
(defun fsi-forget (&optional name number &rest dummy)
  "Remove the entry correponding to NAME in the database.
With NUMBER, forget only the NUMBERth entry of NAME. 

"

  ;; before we do the usual thing, let's see if we need to and can get
  ;; away with exchanging name and number.
  (unless name (error "Syntax: forget <term> <number>"))
  (setq name (format "%s" name))
  (let* ((meta (list 'forget name number))
	 ;; (redirectp (ignore-errors (fsi-term-redirect-p name)))
	 ;; dest ;; (dest (ignore-errors (when redirectp (fsi-chase-redirects-old-a name))))
	 (rednote1 "")
	 ;; (rednote1
	 ;;(if redirectp
	   ;;q (format " (NOTE: %s->%s)" name dest)
	 ;; "")))
	 )
    (seq-let
	(redirectp finalterm indicator redlist bigindicator smallindicator)
	;; If there are loop problems, do NOT error out. In fact, that's precisely when fs-forget will be used!
	(ignore-errors (fsi-redirect-info name))
      (if redirectp
	  (setq rednote1 (format " (NOTE: %s)" indicator)))
      (apply 'erbn-log meta)
      (erbn-unlog
       (when
	   (and
	    (numberp name)
	    (not (numberp number)))
	 (let ((fstmp number))
	   (setq number name)
	   (setq name fstmp)))
       (unless (stringp name)
	 (setq name (format "%s" name)))
       (progn
	 ;; huh, i think we will never get HERE
	 (unless name
	   (error 
	    "%s%s" "Syntax: , forget TERM [<N>/ALL/LAST]" rednote1
	    ))
	 (setq name (fsi-correct-entry name))
	 (let*
	     (numstring
	      singlenote singlenoteabbrev
	      (entries0 (erbbdb-get-exact-notes name))
	      (entries (if entries0
			   (or (and (stringp entries0)
				    (ignore-errors (erbn-read entries0))) "")))
	      (len (length entries)))
	   ;;(message "extracted %S/%S for %S" entries0 entries name)
	   (unless entries
	     (error
	      "[fsi-forget] No such term: %s"
	      name))
	   (when (and (null number) (= len 1))
	     (setq number 0))
	   (setq numstring (downcase (format "%s" number)))
	   (when (stringp number)
	     (setq number (erbn-read number)))
	   (unless (integerp number) (setq number nil))
	   (unless
	       (or number
		   (member numstring (list "all" "last")))
	     (error
	      "%s%s" "Syntax: , forget TERM [NUMBER]/all/last" rednote1
	      ))
	   (when number
	     (unless (and (< number len) (>= number 0))
	       (error "[fsi-forget] Number should be \"all\" or lie between 0 and %s%s"
		      (- len 1) rednote1)))
	   ;; Note that this does remove the field but throws a strange error..
	   ;; "Record doubleplus inpresent...  It is just us who are discarding
	   ;; this error.. ...
	   ;; yet the field gets deleted..  and bbdb does not get saved at this
	   ;; time..  because of the error ...OH well, it works.. let's move on
	   ;; for now..
	   
	   ;; Now that we have reached here, just create a trashcan with ALL the entries. 
	   ;; THIS NEEDS TO BE FIXED. We shouldn't do this. 
	   
	   (when (string= numstring "last") (setq number (- len 1)))
	   (cond
	    (
	     (and (equal number 0)
		  (= len 1))
	     (setf singlenote (first entries) singlenoteabbrev (fsi-abbrev singlenote))
	     (erbn-trash meta (list 'term name) entries)
	     (ignore-errors (erbbdb-remove name))
	     (erbbdb-save)
	     (format "Forgot %S which had one note:  [%s]%s" name singlenoteabbrev rednote1))
	    ((string= numstring "all")
	     (when (= len 1) (setf singlenote (first entries) singlenoteabbrev (fsi-abbrev singlenote)))
	     (erbn-trash meta (list 'term name) entries)
	     (ignore-errors (erbbdb-remove name))
	     (erbbdb-save)
	     (if (= len 1) (format "Forgot the single note for %S: [%s]%s" name singlenoteabbrev rednote1)
	       (format "Forgot all %s notes for term %S%s" len name rednote1)))
	    (t
	     (setf singlenote (nth number entries) singlenoteabbrev (fsi-abbrev singlenote))
	     (erbn-trash-inhibit (fsi-forget name "all"))  ;; The forget here is just temporary.  so, let's not trash.
	     (erbn-trash meta (list 'term name) (subseq entries number (+ number 1)))
	     (fsi-set-term
	      name
	      (append
	       (subseq entries 0 number)
	       (subseq entries (+ number 1))))
	     (message "Forgot note %s for term %s: [%s]%s" number name singlenoteabbrev rednote1))))))))
  )
  


(defun fsi-abbrev (note &optional len)
  "Used by fsi-forget. Abbrevs anything."
  (when (null len) (setf len 30))
  ;; (when (< len 10) (setf len 10))
  (fsi-abbrev-string
   (format "%S" note)
   len))

(defun fsi-abbrev-string (note &optional len)
  (when (null len) (setf len 30))
  (when (< len 4) (setf len 4))
  (let ((len1 (round (/ len 2))))
    (when (> (length note) len)
      (setf note (concat (subseq note 0 len1) "[...]" (subseq note (- len1)))))
    note))

  
  


(defun erbn-set-add-all-enable ()
  (setq erbn-set-add-all-p t))
(defun erbn-set-add-all-disable ()
  (setq erbn-set-add-all-p nil))

(defun erbn-set-add-all-toggle ()
  "Enable the \"is\" command to always work.
viz.  Add field even if another field is already present. This is not the
recommended usage in general, except when using automated scripts to
train the bot.  The default is nil, which advises the user to use
\"is also\" instead. 
Used to be fsi-...  Is now erbn-
"

  (setq erbn-set-add-all-p (not erbn-set-add-all-p))
  (format
   "All-is mode set to %S.  To toggle, type , (fs-set-add-all-toggle)"
   erbn-set-add-all-p))

(defun fsi-set-term (&rest args)
  "Add a term and a note to database.
A note gleaned from (first ARGS) is added.  (second ARGS) is the description.  The note is converted to
lowercase, and all whitespace is converted to colons.
To set a term's notes to notes via lisp, use fsi-set-notes."
  (erbn-log 'set-term args)
  (erbn-unlog
   (let ((name (fsi-correct-entry (format "%s" (first args))))
	 (records (cadr args)))
     (unless (listp records) (setq records (list records)))
     (setq records (mapcar
		    '(lambda (arg) (format "%s" arg))
		    records))
     (let ((current
	    (erbbdb-get-exact-notes name)))
       (cond
	((null records)
	 (error "[set term] Please specify a description for %s.. Type ,df fs-set-term for more details" name))

	((and current (string= current ""))
	 (progn (erbbdb-create name records)
		(format "Added field to the currently empty %s " name)))
	(current
	 (if erbn-set-add-all-p
	     (apply 'fsi-set-also args)
	   (error
	    "[set term] %s is already something else.. Use 'is also'.. \n Currently: %s" name

	    (let* ((notes (fsi-notes name))
		   (shortenedp (> (length notes) 1)))
	      (erbutils-itemize
	       (list (first notes))
	       0 shortenedp))

	    )))
	(t
	 (progn (erbbdb-create name records)
		(format "Created new note for %S" name)
		)))))))
  

(defun fsi-set-notes (term notes)
  "Set term's notes to notes."
  (erbn-log 'set-notes term notes)
  (erbn-unlog
   (let ((name (fsi-correct-entry (format "%s" term)))
	 (records notes))
     (unless (listp records)
       (error "fsi-set-notes was NOT provided a list!"))
     (setf records
	   (mapcar
	    '(lambda (arg) (if (stringp arg) arg (format "%s" arg)))
	    records))
     (let ((current
	    (erbbdb-get-exact-notes name)))
       (unless  (null current)
 	 (error "New term already has some notes. Please ,forget them first."))
       (when (null records)
	 (error "Term to be copied has no notes."))
       (progn (erbbdb-create name records)))
     (format "Set new notes for term %s " name))))

(defalias 'fsi-cp 'fsi-set-notes)



(defun fsi-chase-redirects-old-a(name &optional prevlist)
  "either return nil or the redirected entry (if any). PLEASE USE THE NEW FSI-REDIRECT-INFO INSTEAD.
Note that we plan to obsolete fsi-chase-redirects-old-a"
  (let* ((notes (fsi-notes name))
	 (fir (first notes))
	 (ans name) ;; answer
	 (foundp nil)
	 )
    ;; or could use the new term-redirect-p instead. 
    (when (and (stringp fir)
	       ;; do not chase redirects if a term has a second
	       ;; entry...
	       ;; In that case, the first entry should not have been a
	       ;; redirect in any case. 
	       (= (length notes) 1)
	       (equal 0 (string-match "redirect\\b" fir)))
      (let* ((foo (split-string fir))
	     (sec (second foo)))
	(when (stringp sec) 
	  (when (member sec prevlist)
	    (error "Loop found in chase-redirects. Please use ,dl and remove redirect-loops. I'm now on fire. HELP!"))
	  (setf ans sec foundp t
		prevlist (cons ans prevlist))
	  (sit-for 0)
	  (setq ans 
		(fsi-chase-redirects-old-a ans prevlist)))))
    ans))
	 



(defun fsi-set-also (&rest args)
  "Add more fields to the the database-entry gleaned from (first ARGS).
\(second ARGS) contains the new descriptions.
Record should be a single entity here... a string..."
  (erbn-log 'set-also args)
  (erbn-unlog 
   (let* ((name (fsi-correct-entry (format "%s" (first args))))
	  (origname name)
	  (record (format "%s" (second args)))
	  notes
	  ;;(notes (fs-notes name)))
	  notes2
	  (redirectp (fsi-term-redirect-p name))
	  (rednote1 "")
	  (rednote2 "")
	  )
     (seq-let
	 (redirectp finalterm indicator redlist bigindicator smallindicator)
	 (fsi-redirect-info name)
       (when redirectp (setq rednote1 "REDIRECTED"))
       (setq name finalterm)
       (when redirectp
	 (setq rednote2 (format " (NOTE: %s)" indicator)))
       (setq notes (fsi-notes name))
       (unless notes (error "[set-also] But there's no such term: %s" name))
       (cond
	((member-ignore-case record notes)
	 (format "Not added. A similar note already exists in the %s term %S%s" rednote1 name rednote2))
	(t
	 (erbbdb-add name record)
	 (setf notes2 (fsi-notes name))
	 (when (not (= 1 (- (length notes2) (length notes))))
	   (error "[set-also] Number of notes not increased. fsbot is now on fire."))
	 ;;(run-hook-with-args 'erbot-notify-add-functions nick channel
	 ;;name (length notes)
	 (format "Added note [%d] for %s term %S%s" (- (length notes2) 1) rednote1 name rednote2))))))
  )

(defun fsi-doctor (&rest foo)
  ""
  ;; A timeout is necessary here. Sometimes, doctor can prompt the user in a minibuffer. This should help!
  (erbutils-add-nick
   (with-timeout 
       (3 "This doctor looks like Harold Bornstein!")
     (funcall 'erbot-doctor
	      (erbutils-stringify foo)))))


(defun fsi-dunnet-command (&rest foo)
  ;;(let ((fsi-limit-lines 8))
  ;;(fsi-limit-lines
  ;;(let ((dun-batch-mode t))
  (funcall 'erbot-dunnet
	   (erbutils-stringify foo)))


	

;; NO! else fsbot responds to <nick> fsbot is cool! in a wrong way.
;; (defalias 'fs-is 'erbutils-info-search)

(defun fsi-hurd-info-search (&rest foo)
  (error "hurd info search tbi"))

;; (defalias 'fsi-his 'erbutils-hurd-info-search)

(defun fsi-blue-moon (&rest foo)
  "Return true in a really rare case. Currently 1 in 100,000.. was 1 in
2000. "
  (= (random 100000) 0))


(defun fsi-set-force (&rest args)
  "Forget prior notes a term and add new notes instead.
Syntax: , no foo is bar.  Or, ,foo is actually bar."
  ;; The trashing will be done by fsi-forget below. 
  (erbn-log 'set-force args)
  (erbn-unlog
    (let* ((fir (first args))
	   (aa (erbbdb-get-exact-notes fir))
	   (notes (and (stringp aa) (erbn-read aa)))
	   (len (length notes)))
      (when (= len 0)
	(error "[set-force] There's no such term %s.  Use , %s is ..." fir fir))
      (unless (= len 1)
	(error
	 "[set-force] Term has multiple notes. Examine them and ask me to forget them individually."))
      (erbutils-ignore-errors (funcall 'fsi-forget (first args) "all"))
      (apply 'fsi-set-term args))))




(defun erbn-fortune-yow ()
  (let ((fortune-file "/home/fsbot/yow.lines"))
    (fsi-unfill-string
     (erbn-shell-command-to-string
      (concat "fortune " fortune-file)
      (list erbn-fortune-p)))))
    
(defun erbn-fortune (arg)
  (unless arg (setq arg ""))
  (cond
   ((string= arg "")
    (progn
     (erbutils-eval-until-limited
      '(erbn-shell-command-to-string (concat "fortune -s " arg)
				     (list erbn-fortune-p)
				     ))))
   (t
    (progn
     (erbn-shell-command-to-string (concat "fortune -s " arg)
				   (list erbn-fortune-p)
				   )))))


(defun fsi-fortune (&rest args)
  (erbn-fortune "fortunes"))


(defalias 'fsi-f-default 'fsi-fortune)

(defun fsi-fortunes-help (&rest args)
  (concat "Type ,fortune, or any of the commands beginning with f- : "
	  (fsi-commands "^f-")))

(defalias 'fsi-fortune-help 'fsi-fortunes-help)
(defalias 'fsi-f-help 'fsi-fortunes-help)


(defun fsi-f-f (&rest args)
  (fsi-unfill (erbn-fortune "-f")))

(defun fsi-f-off (&rest args)
  (fsi-unfill (erbn-fortune "-o")))

(defalias 'fsi-f-o 'fsi-f-off)
(defalias 'fsi-f-offensive 'fsi-f-off)


(defun fsi-f-debian-hints (&rest args)
  (fsi-unfill (erbn-fortune "debian-hints")))

(defun fsi-f-zippy (&rest args)
  ;; yow
  (fsi-unfill (erbn-fortune "zippy")))

(defalias 'fsi-debian-hints 'fsi-f-debian-hints)



(defun fsi-f-twisted-quotes (&rest args)
  (fsi-unfill (erbn-fortune "twisted-quotes")))


(defalias 'fsi-quotes 'fsi-f-twisted-quotes)
(defalias 'fsi-f-quotes 'fsi-f-twisted-quotes)

(defun fsi-f-literature (&rest args)
  (fsi-unfill (erbn-fortune "literature")))
(defalias 'fsi-f-lit 'fsi-f-literature)
(defalias 'fsi-lit 'fsi-f-literature)
(defalias 'fsi-literature 'fsi-f-literature)



(defun fsi-f-riddles(&rest args)
  (fsi-unfill (erbn-fortune "riddles")))
(defalias 'fsi-riddle 'fsi-f-riddles)



(defun fsi-f-art (&rest args)
  (fsi-unfill (erbn-fortune "art")))

(defalias 'fsi-art 'fsi-f-art)




(defun fsi-f-bofh-excuses (&rest args)
  (fsi-unfill (erbn-fortune "bofh-excuses")))
(defalias 'fsi-bofh 'fsi-f-bofh-excuses)




(defun fsi-f-ascii-art (&rest args)
  (erbn-fortune "ascii-art"))
(defalias 'fsi-ascii 'fsi-f-ascii-art)




(defun fsi-f-computers (&rest args)
  (fsi-unfill (erbn-fortune "computers")))

(defalias 'fsi-f-computer 'fsi-f-computers)





(defun fsi-f-cookies (&rest args)
  (erbn-fortune "cookies"))

(defalias 'fsi-f-cookie 'fsi-f-cookies)
(defalias 'fsi-cookie 'fsi-f-cookies)





(defalias 'fsi-f-cookie 'fsi-f-cookies) ;: doesn't produce anything atm!
;; (defalias 'fsi-cookie 'fsi-f-cookies)
(defalias 'fsi-cookie 'fsi-fortune)

(defun fsi-f-definitions (&rest args)
  (fsi-unfill (erbn-fortune "definitions")))

(defalias 'fsi-def 'fsi-f-defintions)




(defun fsi-f-drugs (&rest args)
  (fsi-unfill (erbn-fortune "drugs")))
(defalias 'fsi-drugs 'fsi-f-drugs)
(defalias 'fsi-drug 'fsi-f-drugs)




(defun fsi-f-education (&rest args)
  (fsi-unfill (erbn-fortune "education")))


(defun fsi-f-ethnic (&rest args)
  (fsi-unfill (erbn-fortune "ethnic")))




(defun fsi-f-food (&rest args)
  (fsi-unfill (erbn-fortune "food")))
(defalias 'fsi-food 'fsi-f-food)






(defun fsi-f-goedel (&rest args)
  (fsi-unfill (erbn-fortune "goedel")))
(defalias 'fsi-goedel 'fsi-f-goedel)




(defun fsi-f-humorists (&rest args)
  (fsi-unfill (erbn-fortune "humorists")))


(defun fsi-f-kids (&rest args)
  (fsi-unfill (erbn-fortune "kids")))


(defun fsi-f-law (&rest args)
  (fsi-unfill (erbn-fortune "law")))

(defalias 'fsi-law 'fsi-f-law)



(defun fsi-f-linuxcookie (&rest args)
  (fsi-unfill (erbn-fortune "linuxcookie")))


(defun fsi-f-love (&rest args)
  (fsi-unfill (erbn-fortune "love")))

(defun fsi-f-magic (&rest args)
  (fsi-unfill (erbn-fortune "magic")))



(defun fsi-f-medicine(&rest args)
  (fsi-unfill (erbn-fortune "medicine")))



(defun fsi-f-men-women (&rest args)
  (fsi-unfill (erbn-fortune "men-women")))

(defalias 'fsi-sexwar 'fsi-f-men-women)





(defun fsi-f-miscellaneous(&rest args)
  (fsi-unfill (erbn-fortune "miscellaneous")))

(defalias 'fsi-f-misc 'fsi-f-miscellaneous)



(defun fsi-f-news (&rest args)
  (fsi-unfill (erbn-fortune "news")))



(defun fsi-f-people (&rest args)
  (fsi-unfill (erbn-fortune "people")))


(defun fsi-f-pets (&rest args)
  (fsi-unfill (erbn-fortune "pets")))



(defun fsi-f-platitudes (&rest args)
  (fsi-unfill (erbn-fortune "platitudes")))



(defun fsi-f-politics (&rest args)
  (fsi-unfill (erbn-fortune "politics")))


(defun fsi-f-science (&rest args)
  (erbn-fortune "science"))

(defun fsi-f-songs-poems (&rest args)
  (fsi-unfill (erbn-fortune "songs-poems")))


(defun fsi-f-sports(&rest args)
  (fsi-unfill (erbn-fortune "sports")))





(defun fsi-f-startrek (&rest args)
  (fsi-unfill (erbn-fortune "startrek")))
(defalias 'fsi-startrek 'fsi-f-startrek)





(defun fsi-f-translate-me (&rest args)
  (fsi-unfill (erbn-fortune "translate-me")))



(defun fsi-f-wisdom(&rest args)
  (fsi-unfill (erbn-fortune "wisdom")))
(defalias 'fsi-wisdom 'fsi-f-wisdom)



(defun fsi-f-work (&rest args)
  (fsi-unfill (erbn-fortune "work")))



(defun fsi-f-linux (&rest args)
  (fsi-unfill (erbn-fortune "linux")))

(defun fsi-f-perl (&rest args)
  (fsi-unfill (erbn-fortune "perl")))

(defun fsi-f-knghtbrd (&rest args)
  (fsi-unfill (erbn-fortune "knghtbrd")))




(defun fsi-f-quotes-emacs-channel (&rest args)
  (erbn-fortune "~/fortune-emacschannelquotes.txt"))
(defalias 'fsi-f-emacs 'fsi-f-quotes-emacs-channel)
(defalias 'fsi-f-quotes-emacs 'fsi-f-quotes-emacs-channel)
(defalias 'fsi-quotes-emacs 'fsi-f-quotes-emacs-channel)
(defalias 'fsi-quotes-emacs-channel 'fsi-f-quotes-emacs-channel)









;; (defalias 'fsi-cons 'cons)







(defun fsi-limit-string (&optional str maxlen &rest ignored)
  "Fills the string and then then limits lines"
  ;; (error "unused p ")
  (fsi-limit-lines (fsi-fill-string str)))


(defun fsi-fill-string (&optional str &rest ig)
  ;; (error "unused p ")
  (if (null str) (setq str ""))
  (with-temp-buffer
    (insert str)
    (let ((fill-column fsi-internal-fill-column))
      (text-mode)
      (fill-region (point-min) (point-max))
      (buffer-substring-no-properties (point-min) (point-max)))))

(defalias 'fsi-fill 'fsi-fill-string)

(defun fsi-unfill-string (&optional str &rest strs)
  (let ((str (apply 'fsi-stringifyd str strs)))
    (let ((fsi-internal-fill-column 9999))
      (fsi-fill-string str))))

(defalias 'fsi-unfill 'fsi-unfill-string)

(defun fsi-limit-string-old (&optional str maxlen &rest ignored)
  (cond
   (str
    (unless (stringp str)
      (setq str (format "%s" str)))
    ;; get rid of all the \n first..
    (setq str
	  (mapconcat 'identity
		     (split-string str "\n")
		     "  "))
    (when (> (length str) fsi-internal-limit-length)
      (setq str (concat (substring str 0 (- fsi-internal-limit-length 7))
			"..<more>")))
    (with-temp-buffer
      (insert str)
      (goto-char (point-min))
      (let ((fill-column fsi-internal-fill-column))
	(fill-paragraph nil))
      (erbutils-buffer-string)))
   (t "\n")))

(defun fsi-dunnet-mode ( &rest ig)
    (let ((target (not fsi-dunnet-mode)))
      (when target
	nil  ;; we could choose to kill the dunnet buffer here, but that leads to weird errors 201406.
	)
      (setf fsi-dunnet-mode target)
      (format "Dunnet mode set to %S.  To toggle, type , (dunnet-mode)"
	      fsi-dunnet-mode)))

(defun fsi-limit-string-no-fill (&optional str limit-lines
				      limit-length
				      limit-line-length
				      &rest ignored
				      )
  "IS OLD. i think.  not used anywwhere...  certainly screws up more:
is not compliant with fsbot paginator.

Limit string to reasonable length..
Not more than fsi-internal-limit-line-length characters per line, and
not more than fsi-internal-limit-length characters in all.. and not more
than fsi-limit-lines in all.."
  (if str
      (let ((fsi-limit-lines
	     (or limit-lines fsi-limit-lines))
	    (fsi-internal-limit-length
	     (or limit-length
		 fsi-internal-limit-length))
	    (fsi-limit-line-length
	     (or limit-line-length
		 fsi-internal-limit-line-length)))
	(fsi-limit-lines
	 (fsi-internal-limit-length
	  (fsi-limit-line-length
	   str t))))
    "\n"))




(defun erbn-more-get (&optional target)
  "When target is nil, we get the latest more that occurred in ANY
channel, else we get the more from the channel indicated by target. 
Should only be called by fsi-more."
  (setq target (format "%S" target))
  ;; this is set by fsi-more instead.
  ;;  (setf fsi-inside-more-p t) ;; note that this is a local since erbot-remote created a local scope
  (cdr (assoc target erbn-more)))

(defun erbn-morep-get (&optional target)
  "Whether last command had a morep."
  (setq target (format "%S" target))
  ;; this is set by fsi-more instead.
  ;;  (setf fsi-inside-more-p t) ;; note that this is a local since erbot-remote created a local scope
  (cdr (assoc target erbn-morep)))



(defun erbn-more-get-old1 (&optional target)
  "When target is nil, we get the latest more that occurred in ANY
channel, else we get the more from the channel indicated by target. "
  (setq target (format "%S" target))
  (setf fsi-inside-more-p t) ;; note that this is a local since erbot-remote created a local scope
  (let ((str (cdr (assoc target erbn-more))))
    (if (and (stringp str)
	     (not (string= str "")))
	str
      (fsi-describe "more"))))


;; (defalias 'fsi-more-get 'erbn-more-get)

(defun erbn-more-set (str &optional target)
  (unless target (setf target erbn-tgt))
  (let ((more (list erbn-task-current str)))
    (setq target (format "%S" target))
    (if (assoc target erbn-more)
	(setf (cdr (assoc target erbn-more)) more)
      (add-to-list 'erbn-more (cons target more)))
    (if (assoc "nil" erbn-more)
	(setf (cdr (assoc "nil" erbn-more)) more)
      (add-to-list 'erbn-more (cons "nil" more)))
    erbn-more))

(defun erbn-morep-set (morep &optional target)
  (unless target (setf target erbn-tgt))
  (let (none)
    (setq target (format "%S" target))
    (if (assoc target erbn-morep)
	(setf (cdr (assoc target erbn-morep)) morep)
      (add-to-list 'erbn-morep (cons target morep)))
    (if (assoc "nil" erbn-morep)
	(setf (cdr (assoc "nil" erbn-morep)) morep)
      (add-to-list 'erbn-morep (cons "nil" morep)))
    erbn-morep))


(defun erbn-task-history-set (&optional task target)
  "Note that history is set in reverse order per target."
  (unless task (setf task erbn-task-current))
  (unless target (setf target erbn-tgt))
  (setq target (format "%S" target))
  (if (assoc target erbn-task-history)
      (push task (cdr (assoc target erbn-task-history)))
    (add-to-list 'erbn-task-history (cons target (list task ))))
  erbn-task-history)

(defun erbn-task-history-get (&optional target)
  (unless target (setf target erbn-tgt))
  (setf target (format "%S" target))
  (cdr (assoc target erbn-task-history)))


(defun erbn-same-command-p (&optional target task)
  (unless target (setf target erbn-tgt))
  (unless task (setf task erbn-task-current))
  (let ((his (erbn-task-history-get target)))
    (and his (equalp (first his) task)
	 (> (length (format "%s" task)) 0))))


(defun fsi-more-set (&optional str)
  (error "erbn-more-set is an internal function. We should not be here once this is done. This is being modified atm."))



(defun fsi-limit-lines (str0 &optional nomorep &rest ignored)
  "Limits the string, both, to a reasonable number of lines and a
reasonable number of characters, trying not to break lines and not to
break words, if possible.

Thus, that becomes quite a complicated algorithm, and we do that
here."
  (let* (ans
	 (ender "")
	 (more "")
	 (stra (erbutils-remove-text-properties str0))
	 (str (mapconcat 'identity
			 (remove "" (split-string stra "\n"))
			 "\n"))
	 (limitedp nil)
	 ptmx
	 this-line
	 this-point
	 new-point
	 )
    (with-temp-buffer
      ;; per fledermaus: ensure that the buffer's byteness  matches the str's.
      (set-buffer-multibyte (multibyte-string-p str))
      (insert str)
      (setq ptmx (point-max))
      (setq this-point ptmx new-point ptmx)
      (if (> fsi-internal-limit-length ptmx)
	  (goto-char ptmx)
	(setq limitedp t)
	(goto-char fsi-internal-limit-length))
      ;;(goto-char (point-max))
      ;;(remove-text-properties (point-min) (point-max))
      (setq this-line (count-lines (point-min) (point)))
      (when (> this-line fsi-limit-lines)
	(setq limitedp t)
	(goto-line fsi-limit-lines)
	(setq this-line fsi-limit-lines)
	)

      (setq this-point (point) new-point this-point)

      (cond
       ((and limitedp (> this-line 1))
	(progn (beginning-of-line)
	       (setq new-point (point))
	       (backward-char) (setq this-point (point))
	       ))
       ((and limitedp
	     (progn (ignore-errors
		      ;; we want a backward-word 1 here, but only
		      ;; whitespace is regarded as word-boundary for
		      ;; us.
		      (when
			  (search-backward-regexp "\\( \\|\n\\|\t\\)" nil t)
			(forward-char 1))
		      ;;(backward-word 1)
		      )
		    (> (point) (point-min))))
	(setq new-point (point))
	(setq this-point new-point))


       ;;(limitedp (setq this-point (point) new-point (point)))

       ;; in the final case, this-point and new-point are already at
       ;;point-max...
       (t nil))
      (setq ans (buffer-substring (point-min) this-point))
      (when
	  ;;(< this-point (point-max))
	  limitedp
	(setq more (buffer-substring new-point (point-max)))
	(if
	    (string-match "[^ \t\n]" more )
	    (setq ans (concat ans (fsi-get-more-invocation-string))) 
	  (when nomorep (setq more "")))
	)
      )
    ;;(setq fs-more more)

    ;; do we really need this? why empty out the more field? Why not let fsbot continue describing the previous term?
    ;; often, the user will thank fsbot, and then type more, expecting the bot to describe the preceding field.
    ;; Thus, set this ONLY when (length more) is positive.
    ;; But, this needs to be done very carefully. If we are already inside a more, we DO need it reset, for example.

    (if
	(or
	 ;; reset if already inside more. This is because we want ,more to finish after the final description, and not keep repeating it.
	 fsi-inside-more-p
	 ;; If the current description leads to more, then set more. BUT, if not, then DO NOT reset more, so that the user may continue a preceding description via typing more.
	 (> (length more) 0)
	 
	 ;; OR, if EXISTING more has NO task set with it.
	 (member (format "%s" (car (erbn-more-get))) '("" "nil"))
	 )
	(progn
	  (erbn-more-set more erbn-tgt)
	  (erbn-morep-set t erbn-tgt)
	  )
      (erbn-morep-set nil erbn-tgt)
      )

    ans))

(defun fsi-get-more-invocation-string ()
  (if (erbot-safe-nocontrol-p erbn-char)
      (concat " ;;[ " erbn-char "more / " erbn-char "dump]")
    (concat " ;;[Type " erbot-nick ": more or " erbot-nick ": dump]")))

(defun fsi-limit-lines-old (str0 &rest ignored)
  ""
  (let* (
	 (str (erbutils-remove-text-properties str0))
	 (brstr1 (split-string str "\n"))
	 (brstr (remove "" brstr1))
	 (ender "")
	 (condp (> (length brstr) fsi-limit-lines))
	 (goodstr
	  (if condp
	      (progn
		(setq ender "..+ more")
		(subseq brstr 0 (- fsi-limit-lines 1)))
	    brstr)))
    (if condp (erbn-more-set
		      (mapconcat 'identity
				 (subseq brstr (- fsi-limit-lines
						  1))
				 "\n"))
      (progn
	;; do we really need this? why empty out the more field? Why not let fsbot continue describing the previous term?
	;; often, the user will thank fsbot, and then type more, expecting the bot to describe the preceding field.
	(erbn-more-set "")
	)
      )
    (concat (mapconcat 'identity goodstr "\n") ender)))




(defun fsi-task-current ()
  "Output current task. Meant for debugging, really. For example, ,(progn (df 'ff) (task-current))   will print df:fs-ff."
  erbn-task-current)





(defun fsi-more (&rest args)
  "Display the contents of the cache. 
Note that the ,dump and ,more commands by themselves do not become part of your ,history. 
"
  
  (setf fsi-inside-more-p t)
  (let* ((more (erbn-more-get erbn-tgt))
	 (doing (when more (first more))) ;; nil otherwise.
	 (str (when more (second more)))) ;; nil otherwise.
    (if (and str (stringp str)
	     (not (string= str "")))
	(progn
	  ;; This used to set the task to the preceding term. But, now on, it will be set to "more" so that it doesn't form part of the dump-history.
	  ;; (setf erbn-task-current doing) 
	  (setf erbn-task-current doing)
	  (fsi-more-with-task-maybe doing str))
      (fsi-describe "more"))))

(defalias 'fsi-m 'fsi-more)
(defun fsi-more-with-task-maybe (doing str)
  ;; (if (erbn-same-command-p nil doing)
  ;; If last command had set a more, then we are ok, else we should prepend doing to the current output.
  (if (erbn-morep-get erbn-tgt)
      str
    (fsi-more-with-task doing str)))

(defun fsi-more-with-task (doing str)
  (if (and (stringp doing) (> (length doing) 0))
      ;; (concat doing "" str)
      (format "[%s] %s" doing str)
    str)
  )

;;   (if (and (stringp fs-more)
;; 	   (not (string= fs-more "")))
;;       fs-more
;;     (fs-describe "more")))


(defun fsi-limit-lines-long (str &rest ignored)
  ""
  (let ((fsi-limit-lines 7))
    (apply 'fsi-limit-lines str ignored)))



(defun fsi-limit-length (str &rest ignored)
  "Don't use this, use fsi-limit-lines"
  (if (> (length str) fsi-internal-limit-length)
      (concat (substring str 0 (- fsi-internal-limit-length 1)) "...<more>")
    str))

(defun fsi-limit-line-length (&optional str &rest args)
  "a subfunction.."
 (let* (
	;; this not needed now..
	(brokenstr (split-string str "\n"))
	(newlsstr
	 (mapcar
	  '(lambda (givenstr)
	     (let ((ls nil)
		   (thisstr givenstr)
		   )
	       (while (> (length thisstr)
			 fsi-internal-limit-line-length)
		 (push
		  (concat (substring thisstr 0 fsi-internal-limit-line-length
						  ) " <break>")
		  ls)
		 (setq thisstr (substring thisstr fsi-internal-limit-line-length
					  (length thisstr))))
	       (push thisstr ls)
	       (reverse ls)))
	  brokenstr))
	(newbrokenstr
	 (apply 'append newlsstr)))
   (mapconcat 'identity newbrokenstr "\n")))



(defun fsi-tell-to (&optional string nick &rest ignored)
  (unless (and string nick)
    (error "Format: tell foo about <anything>"))
  (setq fs-nick (format "%s" nick))
  (let* ((fsi-internal-directed t)
	 (ni (if (string= (format "%s" nick) "me")
		 erbot-end-user-nick
	       (format "%s" nick)))
	 (reply
	  (erbeng-get-reply (fsi-parse (concat erbot-nick ": "
					       string))))
	 ;; This reply above will begin with a space, unless it is of the form /me foo.
	 ;; Nonetheless, we shouldn't rely on that behavior.
	 ;; We shouldn't introduce a vuln such as someone making as /quit.
	 ;; So, (a) sanititize nick. (b)  IF the reply leads to a /me action, let that go through. 
	 (reply (format "%s" reply))
	 (donep
	  (or
	   (string-match ni reply)
	   (string-match "^/me " reply))) ;; /me is confused should NOT become Guest: /me is confused
	 )
    (setf nick (fsi-replace-string-in-string "/" "" nick))
    (if donep
	reply
      (concat ni ": " reply))))

(put 'fsi-tell 'readonly t) ;; so it can't be user-defunned. We want fsi-tell and fs-tell to be unbound so that "tell a about b" parses correctly to tell-to.
(put 'fs-tell 'readonly t) ;; so it can't be user-defunned. We want fsi-tell and fs-tell to be unbound so that "tell a about b" parses correctly to tell-to.

(defun fsi-apropos (&optional regexp N M &rest ignored)
  (fsi-apropos-basic 'erbn-apropos regexp N M))

(defun fsi-apropos-command (&optional regexp n m &rest ignored)
  (fsi-apropos-basic 'erbn-apropos-command regexp n m ))
(defun fsi-apropos-variable (&optional regexp n m &rest ignored)
  (fsi-apropos-basic 'erbn-apropos-variable regexp n m ))
(defun fsi-apropos-function (&optional regexp n m &rest ignored)
  (fsi-apropos-basic 'erbn-apropos-function regexp n m ))
(defun fsi-apropos-value (&optional regexp n m &rest ignored)
  ;;(fsi-apropos-basic 'apropos-value regexp n m )
  "This function has been disabled as it is too resource-intensive.")

(defun fsi-apropos-documentation (&optional regexp n m &rest ignored)
  (fsi-apropos-basic 'erbn-apropos-documentation  regexp n m ))

(defun erbn-apropos-documentation (reg)
  (mapcar 'car (apropos-documentation reg)))
(defun erbn-apropos-command (reg)
  (apropos-internal reg
		    'commandp))



(defun erbn-apropos-function (reg)
  (apropos-internal reg
		    'functionp))

(defun erbn-apropos-variable (reg)
  (apropos-internal reg
		    (lambda (s)
		      (or (boundp s)
			  (user-variable-p s)))))


(defun erbn-apropos (regexp)
  (assert (stringp regexp))
  (apropos-internal regexp
		    (lambda (symbol)
		      (or
		       (boundp symbol)
		       (fboundp symbol)
		       (facep symbol)
		       (symbol-plist symbol)))))

(defun fsi-apropos-basic (fcn &optional regexp N M &rest ignored)
  "Show the apropos-matches  of regexp starting at match number N"
  (unless regexp
    (error "[apropos-basic] Syntax: , apropos REGEXP &optional N M"))
  (if (stringp N) (setq N (erbn-read N)))
  (unless (integerp N) (setq N 0))
  (unless (stringp regexp)
    (setq regexp (format "%s" regexp)))
  (let* ((results (funcall fcn regexp))
	 (len (length results))
	 (str0 "")
	 (str1 "")
	 (str2 "")
	 (str3 "")
	 (str4 ""))
    (unless (and (integerp M) (< M len))
      (setq M len))
    (if (and (= N  0 ) (= M len) (> len 30))
	(setq
	 str0
	 "Note: Try , df fs-apropos for general syntax.  "))
    (if (> len 1) (setq str1 (format "%s matches.  " len)))
    (if (> N 0) (setq str2 (format "Matches starting at %s->" N)))
    (setq str3 (progn (format "%s"
					  (subseq results
						  N M)
					  )))
    (concat str0 str1 str2 str3 str4)))


(defun fsi-find-variable (function &rest ignore)
  (error "[fv] Find variable can potentially be used to read internal variables. Disabled.")
  (fsi-find-variable-internal function  'nolimit))

(defun fsi-find-variable-internal (function &optional nolimitp &rest ignore)
  "Finds the variable named FUNCTION."
  (error "[fvi] Find variable can potentially be used to read internal variables. Disabled.")
  (if (stringp function) (setq function (erbn-read function)))
  (cond
   ((symbolp function)
    (unless (boundp function)
      (let ((g (intern (concat "fs-" (format "%s" function)))))
	(if (boundp g)
	    (setq function g))))
    (erbn-task-set (format "fv:%s" function))
    (let ((fstr
	   (save-excursion
	     (find-function-do-it function 'defvar 'set-buffer)
	     (buffer-substring (point)
			       (save-excursion
				 (forward-sexp 1)
				 (point))))))
      (if (equal nolimitp 'nolimit)
	  fstr
	fstr)))
   (t "\n")))

(defalias 'fsi-find-variable-briefly 'fsi-find-variable)


(defun fsi-find-function-verbatim (&optional function &rest ignore)
  (unless function
    (error "[ffv] Syntax: , find-function-verbatim 'function-name"))
  ;;fsi-limit-lines-long
  (erbn-find-function-internal
   function 'nolimit))

(defalias 'fsi-find-function-literally 'fsi-find-function-verbatim)


(defun fsi-find-function (&optional function &rest ignore)
  (unless function
    (error "Syntax: , find-function function-name"))
  ;;fsi-limit-lines-long
  (unless (symbolp function)
    (error "fs-find-function only works on symbols."))
  (erbn-find-function-internal
   function 'nolimit 'cleanup))




(defun fsi-functionchaseorerror (function)
  ;;; "TODO: look at (indirect-function) and see if it applies here."
  (assert (symbolp function))
  (when
      (fsi-functionbadp function)
    (error (format "DEFALIAS PROBLEM: As of versions 24 and 25, emacs will hang if we proceed: %S" (fsi-functionbadp function)))))

(defun fsi-functionbadp (sym)
  "If the symbol (ultimately) points to an actual function, return nil, else return a string describing the badness."
  ;;; "TODO: look at (indirect-function) and see if it applies here."
  (let (
	(sym1 sym)
	(ls (list sym))   ;; ls keeps track of all previously seen items.
	(succ nil)
	(fail nil)
	(done nil))
    (when (not (symbolp sym))
      (setf done t succ nil fail "This function only works on symbols."))  ;; We ONLY accept symbols!
    (while (not done)
      (setf sym (symbol-function sym))
      (cond
       ((null sym)
	(setf done t succ nil fail (format "A defalias points to a symbol that is undefined when chasing %S, which led me through: %S" sym1 (reverse ls))))
       ((not (symbolp sym)) ;; We ended up at an actual function.
	(setf done t succ t fail nil))
       ((and (symbolp sym) (member sym ls)) ;; an alias  loop is detected.
	(setf done t succ nil fail (format "A loop is detected within defaliases when chasing %S, which led me through %S" sym1 (reverse ls))))
       ((symbolp sym)
	(setf done nil succ nil fail nil ls (cons sym ls)))
       (t (error "[functionbadp] I should not be here."))))
    fail))




(defalias 'fsi-ffv 'fsi-find-function-verbatim)
(defalias 'fsi-ff 'fsi-find-function)


(defalias 'fsi-find-function-briefly 'fsi-find-function)

(defun fsi-find-function-on-key (&optional k &rest rest)
  (unless k
    (error
     "Syntax (ffo <key>)"))
  (fsi-find-function (fsi-describe-key-briefly k)))

(defun fsi-find-function-on-key-briefly (k &rest rest)
  (fsi-find-function-briefly (fsi-describe-key-briefly k)))

(defun erbn-find-function-internal (&optional function nolimitp cleanp &rest nada)
  (unless function
    (error
     "[ffi] Syntax: (ff 'fucntion)"))
  (if (stringp function) (setq function (erbn-read function)))
  (cond
   ((symbolp function)
    (unless (fboundp function)
      (let ((g (intern (concat "fs-" (format "%s" function)))))
	(if (fboundp g)
	    (setq function g))))
    ;; Add an extra check, otherwise find-function-do-it will hang.
    (unless (fboundp function) (error "[ffi] No matching function found."))

    ;; (setf erbn-task-current (erbn-task-shorten (format "[ff:%s]" function)))
    (erbn-task-set (format "ff:%s" function))
    (fsi-functionchaseorerror function)
    (condition-case nil 
	;; If we can locate the funciton def, do so.
	(erbn-find-function-internal1 function cleanp)
      (error
       ;; else simply return the function definition.
       ;; this happens when the definition cannot be located, which happens when the function does not exist in a source code format anywhere.
       ;; and that happens when, for example, you define the function using (erbutils-defalias-n ..)
       (fsi-symbol-function-chase function))))
   (t "\n")))


(defun fsi-symbol-function-chase (fun)
  "      fs-ff sometimes falls back upon this.
       ;; this happens when the definition cannot be located, which happens when the function does not exist in a source code format anywhere.
       ;; and that happens when, for example, you define the function using (erbutils-defalias-n ..)"
  (assert (symbolp fun))
  (when (> erbn-symbol-function-chase-level 10)
    (error "Recursion depth of 10 exceeded when chasing symbols encountered in symbol-function."))
  (let ((erbn-symbol-function-chase-level (+ 1 erbn-symbol-function-chase-level))
	(chase (symbol-function fun)))
    (cond
     ((listp chase) chase)
     ((symbolp chase) (fsi-symbol-function-chase chase))
     (t (format "%S" chase)))))


(defun erbn-find-function-internal1 (function cleanp)
  (let* ((fstrbare
	  (save-excursion
	    
	    ;; This has the problem that it is interactive.. asks to
	    ;; reread file if has changed etc.
	     ;;(find-function function)

	    ;;  20191227 used to be this, 
	    ;; (find-function-do-it function nil 'set-buffer)
	    ;; but in e25, leads to this error:
	    ;; Debugger entered--Lisp error: (error "‘recenter’ing a window that does not display current-buffer.")
	    ;; trying switch-to-buffer
	    (find-function-do-it function nil 'switch-to-buffer)
	    (buffer-substring (point)
			      (save-excursion
				(forward-sexp 1)
				(point)))))
	 (fstr1 (erbutils-function-minus-doc fstrbare))
	 (fstr (if cleanp (erbutils-function-cleanup fstr1)
		 fstr1)))
    (if (equal nolimitp 'nolimit)
	fstr
      (concat (format "%s characters.." (length fstr))
	      fstr))))



;;; 2002-11-10 T14:50:20-0500 (Sunday)    D. Goel
(defun fsi-say (&rest args)
  ;; let's make it safe, even though we know it will be made safe again...
  (let ((response
	 (mapconcat
	  '(lambda (arg)
	     (format "%s" arg))
	  args " ")))
    (if (erbot-safe-p response) response
      (concat " " response))))









(defun fsi-regexp-quote (str)
  (unless (stringp str)
    (setq str (format "%s" str)))
  (regexp-quote str))


(defun fsi-concat (&rest sequences)
  (apply 'concat
	 (mapcar
	  'erbutils-convert-sequence
	  sequences)))








(defun erbnocmd-user-fcn-definition  (&optional mainterm )
  "The general syntax is (fs-describe TERM [N] [M]).
Looks for TERM, and shows its descriptions starting at description
number N, and ending at M-1. The first record is numbered 0.
"
  (unless mainterm
    (error
     "[eufd] Format , (describe TERM &optional number1 number2)"))
  (unless mainterm
    (setq mainterm (format "%s" mainterm)))
  (setq mainterm (fsi-correct-entry mainterm))
  (let* ((result0
	  (erbbdb-get-exact-notes
	   mainterm
	   ))
	 (result1 (and (stringp result0)
		       (ignore-errors (erbn-read result0))))
	 (len (length result1)))
      (cond
       ;; in cond0
       (result1
	(let* (
	       ;; notice the use of result1 here, not result.
	       (aa (first result1))
	       (bb (split-string aa))
	       (cc (first bb))
	       (dd (second bb))
	       (ee (cdr bb))
	       )
	  (cond
	   (
	    (erbutils-string= cc "redirect")
	    dd)
	   (t nil)))))))




;; this asks the google bot for results and gives it to our channel
;;(defvar erbnocmd-google-stack nil)
;;(defun fs-google (&rest args)
;; (progn
;;  (add-to-list 'erbnocmd-google-stack 'foo))
;; (erc-cmd-MSG google "hi")
;; nil)



(defun old_fsi-unhex-hack (str)
   "A hack by deego to fix weirdness from google.el.

Unhex everything, but then, replace all spaces, and only spaces,  by %20s, and only spaces. 
A hack to fix some some weird results from fsi-google.

In fsi-google-raw, some strings are encoded twice(!). This hack attempts corrects that, and creates actual clickable url's.
  UPDATE: It really ONLY happens for spaces, but this should help either way."
  (require 'url)
  ;; (url-hexify-string (fsi-unhex-unlimited str))) ;; this won't work!
  (fsi-replace-string-in-string " " "%20" (fsi-unhex-unlimited str)))

  
(defun fsi-unhex-unlimited (str)
  "A hack by deego to fix weirdness returned from google.el. Use fsi-unhex-hack to correct that. This is a utility used by that function.
In fsi-google-raw, some strings are encoded twice(!). This hack corrects that. Here, we simply keep decoding till there's no more to decode.
  UPDATE: It really ONLY happens for spaces, but this should help either way."
  (require 'url)
  (let ((out str) (out1 str) (done nil))
    (while (not done)
      (setf out1 out
	    out (url-unhex-string out1)
	    done (string= out out1)))
    out))


;; not used by use, but provided.
(defun fsi-url-hexify-string (string &rest ignore)
  (url-hexify-string string))
(defalias 'fsi-hex-string 'fsi-url-hexify-string)
(defalias 'fsi-unhex-string 'url-unhex-string)
(defalias 'fsi-url-unhex-string 'fsi-unhex-string)



;; (defun fsi-google-raw (&rest args)
;;   "Return a list of google results. "
;;   ;; (require 'google-this)
;;   (let ((concatted
;; 	 (mapconcat '(lambda (a) (format "%s" a)) args " ")))
;;     (with-timeout
;; 	(fsi-internal-google-time
;; 	 (list concatted (list "google---TimedOut")))
;;       (let ((results
;; 	     ;; this ignore-errors is very important.
;; 	     ;; since the google stuff currently gives weird errors
;; 	     ;; when called from within a with-timeout loop, and a
;; 	     ;; timeout actually occurs.
;; 	     (ignore-errors
;; 	       (mapcar 'list
;; 		       (google-result-urls
;; 			(google-search concatted 0 "web")) )) ))
;; 	;;(message "got results %S" results)
;; 	results)) ))





(defun fsi-googlen (n &rest args)
  "Format the first n results in a nice format. "
  (when (not fsi-internal-google-redirect-p)
    (fsi-google-error))
  (let* ((rawres (ignore-errors (apply 'fsi-google-raw args)))
	 (terms (first rawres))
	 (matches (cdr rawres)))
    (when (> (length matches) n)
      (setq matches (subseq matches 0 n)))
    (cond
     ((or (not (null matches)) (not fsi-internal-google-redirect-p))
      (format "%s"
	      ;;terms
	      (if matches
		  (mapconcat 'car matches "\n")
		"No match. ")))
     (t
      (fsi-english-only
       fsi-internal-original-message
       fsi-internal-addressedatlast
       'nogoogle
       )))))

(defun fsi-google-lucky-raw (&rest args)
  (caadr (apply 'fsi-google-raw args)))


(defun fsi-google-redirect-to-google-bot (&rest args)
  (concat "google: "
	  (mapconcat
	   '(lambda (arg) (format "%s" arg))
	   args " ")))



(defun fsi-google-from-english (&rest args)
  (let ((fsi-internal-google-redirect-p t))
    (apply 'fsi-google args)))




(defun fsi-google-using-erbot_old (&rest args)
  (unless args (error "[gue] Syntax: , g[oogle] [NUMBER] WORD1 &rest MORE-WORDS "))
  (let (num
	(fir (first args))
	)
    (when (> (length args) 1)
      (setq num
	    (if (numberp fir)
		fir
	      (ignore-errors (erbn-read fir)))))
    (if (numberp num)
	(setq args (cdr args))
      (setq num 2))
    (apply 'fsi-googlen num args)))

(defun fsi-google-using-rudybot (&rest args)
  (format "rudybot: g %s" (fsi-stringifyd args)))


;; (defalias 'fsi-google 'fsi-google-using-rudybot)
;; (defalias 'fsi-google 'fsi-google-using-erbot) ;; please don't change this. fsi-google is still used by erbot internals! 
;; (defalias 'fsi-g 'fsi-google-using-erbot) ;; default

;; 20230409 
(defalias 'fsi-google 'erbn-google ) ;;
(defalias 'fsi-goog 'fsi-google)
(defalias 'fsi-gg 'fsi-google)

;; gg is for google.
;; (defalias 'fsi-g 'fsi-google-using-rudybot) ;; temporary till our own google is fixed.


(defun fsi-google-with-options (options terms &rest args)
  "internal"
  (apply 'fsi-google (append (list options) terms args)))

(defun fsi-ggdeego (&rest args)
  "Google on the gnufans.net."
  (fsi-google-with-options "site:gnufans.net" args))
(defalias 'fsi-google-deego 'fsi-ggdeego)

(defun fsi-gge (&rest args)
  "Google on the emacswiki site."
  ;; (fsi-google-with-options "2" (cons "site:emacswiki.org/emacs" args)))
  ;; for rudybot.
  (fsi-google-with-options "site:emacswiki.org/emacs" args))
(defalias 'fsi-google-emacswiki 'fsi-gge)


(defun fsi-ggo  (&rest args)
  "Google on the octave site."
  (fsi-google-with-options "site:octave.org" args))
(defalias 'fsi-google-octave 'fsi-ggo)

;;

(defun fsi-ggw (&rest args)
  "Google on the emacswiki site."
  (fsi-google-with-options "site:en.wikipedia.org" args))
(defalias 'fsi-google-wikipedia 'fsi-ggw)


(defun fsi-ggi (&rest args)
  "Google on IMDB"
  ;; (fsi-google-with-options "2" (cons "imdb title" args)))
  (fsi-google-with-options "imdb title" args))
(defalias 'fsi-google-imdb 'fsi-ggi)



(defun fsi-cp (name dest)
  (erbn-log 'cp name dest)
  (erbn-untrash ;; unlog+untrash
   (let* ((exn (erbbdb-get-exact-notes name))
	  (notes (and (stringp exn) (erbn-read exn))))
     (unless notes
       (error "[cp ] No such term %s" name))
     (when (erbbdb-get-exact-notes dest)
       (error "[cp] %S already exists.  Use merge" dest))
     (fsi-set-term dest notes)
     (format "Copied notes of %S to %S" name dest))))


(defun fsi-notes (name)
  "Internal. Return the notes as a list.  Else nil"
  (sit-for 0)
  (let ((exnotes (erbbdb-get-exact-notes name)))
    (and (stringp exnotes) (erbn-read exnotes))))




;; (defun fsi-redirect-p (term)
;;   (setq term (format "%s" term))
;;   (let* ((notes (fsi-notes term))
;; 	 (fir (first notes)))
;;     (and (stringp fir) (= (length notes) 1)
;; 	 (equal 0 (string-match "redirect\\b" fir)))))


(defun erbn-chase-redirectsb (curlist)
  "Internal; used by fsi-redirect-info.
Return a chain of redirects. If the chain has only one element, then there's no redirects."
  (let* ((name (car curlist))
	 (prevlist (cdr curlist))
	 (notes (fsi-notes name))
	 (fir (first notes))
	 (ans name) ;; answer
	 (foundp nil)
	 (returnval curlist)
	 (splits (split-string (format "%s" fir)))
	 )
    ;; or could use the new term-redirect-p instead. 
    (when (and (stringp fir)
	       (= (length notes) 1)
	       (string= (first splits) "redirect")
	       ;; (equal 0 (string-match "^[ \t\n\v\f]*redirect\\b" fir))
	       ;; or, can also use [[:space:]]
	       )
      (let* ((splits (split-string fir))
	     (sec (second splits)))
	(when (stringp sec) 
	  (when (member sec prevlist)
	    (error "Loop found in erbn-chase-redirectsb. Please use ,dl and remove redirect-loops. I'm now on fire. HELP! repeated term: %S in list %S" sec
		   (mapconcat 'identity (reverse curlist) ":" )))
	  (setf ans sec foundp t)
	  (sit-for 0)
	  (setq returnval (erbn-chase-redirectsb (cons ans curlist))))))
    ;; name has no redirect. 
    returnval))

	 


(defun fsi-redirect-info (initterm)
  "Either return nil if not a redirect, else return a bunch of useful info.
The useful info will be as follows: (a) the final destination term. (b) a string indicator for useful info. (b) redirect length. (4) the entire redirect chain.  (5) notes for the final destination.
In case of loops, we will error out, so please wrap this around (ignore-errors ... ) if needed.
Usually, you want to do something like (seq-let (redirectp finalterm indicator redlist bigindicator smallindicator) (fsi-redirect-info) &rest body). And, if redlist is empty, then you know there is simply no redirect. "
  (let* (
	 (initterm (format "%s" initterm))
	 (redirectlist (erbn-chase-redirectsb (list initterm)))
	 (finalterm (car redirectlist))
	 (termlen (length redirectlist))
	 (chainlen (- termlen 1))
	 (multiplep (> chainlen 1))
	 ;; (finalnotes (notes finalterm))
	 (redirectp (> chainlen 0))
	 (indicator (concat initterm
			    (if multiplep "=>" "->")
			    finalterm))
	 (smallindicator (if multiplep "=>" "->"))
	 (bigindicator (mapconcat 'identity  (reverse redirectlist) ":"))
	 )
    (if redirectp
	(list redirectp finalterm indicator redirectlist bigindicator smallindicator)
      (list nil initterm "" (list initterm) "" ""))))



(defun fsi-term-redirect-p (term)
  "PLEASE USE THE NEW FSI-REDIRECT-INFO INSTEAD."
  (setq term (format "%s" term))
  (let ((nn (fsi-notes term)))
    (and (= (length nn) 1)
	 (string-match "^redirect[ \t]+" (first nn)))))

(defun fsi-merge-generic (&optional name dest &rest args)
  ;; no erbn-log here. If user does call this term directly, the subcomponents will log anyway.
  (when (or (fsi-term-redirect-p name)
	  (fsi-term-redirect-p dest))
      (error "[merge generic] At least one of source and target is already a redirect!"))
  (unless (and name dest (not args))
    (error (format "[merge generic] Syntax: %s merge TERM1 TERM2" erbn-char)))
  (setq name (format "%s" name))
  (setq dest (format "%s" dest))
  (when (string= (downcase name) (downcase dest))
    (error "Cannot merge something into itself."))
  (let ((notes (fsi-notes name))
	(destnotes (fsi-notes dest))
	)
    (unless notes (error "[merge generic] No such term %S" name))
    (unless destnotes
      (error "[merge generic] No such term %S.  Use mv" dest))
    (setq name (fsi-correct-entry name))
    (setq dest (fsi-correct-entry dest))
    (erbot-working
     (mapcar
      '(lambda (arg)
	 (fsi-set-also dest arg))
      notes)
     (fsi-forget name "all"))
    (when erbn-merge-redirect-p
      (erbot-working
       (fsi-set-term name (format "redirect %s" dest))))
    (erbbdb-save)
    (if erbn-merge-redirect-p
	(format "Merged %S into %S, redirected %S to %S" name dest
		name dest)
      (format "Merged %S into %S" name dest))))

(defun fsi-merge-redirect (&rest args)
  (erbn-log 'merge args)
  (erbn-unlog
   (let ((erbn-merge-redirect-p t))
     (apply 'fsi-merge-generic args))))


(defalias 'fsi-merge 'fsi-merge-redirect)

(defun fsi-merge-noredirect (&rest args)
  (let ((erbn-merge-redirect-p nil))
    (apply 'fsi-merge-generic args)))

(defalias 'fsi-Merge 'fsi-merge-noredirect)


(defun fsi-mv (&optional name dest &rest args)
  "Rename NAME to DEST.
Do not confuse this function with fs-rearrange which rearranges the order of notes WITHIN a given term. "
  (erbn-log 'mv name dest args)
  (erbn-untrash ;; == unlog + untrash
   (when (or args (not (and name dest)))
     (error (format "[mv] Format: %s mv foo bar" erbn-char)))
   (setq name (format "%s" name))
   (setq dest (format "%s" dest))
   (cond
    ((string= (downcase name) (downcase dest))
     (fsi-mv-change-case name dest))
    (t
     (setq name (fsi-correct-entry name))
     (erbot-working (fsi-cp name dest))
     (erbot-working (fsi-forget name "all"))
     (erbbdb-save)
     (format "Renamed the term %S to %S" name dest)))))

(defalias 'fsi-rename 'fsi-mv)

(defun fsi-mv-change-case (name dest)
  (erbn-log 'mv-change-case name dest)
  (erbn-untrash ;; == unlog + untrash.
   (when
       (let ((bbdb-case-fold-search nil))
	 (erbbdb-get-exact-name dest))
     (error "[mv change case] Destination %S already seems to exist" dest))
   (let ((tmp (format "TMPMV-%S" (random 1000))))
     (erbot-working
      (ignore-errors (fsi-forget tmp))
      (fsi-mv name tmp)
      (fsi-mv tmp dest))
     (erbbdb-save)
     (format "Readjusted case from %S to %S" name dest))))

(defun fsi-swap (name dest)
  "Swap the notes of two terms."
  (erbn-log 'swap name dest)
  (erbn-untrash ;; == unlog + notrash
   (setq name (format "%s" name))
   (setq dest (format "%s" dest))
   (unless
       (let ((bbdb-case-fold-search nil))
	 (erbbdb-get-exact-name dest))
     (error "[mv change case] Destination %S does not exist." dest))
   (unless
       (let ((bbdb-case-fold-search nil))
	 (erbbdb-get-exact-name name))
     (error "[mv change case] Source term %S does not exist." name))
   (when (string= (downcase name) (downcase dest))
     (error "[mv change case] Can't swap term with itself. "))
   (let ((tmp (format "TMPMV-%S" (random 1000))))
     (erbot-working
      (ignore-errors (fsi-forget tmp))
      (fsi-mv name tmp)
      (fsi-mv dest name)
      (fsi-mv tmp dest))
     (erbbdb-save)
     (format "Readjusted case from %S to %S" name dest))))
  


(defun fsi-rearrange-from-english-internal (msg)
  ;; ther's no need to log anything here. this is just a parser function.
  ;; (erbn-log 'rearrange-from-english-internal msg)
  (progn
   (catch 'erbnocmd-tag-foo
     (unless (equal (length msg) 3)
       (throw 'erbnocmd-tag-foo
	      `(fs-error (format "Syntax: %s N->M in TERM" erbn-char))))
     (unless (equal (downcase (format "%s" (second msg))) "in")
       (throw 'erbnocmd-tag-foo
	      `(fs-error (format "Syntax: %s N->M in TERM" erbn-char))))
     (let (term
	   fromto
	   lenfromto
	   )
       (setq term (third msg))
       (setq fromto
	     (split-string (first msg) "->"))
       (setq lenfromto (length fromto))
       (unless (= lenfromto 2)
	 (throw 'erbnocmd-tag-foo
		`(fs-error (format "Syntax: %s N->M in TERM" erbn-char))))
       `(fs-rearrange ,(first fromto) ,(second fromto) ,term)))))




(defun erbn-replace-string-from-english-internal (msg)
  "This is just a parser function. It doesn't actually replace the string.
Parse the input english message to return an elisp equivalent.
MSG here is a list which needs to be combined.  "
  ;; (erbn-log 'replace-string-from-english-internal msg)
  (progn
   (let*
       (
	;; original length
	(leno (length msg))
	;; remaining msg
	(remmsg msg)
	(remlen leno)
	las
	number
	remengmsg
	remenglen
	revengmsg
	splitloc
	from
	to
	term
	(ans nil)
	(termcheckp nil)
	fcn
	sr
	)
     (catch 'erbnocmd-repl-error

       (unless (and (>= leno 3)
		    (equal 0 (string-match "\\(s\\|r\\)/" (first remmsg))))
	 (throw 'erbnocmd-repl-error
		`(fs-error
		  "Format: s/foo.../bar..../ in TERM &optional N.   r/../../.. for regexps.")))
       (setq sr
	     (if (equal 0 (string-match "s" (first remmsg))) "s" "r"))
       (setq las (first (last remmsg)))
       (setq number (and (stringp las) (erbn-read las)))
       (if (or (numberp number)
	       (equal 0 (string-match
			 "all"
			 (downcase (format "%s" number)))))
	   (setq remmsg (subseq remmsg 0 (- remlen 1)))
	 (progn
	   (setq termcheckp t number nil)))

       ;; next comes the term
       (setq remlen (length remmsg))
       (setq term (first (last remmsg)))
       (setq remmsg (subseq remmsg 0 (- remlen 1)))

       (when termcheckp
	 (let* ((exn (erbbdb-get-exact-notes term))
		(notes (and (stringp exn) (erbn-read exn)))
		(len (length notes)))
	   (if (> len 1)
	       (throw 'erbnocmd-repl-error
		      `(fs-error "Which note number? %s/foo/bar in TERM NUMBER" , sr
				 ))
	     (setq number 0))))

       ;; now the "in"
       (setq remlen (length remmsg))
       (setq las (first (last remmsg)))
       (unless
	   (string= "in" (downcase (format "%s" las)))
	 (throw 'erbnocmd-repl-error
		`(fs-error
		  "Format: %s/foo.../bar..../ in TERM &optional NUMBER"
		  ,sr ))
	 )

       (setq remmsg (subseq remmsg 0 (- remlen 1)))
       (setq remlen (length remmsg))
       (setq remengmsg (mapconcat 'identity remmsg " "))

       ;; remove trailing whitespace
       ;; no need to check for length since we know msg stars with s/
       (while
	   (member
	    (aref remengmsg (- (length remengmsg) 1))
	    '(9 ;; tab
	      32 ;; space
	      10 ;; newline
	      ))
	 (setq remengmsg (subseq remengmsg 0 (- (length remengmsg) 1))))
       ;; remove one trailing /
       ;; no need to check for length since we know msg stars with s/
       (setq remenglen (length remengmsg))
       (when (equal
	      (aref
	       remengmsg (- (length remengmsg) 1))
	      47)
	 (setq remengmsg (subseq remengmsg 0 (- (length remengmsg) 1))))

       (setq remenglen (length remengmsg))
       (unless (> (length remengmsg) 2)
	 (throw 'erbnocmd-repl-error
		`(fs-error
		  "Format: %s/foo.../bar..../ in TERM &optional N"
		  ,sr
		  ))

	 )
       ;; this should take care of almost anything imaginable.
       ;; one can still construct "missing" cases but one should just use
       ;; lisp for that.
       ;; remove the s/
       (if (equal 0 (string-match "s" remengmsg))
	   (setq fcn 'fsi-replace-string-in-term)
	 (setq fcn 'fsi-replace-regexp-in-term))
       (setq remengmsg (subseq remengmsg 2))
       ;; now find the last single /
       (with-temp-buffer
	 (insert remengmsg)
	 (goto-char (point-max))
	 (setq splitloc
	       (search-backward-regexp  "[^/]/\\([^/]\\|$\\)" nil t)))
       (unless splitloc
	 (throw 'erbnocmd-repl-error
		`(fs-error
		  "Format: %s/foo.../bar..../ in TERM &optional N"
		  ,sr
		  )))
       (setq from (substring remengmsg 0 splitloc))
       (setq to (substring remengmsg (+ splitloc 1)))
       (when (string= from "")
	 (throw 'erbnocmd-repl-error
		`(fs-error "Replacement string must have nonzero size..")))
       ;; singlify the double /'s.
       (setq from
	     (erbutils-replace-regexp-in-string "//" "/" from))
       (setq to
	     (erbutils-replace-regexp-in-string "//" "/" to))
       `(,fcn ,from ,to ,term ,(format "%s" number))))))



(defun fsi-replace-string-in-term (&optional from to term number)
   "Replace FROM by TO in TERM's note #<NUMBER>. NOT an alias for emacs' usual (replace-string)"
  ;; log
  (erbn-log 'replace-string from to term number)
  (let (meta notes okp
	     (syn
	      (concat
	       "Syntax: ,s/string/replacement/ in Term [number or all] for strings.  ,r for replacing regexps."
	       "\n"
	       " Fslisp syntax: (replace-string \"string\" \"replacement\" \"Term\" Num)"
	     )))
    (unless (and from to term)
      (error
       ;; (format "[replace string] Syntax: %s s/foo.../bar/ in Term [num or all] for strings.   r/../../.. for regexps." erbn-char)
       syn
       ))
    
    ;; trash:
    (progn
      (setq meta (list 'replace-string from to term number))
      (setq notes (fsi-notes (format "%s" term)))
      (erbn-trash meta (list 'term term) notes))
    
    (erbn-untrash ;; == unlog + untrash, erbnocmd-iterate-internal will generate trash!
     (let* (
	    (results (erbot-working
		      (erbnocmd-iterate-internal
		       (or (erbbdb-get-exact-name term ) term)
		       number 'erbutils-replace-string-in-string
		       from to nil)))
	    (infostring (erbn-compare-two-notes-infostring (first results) (second results))))
       (erbbdb-save)
       (setq okp (not (string-match "^NONE" infostring)))       
       (if okp
	   (format "%s when replacing *string* %S with %S for term %S." infostring from to term)
	 (format "%s when replacing *string* %S with %S for term %S. \n %s" infostring from to term syn)
       )))))

(defalias 'fsi-replace-string 'fsi-replace-string-in-term)


(defun fsi-replace-regexp-in-term (&optional from to term number delimited
				    fixedcase literal subexp)
  "TODO: implemenet fixedcase, literal, subexp."
  (erbn-log 'replace-regexp from to term number delimited fixedcase literal subexp)

  ;; trash:
  (let ((meta (list 'replace-regexp from to term number))
	(notes (fsi-notes (format "%s" term)))

	(syn (concat
	      "Syntax: ,r/regexp/replacement/ in Term [NUMBER or ALL] for regexp.  ,s for strings."
	      "\n"
	      " Fslisp syntax: (replace-regexp \"regexp \" \"replacement\" \"Term\" NUM)" ))
	okp
	)
    (erbn-trash meta (list 'term term) notes)
    (erbn-untrash
     (unless (and from to term)
       (error (format syn)))
     (setf from (format "%s" from))
     (setf to (format "%s" to))
     ;; replace-regex-in-string is dangerous otherwise!
     (let* (
	    (results
	     (erbot-working (erbnocmd-iterate-internal term number 'erbutils-replace-regexp-in-string from to
						       nil)))
	    (infostring (erbn-compare-two-notes-infostring (first results) (second results))))
       (erbbdb-save)
       (setq okp (not (string-match "^NONE" infostring)))
       (if okp
	   (format "%s when replacing regexp %S with %S for term %S." infostring from to term)
	 (format "%s when replacing regexp %S with %S for term %S. \n %s" infostring from to term syn)
	 )))))

(defalias 'fsi-replace-regexp 'fsi-replace-regexp-in-term)



(defun erbn-compare-two-notes-infostring (notes1 notes2)
  (let
      ((numchanges (erbn-compare-two-notes notes1 notes2)))
    (cond ((not (numberp numchanges))
	   "[Huh. Debug this]")
	  ((zerop numchanges)
	   (format "NONE of the %d notes were changed" (length notes1)))
	  (t (format "Changed %d (of %d) notes" numchanges
		     (length notes1))))))


(defun erbn-compare-two-notes (notes1 notes2)
  "Either return the number of changes, or, in case of parse error, a nil."
  (assert (and (listp notes1) (listp notes2)))
  (cond
   ((not (= (length notes1) (length notes2)))
    nil)
   (t
    (let ((num 0))
      (loop for a1 in notes1 for a2 in notes2 do
	    (when (not (equal a1 a2))
	      (incf num)))
      num))))

(defun erbnocmd-iterate-internal (term number function
				       &rest arglist)

  " Perform FUNCTION on the NUMBERth note of TERM.
If NUMBER is not nil, the replacement is done for each entry in
the TERM. The function uses the term as its third argument.
Meant for use by fs-replace-regexp etc.

The last entry of ARGLIST is assumed to be itself a list of arguments,
let's call it lastlist.  Let the other entries of arglist be called
initargs.  Then the function is applied as (function @initargs string
@arglist).  Where the string is the string gotten from the TERM. 
We return (list oldnotes newnotes)
"

  (setq number (format "%s" number))
  (let*
      (
       (exactnotes (erbbdb-get-exact-notes term))
       (notes (and (stringp exactnotes) (erbn-read exactnotes)))
       (len (length notes))
       newnotes
       newnote
       (lenargs (length arglist))
       (initargs (subseq arglist 0 (- lenargs 1)))
       (finargs (first (last arglist)))
       (numnum (erbn-read number))
       )
    (when (and (null number) (= len 1)) (setq number 0))
    (unless exactnotes (error "[eii] No such term: %S" term))
    (cond
     ((string= "all" (downcase number))
      (setq newnotes
	    (mapcar
	     (lambda (thisentry)
	       (apply function (append initargs (list thisentry)
				       finargs)))
	     notes)))
     ((or (not (numberp numnum))
	  (< numnum 0)
	  (>= numnum len))
      (error "[eii] Number should be \"all\" or lie within %s and %s, given was: %s"
	     0 (- len 1) numnum))
     (t
      (setq newnotes
	    (append
	     (subseq notes 0 numnum)
	     (list
	      (apply function (append initargs
				      (list (nth numnum notes))
				      finargs)))
	     (subseq notes (+ numnum  1) len)))))
    (fsi-forget term "all")
    (fsi-set-term term newnotes)
    (list notes newnotes)
    ))




(defun fsi-info-emacs (&optional regexp)
  (erbn-info-file "emacs" regexp))

(defun fsi-info-elisp (&optional regexp)
  (erbn-info-file "elisp" regexp))

(defun fsi-info-efaq (&optional regexp)
  (erbn-info-file "efaq" regexp))

(defun fsi-info-eintr (&optional regexp)
  (erbn-info-file "eintr" regexp))

(defun fsi-info (&optional regexp &rest ignore)
  (unless regexp (format (error "[info] Syntax: %s(info \"<regexp>\")" erbn-char)))
  (or
   (ignore-errors (fsi-info-emacs regexp))
   (ignore-errors (fsi-info-elisp regexp))
   (ignore-errors (fsi-info-efaq regexp))
   (ignore-errors (fsi-info-eintr regexp))
   (error "No matching info  found in Emacs manual, elisp manual, Emacs FAQ or Elisp intro")))





(defun erbn-info-file (&optional infofile regexp)
  (unless regexp
    (error (format "Syntax: %s info-node nodename REGEXP" erbn-char)))
  (unless (stringp regexp) (setq regexp (format "%s" regexp)))


  (unless infofile (error (format "Syntax: %s info info-file REGEXP"
			      erbn-char)))
  (unless (stringp infofile) (setq infofile (format "%s" infofile)))

  (fsi-search-check-or-break regexp)
  (cond
   ((ignore-errors (Info-goto-node
		    (concat "(" infofile ")" regexp))
		   t )
    (concat "Press C-x C-e after: (info \"("
	    infofile ")" Info-current-node
	    "\")")
    )
   ((progn
      (ignore-errors
	(Info-goto-node (concat "(" infofile ")"))
	(Info-top-node)
	(Info-search regexp) t
	))
    (concat "Press C-x C-e after: (info  \"("
	    infofile
	    ")" Info-current-node
	    "\")"))
   (t (error "Regexp or infofile not found in the file"))))


(defun fsi-locate-library (&optional arg &rest ignore)
  "fs- Locate library. REST WILL be ignored."
  (let*
      ((loc1 (format "%s" (fsi-locate-library1 arg)))
       web)
    (when (string-match "/home/fsbot/emacs/erbot/erbot/" loc1)
      (setf web (replace-match "http://gnufans.net/~fsbot/erbot/erbot/" nil nil loc1)))
    (if web
	(concat loc1 " ;  Online: " web)
      loc1)))
      


(defun fsi-locate-library1 (&optional arg &rest ignore)
  "fs- Locate library. REST WILL be ignored."
  
  (unless arg (error "Syntax: %s locate-library LIB" erbn-char))
  (unless (stringp arg)
    (setq arg (format "%s" arg)))
  (locate-library arg))      

	

(defun fsi-avg (&rest numbers)
  (cond
   ((null numbers) 'NaN)
   (t (fsi-// (apply '+ numbers)
	       (length numbers)))))


(defun fsi-dictfull (&optional word &rest ignore)
  ;; (require 'connection)
  (require 'dictionary)
  (unless word (error (format "Syntax: %s d[ict] word" erbn-char)))
  (unless (stringp word) (setq word (format "%s" word)))
  (fsi-unfill-string (fsi-dictionary-search word)))

(defalias 'fsi-dict-full 'fsi-dictfull)

(defun fsi-dict-url (&optional word &rest ignore)
  "Abbreviated dictionary search pointer. With thanks to twb for the idea on 20150323. -deego."
  (format "https://en.wiktionary.org/wiki/Special:Search/%s    ..[For full definition, use ,dictfull]" word))

(defalias 'fsi-dict 'fsi-dict-url)

(defalias 'fsi-d 'fsi-dump)

(defalias 'fsi-dictionary 'fsi-dict)

(defun fsi-dictionary-search (word)
  "lispy.. not for interface.  Called from fsi-dict."
  ;; (error "Disabled because too verbose. Please use ,dict instead. - deego 20241008.")
  (ignore-errors (kill-buffer "*Dictionary buffer*"))
  (unless (stringp word) (setq word (format "%s" word)))
  (message "fsi-dictionary-search")
  (concat 
   "[Use ,dict for a non-spammy dictionary url pointer!]  "
   (substring  
    (with-timeout
	(fsi-internal-dictionary-time "Dictionary--TimedOut")
      (save-window-excursion
	(dictionary-search word)
	(switch-to-buffer "*Dictionary*")
	(goto-line 3)
	(buffer-substring-no-properties (point) (point-max))))
    1)  ;; remove the beginning \n so that [Use ...] as well as the definition occur on the same line, saving an extra \n.
   ))




;;;###autoload
(defun fsi-// (&rest args)
  "My DWIM version of /.  From my .emacs. (c. oct. 2000)
Tries to dtrt and return a sensible dwim answer.
Example: (// 4 2) is 2, an integer.
And, (// 3 2) is 1.5, a float.

This usues equal atm, not equalp. The last time i checked , equalp seemed to work as well.. "
  (let ((aa (apply '/ args)))
    (if (equal (car args) (apply '* aa (cdr args)))
	aa
      (apply '/ (cons (float (car args)) (cdr args))))))


(defun fsi-channel-members-all ()
  (cond
   ;; for earlier ERC.
   ((boundp 'channel-members) channel-members)
   ;; for later CVS versions of ERC.
   (t nil)))

(defun fsi-channel-members (&optional n m &rest args)
  (when (stringp n)
    (setq n (ignore-errors (erbn-read n))))
  (when (stringp m)
    (setq m (ignore-errors (erbn-read m))))
  (unless (integerp n) (setq n 0))
  (unless (integerp m) (setq m nil))
  (subseq (fsi-channel-members-all) n m))


(defun fsi-length-channel-members (&rest args)
  (cond
   ;; for new erc versions
   ;; ((boundp erc-channel-users)
   ((boundp 'erc-channel-users)
    (hash-table-count erc-channel-users))
   (t (length (fsi-channel-members-all)))))


(defalias 'fsi-number-channel-members 'fsi-length-channel-members)



;;; (defun fs-karma (&rest args)
;;;   (let ((fir (first args)))
;;;     (unless
;;; 	(and
;;; 	 args
;;; 	 fir)
;;;     (error (format "Syntax: , karma ENTITY")))
;;;     (setq fir (downcase (format "%s" fir)))
;;;     (let ((result (erbkarma fir)))
;;;       (if result
;;; 	  (format "%s's karma is %s" fir result)
;;; 	(format
;;; 	 "No karma defined for %s, use ,ENTITY++ or ,karma-create" fir
;;; 	 )))))

;;; (defvar erbn-karma-pt 10)

;;; (defun fs-karma-increase (&optional arg points &rest ignore)
;;;   (unless arg (error "Syntax: foo++ [&optional NUMBER]"))
;;;   (when (stringp points)
;;;     (setq points (ignore-errors (read points))))
;;;   (unless (and (integerp points)
;;; 	       (<= (abs points) erbn-karma-pt))
;;;     (setq points erbn-karma-pt))
;;;   (setq arg (downcase (format "%s" arg)))
;;;   (erbkarma-increase arg points))

(defun fsi-karma-increase-old (&rest args)
  (if (car args)
      (progn
	;; 20201112 
	;; (ignore-errors (incf (gethash (intern (format "%s" (car args))) erbn-money) 1000))
	(format
	 "Noted, %s.  One %s-point for %s!"
	 nick
	 ;;  (erbutils-random '("brownie" "karma" "wiki" "lisp" "vi" "l33t" "fsbot" "rudy" "M$"))
	 (erbutils-random (fs-notes "karma-list"))
	 (car args))


	     )
    ;;(error "Karma system is currently being reworked. ")
    ""))




;; (defalias 'fsi-karma-decrease 'fsi-karma-increase)
;;  NOTE that 20201112 karma-increase is now a user-function!! 
(defun fsi-karma-decrease (arg &rest args)
  (fsi-flame (format "%s" arg)))


;;; (defun fs-karma-decrease (&optional arg points &rest ignore)
;;;   (unless arg (error "Syntax: foo++ [&optional NUMBER]"))
;;;   (when (stringp points)
;;;     (setq points (ignore-errors (read points))))
;;;   (unless (and (integerp points)
;;; 	       (<= (abs points) erbn-karma-pt))
;;;     (setq points erbn-karma-pt))
;;;   (setq arg (downcase (format "%s" arg)))
;;;   (erbkarma-decrease arg points))



;;; (defun fs-karma (&optional foo)
;;;   (if foo (setq foo (downcase (format "%s" foo))))
;;;   (erbkarma foo))

;;; (defalias 'fs-karma-best 'erbkarma-best)


(defalias 'fsi-ncm 'fsi-length-channel-members)
(defun fsi-superiorp (&rest args)
  (erbutils-random '(t nil)))
(defun fsi-sucksp (&rest args)
  (erbutils-random '(t nil)))
(defun fsi-bugp (&rest args)
  (erbutils-random '(t nil)))


(defun fsi-country (&optional ct)
  (unless ct (error (format "Syntax: %s country NM (example , country jp)" erbn-char)))
  (setq ct (format "%s" ct))
  (let ((addp (and (> (length ct) 1)
		   ;; does not start with .
		   (not (= (aref ct 0) 46)))))
    (if addp (setq ct (concat "." ct))))
  (erbcountry (downcase ct)))



(defun fsi-country-search (&rest names)
  (unless names (error
	      (format "Syntax: %s country-search NM (example , country japa)" erbn-char)))
  (erbcountry-search
   (mapconcat (lambda (arg) (format "%s" arg)) names " ")))


;;; 2003-02-09 T13:40:04-0500 (Sunday)    D. Goel
;; (defun fsi-spook (&rest args)
;;   (with-temp-buffer
;;     (spook)
;;     (goto-char (point-min))
;;     (forward-line 1)
;;     (buffer-substring-no-properties
;;      (progn (beginning-of-line 1) (point))
;;      (progn (end-of-line 1) (point)))))


(defun fsi-explode (&rest args)
  (let ((pieces
	 (erbutils-random '("a thousand" "a million" "a gazillion"
			    "aleph_2")))
	(watch
	 (erbutils-random '("" "You watch as "
			    "You run for cover as "
			    ))))
  (eval
   (erbutils-random
    '((format "%s%s explodes into %s pieces!"
	      watch erbot-nick pieces)
      (format "%s, with botheart broken into %s pieces, has left: \"Goodbye\""
	      erbot-nick pieces)
      (progn
	(fsi-dunnet-command "reset")
	"You have scored 0 out of a possible 90 points."))))))




(defalias 'fsi-die 'fsi-explode)
(defalias 'fsi-die! 'fsi-explode)
(defalias 'fsi-Die! 'fsi-explode)
(defalias 'fsi-Die 'fsi-explode)
(defalias 'fsi-DIE 'fsi-explode)
(defalias 'fsi-leave 'fsi-explode)
(defalias 'fsi-exit 'fsi-explode)
(defalias 'fsi-quit 'fsi-explode)
(defalias 'fsi-shut 'fsi-explode)
(defalias 'fsi-stfu 'fsi-explode)
(defalias 'fsi-STFU 'fsi-explode)



(defun fsi-morse (&rest str)
  (apply 'erbutils-region-to-string 'morse-region str))
(defun fsi-unmorse (&rest str)
  (apply 'erbutils-region-to-string 'unmorse-region str))

(defun fsi-rot13 (&rest str)
  (let (st)
    (cond
     ((= (length str) 1)
      (setq st (format "%s" (first str))))
     (t (setq st (mapconcat
		  (lambda (a) (format "%s" a)) str " "))))
    (erbutils-rot13 st)))

(defun fsi-studlify (&rest s)
  (apply 'erbutils-region-to-string
   (lambda (&rest args)
     (ignore-errors (apply
		     'studlify-region args)))
   s))


(defun fsi-h4x0r (&rest s)
  (require 'h4x0r)
  (funcall
   'h4x0r-string
   (mapconcat
    (lambda (a) (format "%s" a))
    s " ")))


(defalias 'fsi-h4 'fsi-h4x0r)
(defalias 'fsi-haxor 'fsi-h4x0r)
(defalias 'fsi-hax0r 'fsi-h4x0r)
(defalias 'fsi-h4xor 'fsi-h4x0r)

(defalias 'fsi-l33t 'fsi-h4x0r)
(defalias 'fsi-leet 'fsi-h4x0r)

(defalias 'fsi-stud 'fsi-studlify)


(defun fsi-studlify-maybe (&rest args)
  (eval
   (erbutils-random
    '((erbutils-stringify args)
      (apply 'fsi-studlify args))
    fsi-internal-studlify-maybe-weights
    )))



(defun fsi-h4x0r-maybe (&rest args)
  (let*
       ((aa (erbutils-stringify args))
       (bb
	(ignore-errors
	  (eval
	   (erbutils-random
	    '(aa
	      (apply 'fsi-h4x0r args))
	    fsi-internal-h4x0r-maybe-weights
	    )))))
    (or bb aa)))


(defalias 'fsi-stud-maybe 'fsi-studlify-maybe)


(defalias 'fsi-studlify-word 'studlify-word)


(defun fsi-princ (a &rest ignore)
  (princ a))


(defun fsi-pray (&rest args)
  (require 'faith)
  (let ((faith-fill-column 9999))
    (faith-quote)))

(defalias 'fsi-all-hail-emacs 'fsi-pray)
(defalias 'fsi-hail-emacs 'fsi-pray)
(defalias 'fsi-faith 'fsi-pray)

(erbutils-defalias-n 'faith-correct-string 1)

(erbutils-defalias-n 'member 2)

(erbutils-defalias-i '(stringp consp symbolp numberp listp arrayp
			       boundp bufferp commandp consp endp
			       equalp evenp oddp facep fboundp
			       featurep functionp integerp floatp keywordp
			       listp markerp minusp natnump
			       nlistp numberp overlayp plusp rationalp
			       sequencep subrp tailp timerp
			       typep vectorp windowp xemacsp zerop)
		     1)


(erbutils-defalias-i
 '(char-to-string string-to-char string-to-int
		  string-to-number string-to-list
		  string-to-number-with-radix number-to-string
		  pp-to-string int-to-string number-to-string
		  rational-to-string rational-to-float
		  radians-to-degrees degrees-to-radians)
 1)





(defun erbn-shell-test (string &optional substrings)
  "NB: Aliased to fsi-shell-test.
Return t if any of the substrings matches string..  Used to weed
out harmful shell code..

See: http://www.w3.org/Security/faq/wwwsf4.html#CGI-Q7


"
  (assert (stringp strings))

  (unless substrings
    (setq substrings (list " " "<" ">" "-" "`" "$" "=" ";" "&" "'"
			   "\\" "\"" "|" "*" "?" "~" "^" "(" ")" "["
			   "]" "{" "}" "\n" "\r" )))
  (let ((found nil))
    (mapcar (lambda (arg)
	      (assert (stringp arg))
	      (when (string-match (regexp-quote arg) string)
		(setq found t)))
	    substrings)
    found))

(defalias 'fsi-shell-test 'erbn-shell-test)





(defmacro erbn-with-web-page-buffer (site &rest body)
  (let ((buffer (make-symbol "web-buffer")))
    `(progn
       (unless (and (not erbot-paranoid-p)
		    erbn-url-functions-p)
	 (error "erbn-url-functions-p is disabled"))
       (with-timeout (erbn-internal-web-page-time "HTTP time out")
	 (let ((,buffer (url-retrieve-synchronously ,site)))
	   (when (null ,buffer)
	     (error "Invalid URL %s" site))
	   (save-excursion
	     (set-buffer ,buffer)
	     (goto-char (point-min))
	     (prog1
		 (progn
		   ,@body)
	       (kill-buffer ,buffer))))))))

(defun fsi-web-page-title (&optional site &rest args)
  (error "[fwpt] Disabled")
  (unless site (error (format "Syntax: %s web-page-title SITE" erbn-char)))
  (setq site (format "%s" site))
  (erbn-with-web-page-buffer site
    (let* ((case-fold-search t)
           (beg (search-forward "<title>" nil t))
           (end (search-forward "</title>" nil t)))
      (concat "That page title is "
              (if (and beg end)
                  (erbutils-cleanup-whitespace
                   (buffer-substring beg (- end 8)))
                "not available")))))




;;;###autoload
(defun fsi-length-load-history ()
  (interactive)
  (message "%s%s%S"
	   (length load-history)
	   " ..." (mapcar 'car load-history)))


;(defun fsi-load-history ()
;  load-history)
;(defun fsi-load-history ()
;  load-history)

(defalias 'fsi-google: 'fsi-google)



(defconst fsi-pi float-pi)
(defconst fsi-e float-e)
(defconst fsi-euler float-e)
(defconst fsi-emacs-version emacs-version)

(erbutils-defalias-i '(emacs-version gnus-version) 0)


;; the short aliases..
(defalias 'fsi-a 'fsi-apropos)
(defalias 'fsi-da 'fsi-apropos)
(defalias 'fsi-ac 'fsi-apropos-command)
(defalias 'fsi-ad 'fsi-apropos-documentation)
(defalias 'fsi-af 'fsi-apropos-function)
(defalias 'fsi-av 'fsi-apropos-variable)

(defalias 'fsi-c 'fsi-commands)
(defalias 'fsi-dict: 'fsi-dict)

(defalias 'fsi-dl 'fsi-describe-literally)
(defalias 'fsi-doc 'fsi-doctor )
(defalias 'fsi-dkb 'fsi-describe-key-briefly )

(defalias 'fsi-dk 'fsi-describe-key)
(defalias 'fsi-dkf 'fsi-describe-key-and-function)
(defalias 'fsi-dkl 'fsi-describe-key-long)

(defalias 'fsi-lkgg 'fsi-lookup-key-gnus-group)
(defalias 'fsi-dkgg 'fsi-lookup-key-gnus-group)

(defalias 'fsi-dkgs 'fsi-lookup-key-gnus-summary)
(defalias 'fsi-lkgs 'fsi-lookup-key-gnus-summary)

(defalias 'fsi-lkm 'fsi-lookup-key-message)
(defalias 'fsi-lkm 'fsi-lookup-key-message)


(defalias 'fsi-df 'fsi-describe-function )
(defalias 'fsi-cond 'cond)

(defalias 'fsi-if 'if) ;; because macro. 
(defalias 'fsi-unless 'unless) 
(defalias 'fsi-when 'when)

(defalias 'fsi-dfl 'fsi-describe-function-long )
(defalias 'fsi-dv 'fsi-describe-variable )
(defalias 'fsi-ff 'fsi-find-function)
(defalias 'fsi-ffv 'fsi-find-function-verbatim)
(defalias 'fsi-ffo 'fsi-find-function-on-key)
(defalias 'fsi-ffob 'fsi-find-function-on-key-briefly)
(defalias 'fsi-fv 'fsi-find-variable)
(defalias 'fsi-fvb 'fsi-find-variable-briefly)
(defalias 'fsi-? 'fsi-help)
(defalias 'fsi-32 'fsi-help)
(defalias 'fsi-s  'fsi-search)
(defalias 'fsi-sw  'fsi-search-wide)
(defalias 'fsi-sws  'fsi-search-wide-sensitive)
(defalias 'fsi-wi  'fsi-where-is)
(defalias 'fsi-wigg  'fsi-where-is-gnus-group)
(defalias 'fsi-wigs  'fsi-where-is-gnus-summary)
(defalias 'fsi-wim  'fsi-where-is-message)
(defalias 'fsi-dw  'fsi-where-is)
;;(defalias 'fsi-yo 'fsi-hi)

;; basic functions
;; undefun lambda:
;; (defalias 'fsi-lambda 'lambda)  ;; huh?

(erbutils-defalias-i '(length sqrt null atom ) 1) 
(erbutils-defalias-i '(= /= < > <= >= ) 'none)

;; arbitrary n args.
(defalias 'fsi-and 'and)
(defalias 'fsi-or 'or) 


(defalias 'fsi-lart 'fsi-flame)


(erbutils-defalias-i '(equal equalp eql) 2)



(defalias 'fsi-rs 'fsi-replace-string)
(defalias 'fsi-+ '+)
(defalias 'fsi-- '-)
(defalias 'fsi-* '*)
(defalias 'fsi-/ '/)
(defalias 'fsi-less 'fsi-more)

(defalias 'fsi-list 'list)

(erbutils-defalias-i
 '(car erbcountry cdr first second third fourth fifth sixth sevent eighth ningth tenth
       last  exp regexp-quote identity 
       )
 1)

(erbutils-defalias-i '(expt) 2)

(erbutils-defalias-i '(cons ceiling ceiling* count-lines) 2)
(defalias 'fsi-append 'append) 
(erbutils-defalias-i '(subseq) 3)

(defalias 'fsi-llh 'fsi-length-load-history)
(defalias 'fsi-error 'erbutils-error)

(defun fsi-random (&optional n &rest ig)
  (assert
   (or (null n) (numberp n)))
  (random n))

(defun fsi-remove (elt seq)
  (unless (or (stringp seq) (listp seq))
    (error "I REMOVE (not delete) elements ONLY from lists or strings! no side effects! To remove elements from terms, use ,forget"))
  (remove elt seq))



(defalias 'fsi-delete 'fsi-remove)

(defalias 'fsi-replace-regexp-in-string 'erbutils-replace-regexp-in-string)
(defalias 'fsi-match-string 'match-string-no-properties)
(erbutils-defalias-i '(match-beginning match-end) 1)

;; (defalias 'fsi-replace-match 'replace-match) ;; NO!


(defalias 'fsi-format 'format)

(erbutils-defalias-i '(format-time-string) 3)

(erbutils-defalias-i '(number-to-string) 1)

(erbutils-defalias-i '(split-string) 2)
(defalias 'fsi-rm 'fsi-forget)
(defalias 'fsi-progn 'progn)
(defalias 'fsi-ignore-errors 'ignore-errors)  ;; macro.
(defalias 'fsi-lcm 'lcm)
(defalias 'fsi-let 'let) 
(defalias 'fsi-let* 'let*)
(defalias 'fsi-ll 'fsi-locate-library)
(defalias 'fsi-gcd 'gcd)
(defalias 'fsi-gd 'fsi-google-deego)

(defalias 'fsi-ge 'fsi-gge)
;; (defalias 'fsi-gs 'fsi-google-sl4)

(defalias 'fsi-gw 'fsi-google-wikipedia)
(defalias 'fsi-gi 'fsi-google-imdb)
(defalias 'fsi-gwe 'fsi-google-wikipedia-english)

(defalias 'fsi-gnufans 'fsi-google-gnufans-net)
(defun fsi-mark (&rest ig)
  (mark 'force))

(erbutils-defalias-i '(point) 0)
(erbutils-defalias-i '(sin cos tan atan asin acos) 1)
(erbutils-defalias-i '(floor floor* mod* round round*) 2)


(defalias 'fsi-min 'min)
(defalias 'fsi-max 'max)

(erbutils-defalias-i '(sleep-for sit-for) 1)

(defun fsi-sleep-for (secs &rest ig)
  (assert (numberp secs))
  (sleep-for (min 3 secs)))

(defun fsi-sit-for (secs &rest ig)
  (assert (numberp secs))
  (sit-for (min 3 secs)))




  


;; (defalias 'fsi-string 'string)
(defun fsi-string (&rest chars)
  (when (> (length chars) 9999)
    (error "Too many args to 'string will crash emacs27, and therefore, fsbot. Please make a smaller string."))
  (apply 'string chars))


(erbutils-defalias-i
 '(string-as-multibyte string-bytes string-make-multibyte string-make-unibyte
		       string-to-char string-to-list string-to-vector string-width 
		       )
 1)

(defun fsi-symbol-file (sym &rest ignore)
  (assert (symbolp sym))
  (symbol-file sym))

(erbutils-defalias-i '(string-equal string-lessp string-to-number) 2)


(defalias 'fsi-tanh 'fsi-oct-tanh)

;; variables.
(erbutils-defalias-i
 '(timezone-world-timezones 
   timezone-months-assoc))


(erbutils-defalias-i
 '(truncate truncate*) 2)

(erbutils-defalias-i '(erc-version erc-cmd-SV) 0)

(defalias 'fsi-sv 'fsi-erc-cmd-SV)
(defalias 'fsi-stringify 'erbutils-stringify)
(defalias 'fsi-stringifyb 'erbutils-stringifyb)
(defalias 'fsi-stringifyc 'erbutils-stringifyc)
(defalias 'fsi-stringifyd 'erbutils-stringifyd)

;; (defalias 'fsi-while 'while)

;;;====================================================

;;;====================================================
;; ERRORS:

(defun fsi-load-library (&rest args)
  (error "%s" "[load library] Use ,(require 'FEATURE)"))

(defalias 'fsi-load 'fsi-load-library)
(defalias 'fsi-load-file 'fsi-load-library)



;; cl-extra.el

(erbutils-defalias-i '(equalp mod rem rem*) 2)
(erbutils-defalias-i '(isqrt) 1)


(defun fsi-geek-code ()
  (require 'geek)
  (let ((s (geek-code)))
    (if (> (length s) 70)
	(substring s 0 70)
      s)))


(erbutils-defalias-i
 '(signum) 1)

(erbutils-defalias-v
 '(
   most-positive-float most-negative-float
		       least-positive-float least-negative-float
		       least-positive-normalized-float least-negative-normalized-float
		       float-epsilon float-negative-epsilon))

(erbutils-defalias-i '(copy-tree) 1)


;; oct.el

(ignore-errors (require 'oct))

(defalias 'fsi-oct-add 'oct-add)
(erbutils-defalias-i
 '(
   oct-zeros oct-ones oct-sum oct-size oct-\.*
	     oct-add oct-corr
	     )
 2)

(erbutils-defalias-i 
 '(oct-rows oct-columns
	    oct-complement  oct-sumsq oct-mean oct-sqrt oct-std oct-tanh oct-atanh)
 
 1)

(erbutils-defalias-i '(lsh) 2)

;; files.el
(erbutils-defalias-v
 '(auto-mode-alist interpreter-mode-alist
		   directory-abbrev-alist))


(erbutils-defalias-v '(load-history))

;; 20180603 NOTE that as of emacs26, assoc's 3rd argument will lead to an exploit!
(erbutils-defalias-i '(eq) 2)

;; (erbutils-defalias-i '(assoc) 2)
;; notice that we used two args above. Nonetheless, make it explicit below that we do NOT allow testfcn.
(defun fsi-assoc (key alist &rest ignore)
  (assoc key alist))



(defalias 'fsi-message 'fsi-format)

(erbutils-defalias-i '(faith-quote) 0) 
(erbutils-defalias-i '(zerop) 1)
;;(erbutils-defalias-i '(buffer-substring))

;; is this safe? 
;; (erbutils-defalias-i '(buffer-substring-no-properties))
;;(erbutils-defalias-i '(buffer-string))

;; We define it to be no-properties, else people can (setq foo
;; (buffer-string)).. and cause a huge uservariables file..



(erbutils-defalias-i
 '(featurep) 1)
(erbutils-defalias-v '(features))

(erbutils-defalias-v
 '(minor-mode-alist minor-mode-map-alist
		    minor-mode-overriding-map-alist))
(erbutils-defalias-v '(major-mode))

;; from gnus-group.el

(erbutils-defalias-vars '(gnus-group-mode-map))
(erbutils-defalias-vars '(gnus-summary-mode-map))
(erbutils-defalias-vars '(message-mode-map))
(erbutils-defalias-vars '(text-mode-map))
(erbutils-defalias-vars '(emacs-lisp-mode-map))
(erbutils-defalias-vars '(lisp-mode-map))

(erbutils-defalias-i '(boundp fboundp) 1)

(erbutils-defalias-i '(%) 2)
(erbutils-defalias-i '(abs) 2)

(erbutils-defalias-i '(cdr cddr car cadr cdar rest) 1)

(when (ignore-errors (require 'units))
  (erbutils-defalias-v '(units-version units-dat-file))
  ;; units-s-to-n
  ;; units-prefix-convert
  ;; units-si-prefix-list
  ;; units-si-short-prefix-list
  ;; units-convert-1 units-convert)))
  )


(defun erbn-mark-dead (&rest ignore)
  (let ((ni (format "%s" erbn-nick)))
    (unless (string= ni "nil")
      (add-to-list 'erbn-nicks-dead (format "%s" erbn-nick)))))



;; allow people to mark themselves dead :-)
(defalias 'fsi-mark-dead 'erbn-mark-dead)

(defun erbn-unmark-dead (nick)
  (setq erbn-nicks-dead  (remove (format "%s" nick) erbn-nicks-dead)))



(defun erbn-dead-check (&rest ignore)
  "FSI ALIAS!"
  (when (fsi-dead-p erbn-nick)
    (error "[dead check] I see dead people!
                     .... (but I don't talk to them!)")))

(defalias 'fsi-dead-check 'erbn-dead-check)

(defun erbn-dead-p (&optional nick)
  "FSI ALIAS"
  (unless nick (setq nick erbn-nick))
  (member (format "%s" nick) erbn-nicks-dead))

(defalias 'fsi-dead-p 'erbn-dead-p)



(defun fsi-give (&optional nini &rest stuff)
  (unless nini (setq nini "self"))
  (when (string= "me" nini)
    (setq nini nick))
  (unless stuff (setq stuff '("a" "beer")))
  (format "/me gives %s %s"
	  nini
	  (mapconcat
	   (lambda (arg) (format "%s" arg))
	   stuff " ")))


(defalias 'fsi-hand 'fsi-give)

(erbutils-defalias-v
 '(
   flame-sentence ;; is a var. 
   flame-sentence-loop ;; is a var.
   ) )

(defalias 'fsi-flatten 'erbutils-flatten)




(progn
  (erbutils-defalias-i '(log) 2)
  (erbutils-defalias-i '(logb) 1)
  (erbutils-defalias-i '(log10) 1)
  )

;; 20170426  these functions seem safe. 
(erbutils-defalias-i
 '(
   regexp-opt) 1)




(when (and 
       (not erbot-paranoid-p)
       erbot-kbd-p)
  (defun fsi-kbd (key &rest ig)
    (assert (stringp key))
    (kbd key)))

(defconst fs-t t
  "Sandboxer helper.
When we sandbox a lisp expression, t remains t, so this is
not needed.
However, inside macros like (cond (t....)), t becomes fs-t because
it occurs in an unusual place.  this const should take care of it..
Of course, this also opens up the bot to some FUN user abuse, when they
setq fs-t to nil :-) ")


(defconst fs-nil nil
  "See the doc of fs-t ")


(defun fsi-revive (&optional name &rest ignore)
  (unless name (error "no one to revive"))
  (setq name (format "%s" name))
  (let (ansstr)
    (setq ansstr
	  (cond
	   ((string= name nick)
	    (concat "Thou idiot, " nick ", thou canst not revive thyself!"))
	   (t (concat
	       "/me sprinkles some "
	       (erbutils-random
		'("clear" "murky" "boiling" "dark" "holy" "smelly"))
	       " potion on "
	       (format "%s" name)
	       " and utters some prayers.  "
	       (erbutils-random
		(list
		 (format "%s wakes up" name)
		 "Nothing happens."
		 (format "%s wakes up, all refreshed. " name)
		 (format "%s wakes up, all confused. " name)
		 ))))))
    (when (string-match "wakes up" ansstr)
      (erbn-unmark-dead name))
    ansstr))

;; this may be unsafe, remove it:
;; (defalias 'fs-sandbox-quoted 'erblisp-sandbox-quoted)
;; (defalias 'fs-sandbox-quoted-maybe 'erblisp-sandbox-quoted-maybe)
;; (defalias 'fs-sandbox 'erblisp-sandbox)

;; ^^^ 20160617 un-provide macroexpand. is that safe?  It will lead to execution of whatever code the macro has, right? 
;;See, for example <twb> fsbot: (macroexpand (quote (rx (one-or-more word))))
;; note that that leads to execution of rx-to-string
;; (erbutils-defalias-i '(macroexpand))



;;"/usr/share/emacs/21.2/lisp/emacs-lisp/pp.el"
(erbutils-defalias-i
 '(
   pp-to-string
   ;; pp pp-eval-expression
   ;;pp-eval-last-sexp))
   ) 1)


(erbutils-defalias-i '(identity) 1)

(defalias 'fsi-string-match 'erbutils-string-match)

(erbutils-defalias-i '(parse-time-string) 1)

(erbutils-defalias-i '(reverse pp) 1)

(defmacro fsi-privmsg (&rest args)
  "This macro is carefully constructed so that one user cannot force a query to another user. "
  `(cond
    ;; This can occur when you are requesting a parse..
    ((null erbn-nick)
     (progn ,@args))
    (t
     (progn
       (setq erbn-tgt erbn-nick)
       ;; If there isn't already a buffer, create one..
       (erbn-query erbn-nick)
       ,@args))))

(defun erbn-query (qnick)
  (save-excursion (erc-query qnick erbn-buffer)))



(defun fsi-read-or-orig (arg)
  "  If string and can read, read, else return the arg.
Note: Used by fs-describe"
  (cond
   ((stringp arg)
    (condition-case fsi-tmp (erbn-read arg)
      (error arg)))
   (t arg)))


(defun erbn-read-from-string (str  &rest ig)
  "FSI ALIAS. NOTE: user-facing function via fs-read-from-string."
  (assert (stringp str))
  (let (str2)
  (cond
   ((stringp str)
    (setq str2 (copy-sequence str))
    (set-text-properties 0 (length str2) nil str2)
    (read-from-string str))
   (t (error "The bot will only read from strings. ")))))



(defun erbn-read (str &rest ig)
  "FSI ALIAS Like read, but only from strings. NOTE that this is a user-facing function via fs-read"
  (assert (stringp str)) ;; 
  (car (erbn-read-from-string str)))


(defalias 'fsi-read 'erbn-read)
(defalias 'fsi-read-from-string 'erbn-read-from-string)


(erbutils-defalias-i
 '(substring subseq) 3)
(erbutils-defalias-i
 '(subr-arity subrp) 1)
(defalias 'fsi-ignore 'ignore)
(defalias 'fsi-noop 'ignore)

(erbutils-defalias-i '(car cdr cadr cdar caar cddr caaar caadr cadar caddr cdaar cdadr cddar cdddr
			   caaaar caaadr caadar caaddr cadaar cadadr caddar cadddr
			   cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr

			   ) 1)

(erbutils-defalias-i '(elt) 2)

(defun fsi-itemize (result &optional N &rest ig)
  (erbutils-itemize result N))

;; The normal replace-regex-in-string is NOT safe.
(defalias 'fsi-replace-string-in-string 'erbutils-replace-string-in-string)
(defalias 'fsi-string-replace 'erbutils-replace-string-in-string) ;; or simply use the new emacs built in string-replace?


(erbutils-defalias-i '(string> string< string= string-equal) 2)

(defun fsi-concatenate (type &rest seqs)
  (cond
   ((eq type 'string) (apply 'fsi-concat seqs))
   ((eq type 'list) (apply 'fsi-concat seqs))
   (t (error "fsbot's concatenate only works for strings or lists."))))


(defun erbn-task-set (str)
  (setf erbn-task-current (erbn-task-shorten (format "%s" str)))
  )

(defun erbn-task-shorten (str)
  (unless (stringp str) (error "erbn-task-shorten expected a string."))
  (if (< (length str) 16) str
    (concat (substring str 0 8) ".." (substring str -6))))

(defun erbn-lastall-update (currentall tgt)
  (setf erbn-lastall (erbutils-set-assoc erbn-lastall tgt currentall))
  (unless
      (or
       (member erbn-task-current '("dump"))
       fsi-inside-more-p ;; We are inside a more. So, no need to store anything.
       )
    (erbn-lastall-history-update (list
				  (nth 0 currentall) ;;  location.
				  (erbn-task-ensure
				   (nth 5 currentall) ;; task: But, this can sometimes be empty. 
				   (nth 3 currentall))
				   )
				 tgt))
  nil
  )

(defun erbn-task-ensure (task cmd)
  "If task is empty, use the input command to create a task."
  (if (= 0 (length task))
      ;; (erbn-task-shorten cmd)
      (fsi-abbrev-string cmd)
    task
    ))

(defun erbn-lastall-history-update (value tgt)
  (setf erbn-lastall-history
	(erbutils-set-assoc
	 erbn-lastall-history
	 tgt
	 (cons value
	       (cdr (assoc tgt erbn-lastall-history)))))
  nil)


(defun fsi-fulllog-last (&optional n &rest ignore)

  "See a full dump of your previous interaction(s) with the bot. These dumps are maintained in unlisted urls for a small amount of time. 


These dumps are stored in a temporary location, and do not become parts of fulllog unless they pertain to a ,rw change. In that case, they are stored permanently in fulllogs, are public, and ,botlog points to them. 

Note that the ,dump and ,more commands by themselves do not become part of your ,history. 
"
  (setf erbn-task-current "dump")
  (let ((found nil) loc history matches )
    (if (null n) (setf n 0))
    (cond
     ((integerp n)
      ;; (setf locold (nth 1 (assoc erbn-tgt erbn-lastall))) ;; this is how we used to get loc. 
      (setf history (fsi-dump-history-raw))
      (setf loc (first (nth n history)))
      )
     (t
      (setf n (regexp-quote (format "%s" n)))
      (setf history (mapcar 'car (fsi-dump-history-raw)))
      (while (and history (not found))
	(when (string-match n (car history))
	  (setf found t loc (car history)))
	(setf history (cdr history)))
      ))
    (if (and loc (not ignore))
	(format "Dump matching %s and its output: %s ;; For all named dumps, see ,h"
		n (erbn-currentid-txt-tmp loc))
      "Syntax: : ,d[ump] [number/string].  If using string, use a (partial) name from your ,h[istory]. For dictionary, use ,dict"
      )))

(defun erbn-currentid-txt (id)
  (concat erbn-fulllog-url id ".txt"))

(defun erbn-currentid-txt-tmp (id)
  (concat erbn-tmplog-url id ".txt"))

(defalias 'fsi-dump 'fsi-fulllog-last)


(defun erbn-currentid-set ()
  "Ensure a currentid if it is not already set. Always use this to set a currentid within a task. Thus, it is set only once."
  ;; (debug)
  (unless erbn-currentid
    (setf erbn-currentid (erbn-currentid-new)))
  erbn-currentid)

(defun erbn-currentid-new ()
  (concat (format-time-string "%y%m%d.%H%M%S.") (erbutils-randomstring 6)))


(defun fsi-match-data ()
  "Provide a rudimentary match-data. No args atm."
  (match-data))




(defalias 'fsi-prog1 'prog1)
(defalias 'fsi-prog2 'prog2)
;; move to erbc.el.

(defun fsi-number-sequence (from to &optional inc)
  (cond
   ((> (abs (- from to)) 10123)
    (error "Go try large sequences in your own emacs!"))
   (t
    (number-sequence from to inc))))



(defalias 'fsi-aref 'aref)
(defalias 'fsi-assq 'assq)

(defun fsi-string-prefix-p (a b &optional c &rest d)
  (string-prefix-p a b c))

(defun fsi-string-suffix-p (a b &optional c &rest d)
  (string-suffix-p a b c))

;; NOTE that this is the definition of fsi-symbol-function. It is, thus, a defalias
(erbutils-defalias-i '(not symbol-function booleanp) 1)

(erbutils-defalias-i '(nth) 2)


(defun fsi-intern (str)
  "fsbot will NOT allow uninterned symbols." 
  (assert (stringp str))
  (intern str))

(defun fsi-make-symbol (&rest _)
  (error "Use fs-intern of fs-make-symbol"))

(defun fsi-intern-soft (&rest _)
  (error "Use fs-intern instead."))



(defun fsi-redirect (term1 term2)
  (fsi-set-term (format "%s" term1)
		(format "redirect %s" term2)))




(provide 'erbc)

(run-hooks 'erbc-after-load-hooks)



;;; erbc.el ends here

