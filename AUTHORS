LICENSE: Erbot by Dave Goel and others is licensed under GNU GPLv3.
----

AUTHORS, CONTRIBUTORS AND MAINTAINERS:

Primary author:
 deego             <deego3@gmail.com>: 
      	   	   Author, designer, maintainer and host of erbot and fsbot.
      	   	   The vast majority of erbot; continued maintenance; integration of foreign libraries.
		   The core of erbot, including the parser, lisp interpreter, etc.
		   (All code with no listed author is authored by Dave Goel.)


Other Contributors, alphabetically:
      (This list also includes several members of freenode/#emacs whose comments influenced erbot's behavior.)

 #emacsers: 	    Contribution to fsbot's db and userfunctions, suggestions, feature requests, syntax ideas. 

 Alejandro Benitez               <benitezalejandrogm@gmail.com>
 	   	    Original version of erbtranslate. In 2015, when translate.el stopped working, deego changed the backend to use google-translate.el instead.
		    As of 2018, that's broken as well, and erbtranslate.el is not part of erbot.

 Alex Schroeder:    (kensanata)
      		    Numerous suggestions.

 Brian Templeton    (bpt)
       		    suggestions.

 Damien Elmes       (resolve)
 		    Idea for ,, invocation of fsbot.

 David Edmunston    <dme@dme.org>
       		    Is author of Erobot.el - an irc library on top of erc - from which erbot's irc interface is derived.

 defanor	    <defanor@uberspace.net>
 		    erbdbpedia.el

 Dheeraj Buduru     (dbuduru)
 	 	    suggestions

 Enrico Bandiera    suggestions

 Grant Bowman       (grantbow) 
       		    suggestions
		    
 jlf   		    (jlf, #emacs)
 		    suggestions
 
 J. Michael Dupont  (mdupont) 
    	    	    suggestions
		    
 Jorgen Schaefer    (forcer)
 		    -erbrss.el (old, RSS feed for erbot. Atm, broken and not part of erbot.)
 		    suggestions


 Lawrence Mitchell  (lawrence)
 	  	    suggestions
		    
 Luis Fernandes     (e1f)
      		    suggestions

 Mario Lang         (delYsid) 
       		    suggestions
		    
 Michael Olson      (mwolson)    <mwolson@gnu.org>
 	 	    -Makefile. (Not functional atm.)
		    Fixes.
		    
 Sebastian Freundt  (hroptatyr)  <freundt@math.TU-Berlin.DE> :
		    -erbcompat (old) - to use if you run erbot on Xemacs. 
		    -erbmsg (old)    - Memoserv support in erbot.  (Disabled atm, pending a code review.)
 	   	    Fixes.
		    
 Jose E Marchesi    (jemarch) 
 	 	    suggestions
		    
 Pete Kazmier       (pkazmier)
      		    -erbjavadoc, -erburl. (old, Not functional atm.)

 Taylor R Campbell  (riastradh)
 	  	    Russian roulette functions.  (Part of erbc4.el)

 Vivek Dasmohapatra (fledermaus) <vivek@etla.org>
		    Provided a host for fsbot for several years. Co-admined fsbot during that time.
		    Helped with encoding issues.
       		    Authored -erbim.el (old: Internationalization search functions. Broken for emacs >=24).
		    Fixes.
		    
 Yann Hodique       (sigma)
      		    suggestions


Where (old) refers to files that are not part of erbot atm. They continue to be included with erbot, but their names are now prefixed with a -




