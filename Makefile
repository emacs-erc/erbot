# Copyright (C) 2023 Free Software Foundation, Inc.

# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is included with this
# program in COPYING.  See also <http://www.gnu.org/licenses/>.

EMACS ?= emacs
SELECTOR ?= t
ERC_D ?= /checkout/test/lisp/erc/resources/erc-d

.PHONY: all install is-same clean run test

all:
ifeq ($(CI),)
	@echo These recipes should only run in the CI container.
	@false
endif
	@true

install:
	$(EMACS) -batch \
		-eval '(package-refresh-contents)' \
		-eval '(package-install (quote bbdb))' \
		-eval '(package-install (quote timerfunctions))'

# This test requires a server reachable on "localhost" over port 6667
# without TLS.  An erbot instance with nick "fsbot" must already be
# joined to channel #chan.  Outside an EMBA container, you'll need to
# run "make build/bugs" to obtain the testing library.  You can then
# run "make test ERC_D=build/bugs/lisp/erc-d" as normal.
test:
	test -d /checkout
	$(EMACS) -Q -batch \
		-L $(ERC_D) \
		-L test \
		-l test/erbot-black-box-tests.el \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

is-same: build/erbot/ChangeLog build/data/botbbdb
	test -f build/data/botbbdb
	diff ChangeLog build/erbot/ChangeLog

build/commit/new: | build/commit
	diff -w ChangeLog build/erbot/ChangeLog | \
		grep '^> ' | sed 's/^> //' > $@

build/commit/args build/commit/message &: build/commit/new
	$(EMACS) -Q -batch -l scripts/change-log-to-git-commit.el \
		--file=build/commit/new -f chlog2git-compose \
		message > build/commit/args.maybe
	test -s build/commit/args.maybe
	mv build/commit/args.maybe build/commit/args

build/updated:
	rm -rf lisp data
	cp -r build/erbot lisp
	cp -r build/data data
	rm lisp/COPYING data/COPYING
	mv -f lisp/ChangeLog ChangeLog
	mv -f lisp/AUTHORS AUTHORS
	mv -f lisp/README.TXT README
	touch $@

# https://en.wikipedia.org/wiki/Fortune_%28Unix%29
# https://man.archlinux.org/man/strfile.1.en
#
# The image includes /usr/share/games/fortunes/zippy{,.dat}, but it
# seems to have a different set of lines.
build/yow.lines.dat: build/yow.lines
	strfile $<

build/yow.lines: build/yow.lines.orig
	sed 's/\x0/\n%/' $< > $@

build/yow.lines.orig: id=231108d096a0a850a0384e5545b66ac1c5e899ca
build/yow.lines.orig: baseurl=https://git.savannah.gnu.org/cgit/emacs.git
build/yow.lines.orig: | build
	wget $(baseurl)/plain/etc/yow.lines?id=$(id) -O $@

build/erbot/ChangeLog: | build
	wget -nH --cut-dirs=2 -P build \
		-r -np --wait=1 --random-wait -e robots=off \
		http://gnufans.net/~fsbot/erbot/erbot/ || :

build/data/botbbdb: | build
	wget -nH --cut-dirs=1 -P build \
		-r -np --wait=1 --random-wait -e robots=off \
		http://gnufans.net/~fsbot/data/ || :

build/bugs: | build
	git clone --depth=1 https://gitlab.com/emacs-erc/bugs.git $@

build build/commit:
	mkdir -p build/commit

clean:
	rm -rf build

# Run bot interactively for debugging (ideally inside the container).
# Requires manually executing the startup procedure.
run:
	emacs -L lisp -L vendor -l scripts/run-bot.el scripts/run-bot.el
