# Copyright (C) 2023 Free Software Foundation, Inc.

# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is included with this
# program in COPYING.  See also <http://www.gnu.org/licenses/>.

FROM registry.gitlab.com/emacs-erc/bugs/emacs:master

# We're exec'ing emacs directly rather than going through a login shell, so
# /etc/profile, which normally adds /usr/games to users' PATHs, won't get
# sourced.
RUN apt-get update \
    && apt-get install -y --no-install-recommends fortune-mod fortunes wget \
    && rm -rf /var/lib/apt/lists/* \
    && ln -sf /usr/games/fortune /usr/bin/fortune

RUN groupadd -g 1000 fsbot \
    && useradd -m -g 1000 -u 1000 fsbot \
    && cd /home/fsbot \
    && mkdir -p public_html/botlog \
    && mkdir public_html/logtmp \
    && mkdir public_html/logfull \
    && mkdir bin .emacs.d \
    && touch .emacs.d/init.el

COPY . /home/fsbot/opt/tmp/

RUN cd /home/fsbot/opt/tmp/ \
    && git apply -- patches/* \
    && make build/yow.lines.dat && mv build/yow.lines* /home/fsbot/ \
    && mv lisp data vendor scripts test Makefile COPYING ../ \
    && cd /home/fsbot && rm -rf opt/tmp \
    && ln -sf /usr/share/games/fortunes opt/fortunes \
    && echo "alias ll='ls -alh'" >> .bashrc \
    && chown -R 1000:1000 ./

USER fsbot
WORKDIR /home/fsbot/opt

RUN git config --global user.email "fsbot@gnufans.net" \
    && git config --global user.name "fsbot" \
    && make install

# Intentionally misspelled.
ENV ERBOT_NICK ffsbot

ENTRYPOINT ["emacs", "-batch", "-L", "lisp", "-L", "vendor", \
    "-eval", "(package-initialize)", \
    "-l", "scripts/run-bot.el", "-f", "run-bot-batch"]
# You can still pass new/overriding options before any args.
CMD ["localhost", "6667", "#test"]

