;; -*- lexical-binding: t; -*-
(require 'cl-lib)

(defun chlog2git-scan ()
  (skip-syntax-forward "-")
  (when-let*
      (((looking-at (rx bol (or "1" "2"))))
       (date-end (progn (skip-syntax-forward "^-") (point)))
       (date (string-trim
              (buffer-substring-no-properties (pos-bol) (goto-char date-end))))
       (name (and (skip-syntax-forward "-")
                  (buffer-substring-no-properties (point) (pos-eol))))
       ((and (forward-line)
             (or (not (looking-at (rx bol eol))) (forward-line))))
       (subject (progn
                  (beginning-of-line-text)
                  (string-trim-right
                   (buffer-substring-no-properties (point) (pos-eol)))))
       (rest (let (rest)
               (while (and (zerop (forward-line))
                           (looking-at (rx "\t" (+ nonl))))
                 (beginning-of-line-text)
                 (push (string-trim
                        (buffer-substring-no-properties (point) (pos-eol)))
                       rest))
               (string-join (nreverse rest) "\n"))))
    (when (string-empty-p rest)
      (setq rest nil))
    (list date name (concat subject (and rest "\n\n") rest))))

(defun chlog2git-compose ()
  (goto-char (point-min))
  (cl-assert command-line-args-left t)
  (message "chlog2git-compose: %S"
           (list :command-line-args-left command-line-args-left
                 :default-directory default-directory
                 :buffer-file-name (buffer-file-name)))
  (when-let* ((file (pop command-line-args-left))
              (args (chlog2git-scan)))
    (message "chlog2git-compose: %S"
             (list :args (take 2 args)
                   :message (nth 2 args)))
    (pcase-let ((`(,date ,author ,message) args))
      (princ "--date\n")
      (princ date)
      (terpri)
      (princ "--author\n")
      (princ author)
      (terpri)
      (flush-standard-output)
      (with-temp-file file
        (insert message)))))
