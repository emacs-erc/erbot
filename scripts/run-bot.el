;;; run-bot.el --- Helpers for running ErBot on HEAD -*- lexical-binding: t -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; A copy of the GNU General Public License appears in COPYING, which
;; accompanies this program.  See also <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file is only meant to run inside the provided container image.
;; Doing so elsewhere may be harmful.  You can set the bot's nickname
;; with the environment variable "ERBOT_NICK" and the password with
;; "ERBOT_PASSWORD".  Other "ERBOT_"-prefixed variables may also be
;; honored.  The nick defaults to "fsbot" and the password to nil,
;; meaning no authentication is performed.  The only mandatory
;; command-line argument is a server name.  Along with that, you can
;; provide a port (which defaults to 6667) and any number of channels
;; to join.  This script expects specific libraries to be loadable
;; when `require'd.  See the accompanying Makefile and Dockerfile for
;; usage examples.

;; TODO:
;;
;; - Find out the right way to load ErBot's dependencies.
;;
;; - Investigate errors and warnings given when loading dependencies.
;;   For example, loading `erbbdb' gives "outdated usage of
;;   `bbdb-search'", and `timingfunctions' produces complaints about a
;;   dependency cycle.

;;; Code

;; It seems this option in particular must be customized before
;; loading `erbcspecial' because it's needed during macro expansion.
;; The same does not apply across the board, however (e.g.,
;; `erbot-octave-p').
(defvar erbot-paranoid-p)
(setq erbot-paranoid-p (getenv "ERBOT_PARANOID"))

;; Our setup currently suffers from various loading issues, the most
;; distressing being a handful of eager macro-expansion failures.  For
;; example, loading `erbn-pf-file' via certain functions in erbc3.el
;; produces
;;
;;   Eager macro-expansion failure: (void-variable fs-function)
;;
;; which refers to the "user function" `fs-pts/make-list-f'.
;; Similarly, certain functions in erbcspecial.el that rely on
;; `erbn-mapcar-etc', such as `fsi-mapcan', don't get defined at all
;; because `fsi-sandbox-standardize-function' rejects local symbols,
;; like `fs-f', which results in
;;
;;   fsi-eval: Symbol's value as variable is void: fs-f
;;
;; The same problem also affects functions that call `fsi-apply', such
;; as `fsi-funcall'.  FWIW, these errors do not occur at runtime.  The
;; following advice-related hacks try to circumvent these issues at
;; load time in the most superficial manner.

(defmacro run-bot--with-neutered-safety-measures (&rest body)
  `(progn
     (advice-add 'fsi-sandbox-standardize-function :override #'identity)
     (advice-add 'erbn-readonly-check :override #'ignore)
     (unwind-protect ,(macroexp-progn body)
       (advice-remove 'erbn-readonly-check #'ignore)
       (advice-remove 'fsi-sandbox-standardize-function #'identity))))

;; Not sure we should be loading `erball' at all here, but as long as
;; we are, might as well ensure it finishes.
(with-suppressed-warnings ((obsolete cl))
  (eval-and-compile
    (run-bot--with-neutered-safety-measures (require 'erball))))

(require 'erb1)
(require 'erc-services) ; for `erc-nickserv-passwords'
(require 'erc-join) ; for autojoin

(define-obsolete-variable-alias 'erc-process
  'erc-server-process "22") ; 5.1
(define-obsolete-variable-alias 'erc-announced-server-name
  'erc-server-announced-name "22")
(define-obsolete-variable-alias 'erc-default-coding-system
  'erc-server-coding-system "22")
(define-obsolete-function-alias 'erc-send-command
  'erc-server-send "22")
(define-obsolete-function-alias 'print-help-return-message
  'help-print-return-message "23.2")

(defvar fsi-internal-english-weights-spec) ; undefined in code base
(defvar network-security-level)

(setq bbdb-file (expand-file-name "data/botbbdb")
      erbn-pf-file (expand-file-name "data/userfunctions.el")
      erbn-pv-file (expand-file-name "data/uservariables.el")
      erbot-nick (or (getenv "ERBOT_NICK") "fsbot")
      fsi-m8b-p ??
      ;; Not sure what to use here, so just copy `fsi-do-random'.
      fsi-internal-english-weights-spec
      '((1 1 1 1) ((fsi-doctor msg) (fsi-flame nick) (fsi-yow) (fsi-fortune))))

;; Use more aggressive reconnect strategy (new in ERC 5.6).
(setq erc-server-reconnect-function #'erc-server-delayed-check-reconnect)

(defun run-bot--neuter-safety-measures-while-loading (orig &rest rest)
  (run-bot--with-neutered-safety-measures (apply orig rest)))

;; FIXME Figure out how to configure this bot properly because what
;; follows is obviously an ugly hack.  Basically, a "file not found"
;; error is signaled on the first PRIVMSG to a channel by any party.
;; A naive guess is that if `erbn-fulllog-new' hasn't yet run, which
;; seems to be true until `erbot-reply' is called with something other
;; than `noreply', then the call to `erbn-fulllog-make-permanent' at
;; the bottom of `erbot-remote1' fails with a `file-missing' for
;; somefile.txt at `erbn-tmplog-path'.  (This entire sequence runs on
;; the handler hook `erc-server-PRIVMSG-functions').  Not sure if this
;; issue occurs with erbot's native `erbot-servers-channels', but when
;; testing ERC, we can't afford to wait two minutes on every init,
;; hence our resorting to ERC's built-in autojoin facility.
(defun run-bot--ensure-log-file-on-join ()
  (when-let* ((erbn-logging)
              (file (expand-file-name (concat erbn-currentid ".txt")
                                      erbn-tmplog-path)))
    (cl-pushnew (find-file-noselect file) erbot-buffers-to-save)))

(defun run-bot--setup ()
  (add-hook 'erc-join-hook #'run-bot--ensure-log-file-on-join)
  ;; Leave this advice for the duration of the session because it's
  ;; needed whenever ErBot modifies `erbn-pf-file', which happens, for
  ;; example, while defining new user functions.
  (advice-add 'erbn-pf-load :around
              #'run-bot--neuter-safety-measures-while-loading)
  ;; Calling `erbot-install' doesn't seem to be the right move because
  ;; it runs `erball-reload', which lives in erball.el, meaning that
  ;; the latter file needs loading beforehand.  Moreover, `erball'
  ;; already contains a top-level call to `erball-load', which loads
  ;; the dependency manifest `erball-req', a superset of the one
  ;; loaded by `erball-reload' (`erball-req2').
  (erbn-pv-load)
  (erbn-pf-load)
  (erbot-install1))

(defun run-bot--start (server port password channels)
  (when password
    (cl-pushnew 'sasl erc-modules))
  (when channels
    (setq erc-autojoin-channels-alist `((".+" ,@channels))))
  (when (member server '("localhost" "127.0.0.1" "::1"))
    (require 'nsm)
    (setq network-security-level 'low))
  (funcall (if (= port erc-default-port) #'erc #'erc-tls)
           :server server
           :port port
           :nick erbot-nick
           :user erbot-nick
           :full-name erbot-nick
           :password password))

(defun run-bot--idle ()
  (erc-toggle-debug-irc-protocol)
  (with-current-buffer "*erc-protocol*" (toggle-truncate-lines))
  (while noninteractive
    (when (get-buffer "*trace-output*")
      (with-current-buffer "*trace-output*"
        (when (> (point-max) 1)
          (message "%s" (delete-and-extract-region (point-min) (point-max))))))
    (with-current-buffer "*erc-protocol*"
      (when (> (point-max) 1)
        (with-silent-modifications
          (princ (delete-and-extract-region (point-min) (point-max)))
          (flush-standard-output))))
    (accept-process-output nil 2)))

(defun run-bot-batch ()
  "Run fsbot non-interactively inside the CI container."
  (dolist (f (list bbdb-file erbn-pf-file erbn-pv-file))
    (unless (file-writable-p f) (error "File %s not writable" f)))
  (unless command-line-args-left (error "Missing server argument"))
  (message "run-bot: %S" (list :default-directory default-directory
                               :command-line-args-left command-line-args-left
                               :erbot-nick erbot-nick
                               :passwordp (and (getenv "ERBOT_PASSWORD") t)))
  (run-bot--setup)
  (run-bot--start (pop command-line-args-left)
                  (string-to-number (or (pop command-line-args-left) "6667"))
                  (getenv "ERBOT_PASSWORD")
                  command-line-args-left)
  (run-bot--idle))

;;; run-bot.el ends here
