;;; erbot-black-box-tests.el --- Local black-box tests for ErBot -*- lexical-binding: t -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file tests baseline behavior for a bare-bones ErBot instance.
;; It expects a running IRC server on "localhost", port 6667, with a
;; nick named "fsbot" already joined to a channel named "#chan".  Each
;; test issues a series of bot commands and verifies expected output.
;; It does not itself `require' any of ErBot's libraries.

;;; Code
(require 'ert)
(require 'erc)
(require 'erc-d-t)

;; From test/lisp/erc/resources/erc-scenarios.common.el in emacs.git.
(defun erc-scenarios-common-say (str)
  (let (erc-accidental-paste-threshold-seconds)
    (goto-char erc-input-marker)
    (insert str)
    (erc-send-current-line)))

(defun erbot-black-box-tests--clean ()
  (when noninteractive
    (erc-cmd-QUIT "")
    (delete-process erc-server-process)
    (sleep-for 3)
    (erc-buffer-filter #'kill-buffer)))

(ert-deftest erbot-baseline ()
  (let ((expect (erc-d-t-make-expecter)))

    (ert-info ("Client connects successfully")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port 6667
                                :nick "tester")
        (funcall expect 20 "user modes for tester")
        (erc-cmd-JOIN "#chan")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
      (funcall expect 20 "<bob> tester, welcome!")

      (ert-info ("Reports matching version")
        (erc-scenarios-common-say ",erc-version")
        (funcall expect 20 `(: "<fsbot> ERC " ,erc-version)))

      (ert-info ("Dispenses info from database")
        (erc-scenarios-common-say ",erc")
        (funcall expect 20 '(: "<fsbot> " (+ nonl) (: (? "\n") (* nonl))
                               "IRC client")))

      (ert-info ("Evaluates arbitrary lisp")
        (erc-scenarios-common-say ",(let ((foo 6) (bar 7)) (* foo bar))")
        (funcall expect 20 "<fsbot> 42"))

      (ert-info ("Runs map-related funcs using `erbn-mapcar-etc' (regression)")
        (erc-scenarios-common-say ",(mapcan 'list '(1 2))")
        (funcall expect 20 "<fsbot> (1 2)")
        (erc-scenarios-common-say
         ",(mapconcat 'identity '(\"a\" \"b\") \"-\")")
        (funcall expect 20 "<fsbot> a-b"))

      (ert-info ("Runs `locate-library'")
        (erc-scenarios-common-say ",ll erc")
        (funcall expect 20 "/lisp/erc/erc.el"))

      (ert-info ("Answers magic 8-ball questions")
        (erc-scenarios-common-say "how??")
        (funcall expect 20 '(: "<fsbot> " alpha)))

      (ert-info ("Sends CTCP ACTION")
        (erc-scenarios-common-say ",shoot bob")
        (funcall expect 20 '(: "* fsbot " (+ nonl) (: (? "\n") (* nonl))
                               "bob")))

      (erbot-black-box-tests--clean))))

(ert-deftest erbot-query-user-functions ()
  (let ((expect (erc-d-t-make-expecter)))

    (ert-info ("Client connects successfully")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port 6667
                                :nick "tester")
        (funcall expect 20 "user modes for tester")
        (erc-cmd-QUERY "fsbot")))

    (with-current-buffer "fsbot"

      ;; Start from a clean slate.
      (erc-scenarios-common-say "fmakunbound my-list")
      (funcall expect 10 "fs-my-list")

      (ert-info ("Defining new function succeeds")
        (erc-scenarios-common-say "(defun my-list () (list 1 2 3))")
        (funcall expect 10 "<fsbot> fs-my-list"))

      (ert-info ("New function usable as bot command and in lisp form")
        (erc-scenarios-common-say "my-list")
        (funcall expect 10 "<fsbot> (1 2 3)")

        (erc-scenarios-common-say "(mapcar '1+ (my-list))")
        (funcall expect 10 "<fsbot> (2 3 4)"))

      (ert-info ("Other user functions load cleanly (regression)")
        (erc-scenarios-common-say ",order-drink tea tester")
        (funcall expect 20 "* fsbot slides a tea over to tester"))

      (ert-info ("Undefining function succeeds")
        (erc-scenarios-common-say "fmakunbound my-list")
        (funcall expect 20 "<fsbot> fs-my-list")
        (erc-scenarios-common-say "my-list")
        (funcall expect 20 "<fsbot> try"))

      (erbot-black-box-tests--clean))))

(ert-deftest erbot-query-edit ()
  (let ((expect (erc-d-t-make-expecter)))

    (ert-info ("Client connects successfully")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port 6667
                                :nick "tester")
        (funcall expect 20 "user modes for tester")
        (erc-scenarios-common-say "/msg fsbot help-erbot")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "fsbot"))

      (ert-info ("Prints help")
        (funcall expect 10 "<fsbot> [1] To talk")
        (funcall expect 10 '(: "<fsbot> [" digit (+ nonl) ",more / ,dump"))

        (erc-scenarios-common-say "more")
        (funcall expect 10 '(: "<fsbot> [" digit (+ nonl) "help-forget"))
        (funcall expect 10 '(: "<fsbot> [" digit (+ nonl) ",more / ,dump")))

      ;; Start from a clean slate.
      (erc-scenarios-common-say "forget erc-v3 all")
      (funcall expect 10 '(: (or "forgot" "no such term")))

      (ert-info ("Adds notes")
        (erc-scenarios-common-say
         "erc-v3 is a long-running initiative to bring IRCv3 to ERC")
        (funcall expect 10 "new note for \"erc-v3\"")

        (erc-scenarios-common-say
         "erc-v3 is also https://debbugs.gnu.org/cgi/bugreport.cgi?bug=49860")
        (funcall expect 10 "Added note [1] for  term \"erc-v3\"")

        (erc-scenarios-common-say "erc-v3")
        (funcall expect 10 "[0] a long-running")
        (funcall expect 10 "[1] https://debbugs"))

      (ert-info ("Moves notes")
        (erc-scenarios-common-say "1->0 in erc-v3")
        (funcall expect 10 "Moved note 1 to 0")

        (erc-scenarios-common-say "erc-v3 1")
        (funcall expect 10 "[1] a long-running"))

      (ert-info ("Forgets notes")
        (erc-scenarios-common-say "forget erc-v3 0")
        (funcall expect 10 "Forgot note 0")

        (erc-scenarios-common-say "erc-v3")
        (funcall expect 10 "a long-running")

        (erc-scenarios-common-say "forget erc-v3")
        (funcall expect 10 "forgot \"erc-v3\""))

      (erbot-black-box-tests--clean))))

(ert-deftest erbot-channel-merge ()
  (let ((expect (erc-d-t-make-expecter)))

    (ert-info ("Client connects successfully")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port 6667
                                :nick "tester")
        (funcall expect 20 "user modes for tester")
        (erc-cmd-JOIN "#chan")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))

      ;; Start from a clean slate.
      (erc-scenarios-common-say ",forget erc-v3 all")
      (funcall expect 10 '(: (or "forgot" "no such term")))
      (erc-scenarios-common-say ",forget bug#49860 all")
      (funcall expect 10 '(: (or "forgot" "no such term")))

      (ert-info ("Adds notes")
        (erc-scenarios-common-say
         ",erc-v3 is a long-running initiative to bring IRCv3 to ERC")
        (funcall expect 10 "new note for \"erc-v3\"")

        (erc-scenarios-common-say
         ",bug#49860 is https://debbugs.gnu.org/cgi/bugreport.cgi?bug=49860")
        (funcall expect 10 "new note for \"bug#49860\""))

      (ert-info ("Merges notes")
        (erc-scenarios-common-say ",merge bug#49860 erc-v3")
        (funcall expect 10 "merged \"bug#49860\" into")

        (erc-scenarios-common-say ",erc-v3 1")
        (funcall expect 10 "debbugs.gnu.org")

        (erc-scenarios-common-say ",bug#49860")
        (funcall expect 10 "[->] erc-v3")
        (funcall expect 10 "debbugs.gnu.org")

        (erc-scenarios-common-say ",dl bug#49860")
        (funcall expect 10 '(: "bug#49860" (+ nonl) (: (? "\n") (* nonl))
                               "redirect erc-v3")))

      (ert-info ("Forgets notes")
        (erc-scenarios-common-say ",forget erc-v3 all")
        (funcall expect 10 "forgot all 2 notes")

        ;; Doesn't cascade (as of June 2023)
        (erc-scenarios-common-say ",forget bug#49860")
        (funcall expect 10 "forgot \"bug#49860\" which"))

      (erbot-black-box-tests--clean))))


;;; erbot-black-box-tests.el ends here
